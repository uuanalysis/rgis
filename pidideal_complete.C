// Macro for running Panda pid tasks
// to run the macro:
// root  pid_complete.C  or in root session root>.x  pid_complete.C
int pidideal_complete(Int_t nEvents=10, TString prefix="ll", TString options="") {
    
    std::cout << "FLAGS: " << nEvents << "," << prefix << "," << options << std::endl;
    std::cout << std::endl;
    
    
    //***
    //----- User Settings    
    TString parAsciiFile = "all.par";
    TString input        = "";                 // set "dpm" or "llbar_fwp.DEC";
    TString output       = "pid";              // pid output name
    TString friend1      = "sim";
    TString friend2      = "digi";
    TString friend3      = "reco";             // reco, barrel or hough
    TString friend4      = "";
    //TString prefix     = "llbar_fwp";        // set "llbar_fwp" or "evtcomplete";
    //TString options    = "";                 // default: uses PndRecoKalmanTask (the Single Hypothesis)
    
    
    // Note that the default tracker is BarrelTrackFinder. 
    // Either set fOption="barreltrack" or leave it empty. 
    // Set options="stttracking" for IdealTrackFinder.
    options = "stttracking";
    
    //***
    //----- Init Settings
    PndMasterRunAna *fRun= new PndMasterRunAna();
    
    fRun->SetInput(input);
    fRun->AddFriend(friend1);
    fRun->AddFriend(friend2);
    fRun->AddFriend(friend3);
    fRun->AddFriend(friend4);
    fRun->SetOutput(output);
    fRun->SetParamAsciiFile(parAsciiFile);
    //fRun->SetOptions(fRun->GetOptions() + "multikalman" + options);
    fRun->Setup(prefix);
    
    
    //***
    //----- Add Options
    
    /* ***********************************************************************************************************************************************
    *
    * if options.Contains(""), then IdealTrackFinder will use the PndRecoKalmanTask (single particle hypothesis)
    * if options.Contains("genfit2"), then IdealTrackFinder will use the PndRecoKalmanTask2 (single particle hypothesis with GenFit2)
    * if options.Contains("multikalman"), then  IdealTrackFinder will use PndRecoMultiKalmanTask (multiple particle hypothesis)
    * if options.Contains("multikalman:genfit2"), then  IdealTrackFinder will use PndRecoMultiKalmanTask2 (multiple particle hypothesis with GenFit2)
    *
    * ***********************************************************************************************************************************************/
    
    fRun->SetOptions(options);
     
     
    //***
    //----- Add PID Tasks
    fRun->AddPidTasks();
    
    
    //***
    //----- Intialise & Run
    PndEmcMapper::Init(1);
    fRun->Init();
    fRun->Run(0, nEvents);
    fRun->Finish();
    return 0;
}
