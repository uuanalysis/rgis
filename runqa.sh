#!/bin/bash


echo "Started Ideal Tracking..."
root -q -b recoideal_complete.C > recoideal.log 2>&1

echo "Started Barrel Tracking..."
root -q -b recobarrel_complete.C > recobarrel.log 2>&1

echo "Started Tracking QA..."
root -q -b recoqa_complete.C > log_recoqa.log 2>&1

echo "Script has finished..."


#echo "Started Tracking QA..."
#root -q -b recoqa_complete.C > recoqa.log 2>&1
