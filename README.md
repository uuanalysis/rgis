## Rest-gas Impact Studies (RGIS)

### (+) _Simulating the Signal Events_

- non-extended target
- ppbar --> lambda lambdabar --> p pion + c.c
- beam momentum: 1.64 GeV, `fwp` instead of `phsp`
- evtgen for generating a events
- ideal trackfinder

```
ppbar --> lambda + lambdabar --> p + piminus + c.c
```

Data generated after pre-selection criteria has the data with following convention:

```bash
# ppbar --> lambda + lambdabar

--	        pbarp

# lambdabar --> pbar + piplus

d0          lambdabar
d0d0        pbar (antiproton)
d0d1        pi+

# lambda --> p + piminus

d1          lambda
d1d0        p (proton)
d1d1        pi-

```



### (+) _Code Structure_

- `docs/` contain genral documents
- `tracking/` contains simulation macros for tracing
- `analysis/` contains **rho** tutorial
- `PndLLbarAnaTask` is code linked to pandaroot/src
- `macro/` is main working directory from Walter

### (+) _Obserables_

- global efficiency
- efficiency as function of polar angle etc.

### (+) _Current Task_

- repeat above
- extended target


## _Code Run_

```bash
$ pndana					# alias for PandaRoot config, runs root
$ ./runideal.sh				# events=10000, OR
$ root -l anaideal.C		# run PndLLbarAnaTask (first build pandaroot)
root[0]: TBrowser b
```

## _Copying Files from GSI to Current Directory_

```bash
# using scp
scp aakram@lxpool.gsi.de:files.tar.gz . 	    # OR
scp lxpool:files.tar.gz .					    # lxpool in .ssh/config
```

```bash
# using rsync
rsync -avzP aakram@lxpool.gsi.de:files.tar.gz . # OR
rsync -avzP lxpool:files.tar.gz .			    # lxpool in .ssh/config
```