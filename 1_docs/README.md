
## Residual Gas

- Residual Gas from PANDA Targets by **Alfons Khoukaz**

## PANDA Analysis

- [Rho Analysis](https://panda-wiki.gsi.de/foswiki/bin/view/Computing/PandaRootRhoTutorial)
- [PANDA Lecture Week 2017](https://indico.gsi.de/event/6430/timetable/?view=nicecompact)
- [PANDA Comp Workshop in Thailand (2 Jul 17)](https://indico.gsi.de/event/4894/timetable/#20170703)


## Material

See `Dropbox` folder named `RGIS` for Talks, Turotials etc.

- Talks by Alfons Khoukaz
- Talks by Jennifer Puetz
- Talks by Klaus Götzen
- Talks by Ralf Kliemt
- etc, etc.


## HPC Cluster at GSI

- SBATCH Scripts 
- Virgo Cluster
- Koronos Cluster