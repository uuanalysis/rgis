/*
* PndLLbarAnaRGISTask.h
*
*	Created on: October 1, 2020
*  		Author: Adeel Akram
*/

#ifndef PNDANALYSIS_LLBARANATASK_RGISANATASK_H
#define PNDANALYSIS_LLBARANATASK_RGISANATASK_H

#include "FairTask.h"
#include <map>
#include <string>
#include "TLorentzVector.h"
#include "TDatabasePDG.h"

class TClonesArray;
class TObjectArray;
class TH1F;
class TH2F;

class RhoMassParticleSelector;
class PndAnalysis;
class RhoCandList;
class RhoCandidate;
class RhoTuple;
class RhoDecayTreeFitter;

class PndLLbarAnaTaskRGIS : public FairTask {
public:

	PndLLbarAnaTaskRGIS();
	virtual ~PndLLbarAnaTaskRGIS();
	virtual void SetParContainers();
	virtual InitStatus Init();
	virtual void Exec(Option_t* opt);
	virtual void Finish();
	void SetBeamMom(Double_t bm) { fBeamMom = bm; }
	
	// *** Walter added
	TLorentzVector TransformCoords(std::vector<TVector3> initframe, std::vector<TVector3> finalframe, TLorentzVector p);
	std::vector<TVector3> GenerateUnitVectors(TLorentzVector p_beam, TLorentzVector p_out, TLorentzVector p_ref);

private:

	int fEvtCount;                         		// Event Counter
	double fMass0_pi;							// Mass of Pion
	double fMass0_proton;						// Mass of Proton
	double fMass0_lam;							// Mass of Lambda
	Double_t fBeamMom;							// Beam Momentum
	//Double_t fBeamEnergy;						// Beam Energy: fBeamEnergy = sqrt(fBeamMom^2 + fMass0_proton^2)
	TDatabasePDG *fPdgCode;						// Particle ID from PDG
	PndAnalysis *fAnalysis;						// PndAnalysis Object
	RhoMassParticleSelector* fLambdaMassSel;   	// Cut on Invariant Mass
	TLorentzVector fIni;						// TLorentzVector of initial pbarpsystem
	bool fIsBestPID;							// Select PID Criteria: True (BestPID), False (TightPID)
	
	// *** Walter added
	TLorentzVector fTarg;
	TLorentzVector fBeam;
	double alpha_lam;
	double alpha_lambar;
	
	// ***
	// *** TODO: Remove Counters when debugged.
	// ***
	
	// After PID Selection
	int p_counter=0;
	int pbar_counter=0;
	int piplus_counter=0;
	int piminus_counter=0;
	
	// After MassSel.
	int lam0_counter =0;
	int lam0bar_counter=0;
	int pbarp_counter =0;
	
	// After VF
	int lam0_vf_counter =0;
	int lam0bar_vf_counter=0;
	int pbarp_vf_counter =0;
	
	int failed_lam0_vf = 0;
	int failed_lam0bar_vf = 0;
	
	// After 4C Fit
	int skipped = 0;
	int all4Ccount = 0;
	int goodfit = 0;
	
	// *** 
	// *** RhoTuples (RhoCandidate)
	// *** 
	RhoTuple *fntpPiMinus;
	RhoTuple *fntpPiPlus;
	RhoTuple *fntpProton;
	RhoTuple *fntpAntiProton;
	RhoTuple *fntpLambda;
	RhoTuple *fntpLambdaBar;
	RhoTuple *fntpPbarP;
	RhoTuple *fntpBestPbarP;
	
	// *** Walter added
	RhoTuple *fntpMCtruth;

	// *** 
	// *** Histograms (4-momentum)
	// *** 
	//TH1F *hpiminus_mom;
	//TH1F *hpiplus_mom;
	//TH1F *hproton_mom;
	//TH1F *hantiproton_mom;
    //TH1F *hlam0_mom;
	//TH1F *hlam0bar_mom;
		
	// *** 
	// *** Functions
	// *** 
	int SelectTruePid(PndAnalysis *ana, RhoCandList &cand_list);
	std::map<int,int> VertexQaIndex(RhoCandList* candList, Double_t prob_cut=0.01);
	
	ClassDef(PndLLbarAnaTaskRGIS,1);
};

#endif /* PNDANALYSIS_LLBARANATASK_RGISANATASK_H */
