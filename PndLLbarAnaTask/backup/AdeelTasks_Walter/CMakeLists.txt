# Create a library called "llbaranataskrgis" 
#Following lines were added
#${CLHEP_INCLUDE_DIR} 
#${SIMPATH}/include 
#${FAIRROOT_LIBRARY_DIR}

set(INCLUDE_DIRECTORIES
	${ROOT_INCLUDE_DIR} 
	${CLHEP_INCLUDE_DIR} 
	${BASE_INCLUDE_DIRECTORIES} 
	${SIMPATH}/include 
	${CMAKE_SOURCE_DIR}/field 
	${CMAKE_SOURCE_DIR}/trackbase 
	${CMAKE_SOURCE_DIR}/pnddata
	${CMAKE_SOURCE_DIR}/pnddata/PidData
	${CMAKE_SOURCE_DIR}/passive
	${CMAKE_SOURCE_DIR}/tpc
	${CMAKE_SOURCE_DIR}/lhetrack
	${CMAKE_SOURCE_DIR}/detectors/emc/EmcData
	${CMAKE_SOURCE_DIR}/detectors/emc/EmcReco
	${CMAKE_SOURCE_DIR}/fsim
	${CMAKE_SOURCE_DIR}/analysis/
	${CMAKE_SOURCE_DIR}/analysis/rho
	${CMAKE_SOURCE_DIR}/analysis/AnalysisTools
	${CMAKE_SOURCE_DIR}/analysis/rho/RhoBase
	${CMAKE_SOURCE_DIR}/analysis/rho/RhoFitter
	${CMAKE_SOURCE_DIR}/analysis/rho/RhoMath#added
	${CMAKE_SOURCE_DIR}/analysis/rho/RhoTools#added
	${CMAKE_SOURCE_DIR}/analysis/rho/RhoSelector
	${CMAKE_SOURCE_DIR}/analysis/rho/RhoHistogram#added
	${CMAKE_SOURCE_DIR}/analysis/rho/DecayTreeFitter#added
	#${CMAKE_SOURCE_DIR}/PndTools/AnalysisTools
	#${CMAKE_SOURCE_DIR}/PndTools/AnalysisTools/Fitter
	#${CMAKE_SOURCE_DIR}/PndTools/PndParticleFitters
	${CMAKE_SOURCE_DIR}/genfit
	${CMAKE_SOURCE_DIR}/hyptools
	${CMAKE_SOURCE_DIR}/AdeelTasks
)

include_directories(${INCLUDE_DIRECTORIES})

set(LINK_DIRECTORIES
	${ROOT_LIBRARY_DIR}
	${FAIRROOT_LIBRARY_DIR}
)
 
link_directories(${LINK_DIRECTORIES})

############### Adeel: libLLbarAnaTaskRGIS (start) #############
set(SRCS
PndLLbarAnaTaskRGIS.cxx
)
set(LINKDEF PndLLbarAnaTaskRGISLinkDef.h)
set(LIBRARY_NAME LLbarAnaTaskRGIS)
############### Adeel: libLLbarAnaTaskRGIS (end) ###############

set(DEPENDENCIES Base PndData Rho AnalysisTools TrkBase)
GENERATE_LIBRARY()
