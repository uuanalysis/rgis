/*
* PndLLbarAnaTaskIdeal.h
*
*  Created on: May 5, 2016
*  Author: Walter Ikegami Andersson
*/

#ifndef PndLLbarAnaTaskIdeal_H
#define PndLLbarAnaTaskIdeal_H 1

#include "FairTask.h"
#include <map>
#include <string>
#include "TLorentzVector.h"
#include "TDatabasePDG.h"

class TClonesArray;
class TObjectArray;
class TH1F;
class TH2F;

class RhoMassParticleSelector;
class PndAnalysis;
class RhoCandList;
class RhoTuple;
class RhoDecayTreeFitter;

class PndLLbarAnaTaskIdeal : public FairTask
{

 public:
	
	// ** Default constructor   
	PndLLbarAnaTaskIdeal();
	
	// ** Destructor 
	~PndLLbarAnaTaskIdeal();	
	
	// ** Virtual method Init 
	virtual InitStatus Init();
	
	// ** Virtual method Exec 
	virtual void Exec(Option_t* opt);
	
	// ** Virtual method Finish
	virtual void Finish();
	
	void SetBeamMom(Double_t ini) { fIni = ini; }

 protected:
	
	Double_t fIni;
	TDatabasePDG *pdg;
	
 private: 
	// *** event counter
	int nevts;

	double m0_pi;
	double m0_p;
	double m0_lam;
	
	// Mass selector
	RhoMassParticleSelector* lambdaMassSelector;

	// Truthmatch filter
	int  SelectTruePid(PndAnalysis *ana, RhoCandList &l);

	RhoTuple *fntpPiMinus_P4;
	RhoTuple *fntpPiPlus_P4;
	RhoTuple *fntpProton_P4;
	RhoTuple *fntpAntiProton_P4;

	RhoTuple *fntpLambda_P4;
	RhoTuple *fntpLambdaBar_P4;

	RhoTuple *fntpPbarp;
	RhoTuple *fntpBestPbarp;

	//Piminus truthmatch histos
	TH1F *hpiminus_P;

	//Piplus truthmatch histos
	TH1F *hpiplus_P;

	//Proton truthmatch histos
	TH1F *hproton_P;

	//AntiProton truthmatch histos
	TH1F *hantiproton_P;

	//Lambda truthmatch histos
	TH1F *hlam0_M_all;

	//Lambdabar truthmatch histos
	TH1F *hlam0bar_M_all;
	
	//Ppbar truthmatch histos
	TH1F *hpbarp_M_all;

	// *** --------------------- ***
	// *** True MC 1D histograms ***
	// *** --------------------- ***

	// *** ------------------------- ***
	// *** Create some 2D histograms ***
	// *** ------------------------- ***
	
	// *** the PndAnalysis object
	PndAnalysis *theAnalysis;

	// *** Get parameter containers
	virtual void SetParContainers();
		
	ClassDef(PndLLbarAnaTaskIdeal,1);
  
};

#endif
