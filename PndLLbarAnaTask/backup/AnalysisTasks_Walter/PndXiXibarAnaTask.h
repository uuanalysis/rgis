/*
 * PndXiXibarAnaTask.h
 *
 *  Created on: Sep 23, 2016
 *      Author: walan603
 */

#ifndef PNDXIXIBARANATASK_H_
#define PNDXIXIBARANATASK_H_

#include "FairTask.h"
#include <map>
#include <string>
#include <iostream>
#include "TLorentzVector.h"
#include "TDatabasePDG.h"
#include "TMatrixD.h"

class TClonesArray;
class TObjectArray;
class TH1F;
class TH2F;

class RhoMassParticleSelector;
class PndAnalysis;
class RhoCandList;
class RhoCandidate;
class RhoTuple;
class RhoDecayTreeFitter;

class PndXiXibarAnaTask : public FairTask
{

 public:

	// ** Default constructor
	PndXiXibarAnaTask();

	// ** Destructor
	~PndXiXibarAnaTask();

	// ** Virtual method Init
	virtual InitStatus Init();

	// ** Virtual method Exec
	virtual void Exec(Option_t* opt);

	// ** Virtual method Finish
	virtual void Finish();

	void SetBeamMom(Double_t ini) { fBeamMom = ini; }

 protected:

	Double_t fBeamMom;
	TLorentzVector fIni;
	TLorentzVector fBeam;
	TLorentzVector fTarg;
	TDatabasePDG *pdg;

 private:
	// *** event counter
	int nevts;

	double m0_pi;
	double m0_p;
	double m0_lam;
	double m0_xi;
	double alpha_lam;
	double alpha_lambar;
	double alpha_xi;
	double alpha_xibar;

	// Mass selector
	RhoMassParticleSelector* lambdaMassSelector;
	RhoMassParticleSelector* xiMassSelector;

	// Truthmatch filter
	int  SelectTruePid(PndAnalysis *ana, RhoCandList &l);

	void VertexFit(RhoCandList &l, Double_t prob_cut);

	void MassFit(RhoCandList &l, Double_t mass, Double_t prob_cut);

	void Tree4CFit(RhoCandList &l, TLorentzVector ini, Double_t prob_cut);

    std::map<int,int> VertexQaIndex(RhoCandList* candList, Double_t prob_cut);

    std::map<int,int> MassFitQaIndex(RhoCandList* candList, Double_t m0, Double_t prob_cut);

    void CombinedList(RhoCandidate* cand, RhoCandList* combinedList, int pdg_id);

	void NotCombinedList(RhoCandList combinedList, RhoCandList * candList);

	RhoTuple *fntpPiMinus;
	RhoTuple *fntpPiPlus;
	RhoTuple *fntpProton;
	RhoTuple *fntpAntiProton;

	RhoTuple *fntpLambda;
	RhoTuple *fntpLambdaBar;

	RhoTuple *fntpXi_PiMinus;
	RhoTuple *fntpXiBar_PiPlus;

	RhoTuple *fntpXi;
	RhoTuple *fntpXiBar;

	RhoTuple *fntpPbarp;
	RhoTuple *fntpBestPbarp;

	//MC true tuples

	RhoTuple *fntpMCtruth;

	// *** --------------------- ***
	// *** True MC 1D histograms ***
	// *** --------------------- ***

	// *** ------------------------- ***
	// *** Create some 2D histograms ***
	// *** ------------------------- ***

	// *** the PndAnalysis object
	PndAnalysis *theAnalysis;

	// *** Get parameter containers
	virtual void SetParContainers();

	ClassDef(PndXiXibarAnaTask,1);

};

#endif
