/*
 * PndSpinObsTools.h
 *
 *  Created on: Sep 29, 2016
 *      Author: walan603
 */

#ifndef PNDSPINOBSTOOLS_H_
#define PNDSPINOBSTOOLS_H_

#include <string>
#include <iostream>

#include "TLorentzVector.h"
#include "TMatrixD.h"

//class PndSpinObsTools {
//};

TLorentzVector TransformCoords(std::vector<TVector3> initframe, std::vector<TVector3> finalframe, TLorentzVector p);

Double_t SpinWeight(std::vector<TVector3> pdecayframe, std::vector<TVector3> pbardecayframe, TLorentzVector p,
		TLorentzVector pbar, Double_t Py, Double_t Pybar, Double_t Cxx, Double_t Cyy, Double_t Czz, Double_t Cxz, Double_t Czx);

Double_t SpinWeightGeneral(std::vector<TVector3> pdecayframe, std::vector<TVector3> pbardecayframe, TLorentzVector p,
		TLorentzVector pbar, Double_t Py, Double_t Pybar, TMatrixD Cij, Double_t alpha, Double_t alphabar);

TMatrixD GenerateSpinCorrTrigFuncs(Double_t cmAngle);

TMatrixD GenerateSpinCorrData(Double_t cmAngle);

Double_t GenerateSpinCorrTrigSingle(Double_t cmAngle);

Double_t GenerateSpinCorrDataCxx(Double_t cmAngle);

Double_t GenerateSpinCorrDataCyy(Double_t cmAngle);

Double_t GenerateSpinCorrDataCzz(Double_t cmAngle);

Double_t GenerateSpinCorrDataCxz(Double_t cmAngle);

std::vector<TVector3> GenerateUnitVectors(TLorentzVector p_beam, TLorentzVector p_out, TLorentzVector p_ref);

#endif /* PNDSPINOBSTOOLS_H_ */
