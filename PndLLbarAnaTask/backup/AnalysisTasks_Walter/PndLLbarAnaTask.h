/*
* PndLLbarAnaTask.h
*
*  Created on: July 7, 2015
*  Author: Walter Ikegami Andersson
*/

#ifndef PndLLbarAnaTask_H
#define PndLLbarAnaTask_H 1

#include "FairTask.h"
#include <map>
#include <string>
#include <iostream>
#include "TLorentzVector.h"
#include "TDatabasePDG.h"
#include "TMatrixD.h"

class TClonesArray;
class TObjectArray;
class TH1F;
class TH2F;

class RhoMassParticleSelector;
class PndAnalysis;
class RhoCandList;
class RhoCandidate;
class RhoTuple;
class RhoDecayTreeFitter;

class PndLLbarAnaTask : public FairTask
{

 public:
	
	// ** Default constructor   
	PndLLbarAnaTask();
	
	// ** Destructor 
	~PndLLbarAnaTask();	
	
	// ** Virtual method Init 
	virtual InitStatus Init();
	
	// ** Virtual method Exec 
	virtual void Exec(Option_t* opt);
	
	// ** Virtual method Finish
	virtual void Finish();
	
	void SetBeamMom(Double_t ini) { fBeamMom = ini; }

	void SetIsSig(bool issig) { fSignalsample = issig; }
	void SetIsDpm(bool isdpm) { fDpmsample = isdpm; }

 protected:
	
	Double_t fBeamMom;
	bool fSignalsample;
	bool fDpmsample;
	TDatabasePDG *pdg;
	TLorentzVector fBeam;
	TLorentzVector fTarg;
	TLorentzVector fIni;
	
 private: 
	// *** event counter
	int nevts;

	double m0_pi;
	double m0_p;
	double m0_lam;
	double alpha_lam;
	double alpha_lambar;

	double SumWeightCosThetaX;
	double SumWeightCosThetaXBar;
	double SumWeightCosThetaY;
	double SumWeightCosThetaYBar;
	double SumWeightCosThetaZ;
	double SumWeightCosThetaZBar;
	double SumWeightThtX_ThtX;
	double SumWeightThtX_ThtY;
	double SumWeightThtX_ThtZ;
	double SumWeightThtY_ThtX;
	double SumWeightThtY_ThtY;
	double SumWeightThtY_ThtZ;
	double SumWeightThtZ_ThtX;
	double SumWeightThtZ_ThtY;
	double SumWeightThtZ_ThtZ;
	
	double SumWeight;

	// Mass selector
	RhoMassParticleSelector* lambdaMassSelector;

	// Truthmatch filter
	int  SelectTruePid(PndAnalysis *ana, RhoCandList &l);

	int  SelectMomentum(PndAnalysis *ana, RhoCandList &l, Double_t min, Double_t max);

	void VertexFit(RhoCandList &l);

	void MassFit(RhoCandList &l, Double_t mass);

	void Tree4CFit(RhoCandList &l, TLorentzVector ini, Double_t prob_cut);

    std::map<int,int> VertexQaIndex(RhoCandList* candList, Double_t prob_cut);

    std::map<int,int> MassFitQaIndex(RhoCandList* candList, Double_t m0, Double_t prob_cut);

    void Pull(TString pre, RhoTuple *t, RhoCandidate *c, RhoCandidate *fit);

    int tagHits(RhoCandidate *c);

    int trackBranch(RhoCandidate *c);

	RhoTuple *fntpPiMinus;
	RhoTuple *fntpPiPlus;
	RhoTuple *fntpProton;
	RhoTuple *fntpAntiProton;

	RhoTuple *fntpLambda;
	RhoTuple *fntpLambdaBar;

	RhoTuple *fntpPbarp;
	RhoTuple *fntpPbarp4Cfit;

	RhoTuple *fntpMCtruth;
/*
	RhoTuple *fntp_MC_PiMinus;
	RhoTuple *fntp_MC_PiPlus;
	RhoTuple *fntp_MC_Proton;
	RhoTuple *fntp_MC_AntiProton;

	RhoTuple *fntp_MC_Lambda;
	RhoTuple *fntp_MC_LambdaBar;
*/
	// *** the PndAnalysis object
	PndAnalysis *theAnalysis;

	// *** Get parameter containers
	virtual void SetParContainers();
		
	ClassDef(PndLLbarAnaTask,1);
  
};

#endif
