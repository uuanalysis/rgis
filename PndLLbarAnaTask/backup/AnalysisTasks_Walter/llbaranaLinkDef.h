#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
 
#pragma link C++ class  PndLLbarAnaTask+;
#pragma link C++ class  PndLLbarAnaTaskIdeal+;
#pragma link C++ class  PndLLbarAnaTask_DecayTreeFit+;
#pragma link C++ class  PndLLbarTesting+;
#pragma link C++ class  PndXiXibarAnaTask+;
#pragma link C++ class  PndXiXibarAnaTask_DecayTreeFit+;

#endif
