/*
* PndLLbarAnaRGISTask.cxx
*
*	Created on: October 1, 2020
*  		Author: Adeel Akram
*/
// Task Header
#include "PndLLbarAnaTaskRGIS.h"

// C++ Headers
#include <string>
#include <iostream>

// FAIR Headers
#include "FairRootManager.h"
#include "FairRunAna.h"
#include "FairRuntimeDb.h"
#include "FairRun.h"
#include "FairRuntimeDb.h"

// ROOT Headers
#include "TClonesArray.h"
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TParticlePDG.h"

// PandaRoot Headers
#include "PndAnalysis.h"
#include "PndPidCandidate.h"

// RHO Headers
#include "RhoCandidate.h"
#include "RhoHistogram/RhoTuple.h"
#include "RhoFactory.h"
#include "RhoMassParticleSelector.h"
#include "RhoDecayTreeFitter.h"
#include "PndRhoTupleQA.h"
#include "Rho4CFitter.h"
#include "RhoKinVtxFitter.h"
#include "RhoKinFitter.h"
#include "RhoKinHyperonVtxFitter.h"
#include "RhoKinHyperonFitter.h"
#include "RhoVtxPoca.h"


PndLLbarAnaTaskRGIS::PndLLbarAnaTaskRGIS() : FairTask("PndLLbarAnaTaskRGISIdeal"), fBeamMom (1.642), fIsBestPID(true) {

	// Event Counter
	fEvtCount = 0;
	fMass0_pi = 0.0;
	fMass0_proton = 0.0;
	fMass0_lam = 0.0;

	// Rho Tuples
	fntpPiMinus = NULL;
	fntpPiPlus = NULL;
	fntpProton = NULL;
	fntpAntiProton = NULL;
	fntpLambda = NULL;
	fntpLambdaBar = NULL;
	fntpPbarP = NULL;
	fntpBestPbarP = NULL;

	// Rho Analysis
	fAnalysis = NULL;

	// Rho Mass Selector
	fLambdaMassSel = NULL;

	// PDG Database
	fPdgCode = NULL;

}

PndLLbarAnaTaskRGIS::~PndLLbarAnaTaskRGIS() {
}

void PndLLbarAnaTaskRGIS::SetParContainers()
{
  // Get run and runtime database
  FairRun* run = FairRun::Instance();
  if (! run) Fatal("SetParContainers", "No analysis run");
}

InitStatus PndLLbarAnaTaskRGIS::Init() {

	fEvtCount = 0;  // Reset the event counter
	fAnalysis = new PndAnalysis();  // initialize analysis object
	fPdgCode  = new TDatabasePDG();  // PDG database

	// Particle Masses (pion, proton, lambda)
	fMass0_pi = ((TParticlePDG*)fPdgCode->GetParticle(-211))->Mass();
	fMass0_proton = ((TParticlePDG*)fPdgCode->GetParticle(-2212))->Mass();
	fMass0_lam 	= ((TParticlePDG*)fPdgCode->GetParticle(-3122))->Mass();

	// Cut on Invariant Mass: RhoMassParticleSelector("particle", center, width)
	fLambdaMassSel = new RhoMassParticleSelector("lambda0", fMass0_lam, 0.3);  // +-0.05 GeV width

	// +++------------------------------------------------------------------+++
	// 								   TLorentzVector
	// +++------------------------------------------------------------------+++
	// *** 4-momentum (p4) lorentz vector of the initial pbarpsystem (p4_ini = p4_beam + p4_target)
	// *** The initial Lorentz vector (px, py, pz, E) have to be conserved indiviually.

	// fIni = (p_beam + p_target, E_beam + E_target) = (0, 0, pz_beam, 1.891 + 0.938) = (0, 0, 1.642, 2.829)
	fIni = TLorentzVector(0., 0., fBeamMom, 2.829);

	//Double_t fBeamEnergy = sqrt(pow(fBeamMom, 2) + pow(fMass0_proton, 2));
	//Double_t fTargetEnergy = sqrt(0. + pow(fMass0_proton, 2));
	//fIni = TLorentzVector(0., 0., fBeamMom, (fBeamEnergy + fTargetEnergy));   // 2.829


	// +++------------------------------------------------------------------+++
	// 								    RhoTuples
	// +++------------------------------------------------------------------+++
	
	// *** 
	// *** RhoTuples (RhoCandidate)
	// *** 
	fntpPiMinus    = new RhoTuple("ntpPiMinus", "PiMinus Info.");                      // Before Mass Sel.
	fntpPiPlus 	   = new RhoTuple("ntpPiPlus", "PiPlus Info.");                        // Before Mass Sel.
	fntpProton	   = new RhoTuple("ntpProton", "Proton Info.");                        // Before Mass Sel.
	fntpAntiProton = new RhoTuple("ntpAntiProton", "AntiProton Info.");                // Before Mass Sel.
	fntpLambda	   = new RhoTuple("ntpLambda", "Lambda Info.");                        // After Vertex Fit
	fntpLambdaBar  = new RhoTuple("ntpLambdaBar", "LambdaBar Info.");                  // After Vertex Fit
	fntpPbarP	   = new RhoTuple("ntpPbarP", "PbarPSystem Info.");                    // After Vertex Fit
	fntpBestPbarP  = new RhoTuple("ntpBestPbarP", "Best PbarPSystem Info.");           // After 4C Fit
	
	// *** 
	// *** Histograms (4-momentum)
	// *** 
	//hpiminus_mom 	= new TH1F("hpiminus_mom","#pi^{-} momentum",300,0.,3.);           //piminus hist.
	//hpiplus_mom 	= new TH1F("hpiplus_mom","#pi^{+} momentum",300,0.,3.);            //piplus hist.
	//hproton_mom 	= new TH1F("hproton_mom","p momentum",300,0.,3.);                  //proton hist.
	//hantiproton_mom = new TH1F("hantiproton_mom","#bar{p} momentum",300,0.,3.);      //antiproton hist.
	//hlam0_mom 	= new TH1F("hlam0_mom","#Lambda_{0} momentum",300,0.,3.);          //lam0 hist.
	//hlam0bar_mom  = new TH1F("hlam0bar_mom","#bar{#Lambda_{0}} momentum",300,0.,3.); //lam0bar hist.
    
    // ***
    // *** Set PID Criteria
    // ***
	if(fIsBestPID)
		std::cout << "-I- Using Best PID Criteria" << std::endl;
	else
		std::cout << "-I- Using Tight PID Criteria" << std::endl;
	
	
	std::cout << "PndLLbarAnaTask - Initialization successful!" << std::endl;
	return kSUCCESS;
	
}// end Init()

void PndLLbarAnaTaskRGIS::Exec(Option_t*) {
	
	// necessary to read the next event
	fAnalysis->GetEventInTask();  				// instead of GetEvent() used in the macro

	// *** Counter
	++fEvtCount;
	
	//if ((fEvtCount%100)==0) std::cout << "-I- Processing Event: "<< fEvtCount << std::endl;
	//if (!(fEvtCount%100))   std::cout << "-I- Processing Event: "<< fEvtCount << std::endl;
	std::cout << "\n-I- Processing Event: " << fEvtCount << std::endl << std::endl;
	
	// *** RhoTupleQA
	PndRhoTupleQA qa(fAnalysis, 1.642);
	
	// *** RhoCandLists
	RhoCandList p, pbar, piplus, piminus, lam0, lam0bar, pbarpsystem, mclist;
	RhoCandList lam0_best, lam0bar_best,  pbarpsystem_best;
	
	// ***
	// *** Select with ideal PID info
	// ***
	if(fIsBestPID) 
	{			
		fAnalysis->FillList(p,"ProtonBestPlus","PidAlgoIdealCharged");
		fAnalysis->FillList(pbar,"ProtonBestMinus","PidAlgoIdealCharged");
		fAnalysis->FillList(piplus,"PionBestPlus","PidAlgoIdealCharged");
		fAnalysis->FillList(piminus,"PionBestMinus","PidAlgoIdealCharged");
		fAnalysis->FillList(mclist,"McTruth");
	}
	else 
	{
		fAnalysis->FillList(p,"ProtonTightPlus","PidAlgoIdealCharged");
		fAnalysis->FillList(pbar,"ProtonTightMinus","PidAlgoIdealCharged");
		fAnalysis->FillList(piplus,"PionTightPlus","PidAlgoIdealCharged");
		fAnalysis->FillList(piminus,"PionTightMinus","PidAlgoIdealCharged");
		fAnalysis->FillList(mclist,"McTruth");
	}
	
	// Counters: p, pbar, piplus and piminus
	p_counter += p.GetLength();
	pbar_counter += pbar.GetLength();
	piplus_counter += piplus.GetLength();
	piminus_counter += piminus.GetLength();
		

	// ***
	// *** Combinatorics
	// ***
	lam0.Combine(p, piminus);
	lam0.SetType(3122);
	lam0bar.Combine(pbar, piplus);	
	lam0bar.SetType(-3122);
	//pbarpsystem.Combine(lam0, lam0bar);
	//pbarpsystem.SetType(88888);
	
	// ***
	// *** LambdaMassSel
	// ***
	std::cout << "-I- Starting Mass Selection." << std::endl;
	lam0.Select(fLambdaMassSel);
	lam0bar.Select(fLambdaMassSel);
	//pbarpsystem.Cleanup();
	pbarpsystem.Combine(lam0, lam0bar);
	pbarpsystem.SetType(88888);
	
	// Counters: lam0, lam0bar, pbarpsystem
	lam0_counter += lam0.GetLength();
	lam0bar_counter += lam0bar.GetLength();
	pbarp_counter += pbarpsystem.GetLength();
				
	// +++------------------------------------------------------------------+++
	// 								Writting RhoTuples
	// +++------------------------------------------------------------------+++
	
	// ***
	// ***	RhoTuple: Pion-
	// ***
	for (int j=0; j < piminus.GetLength(); ++j) {
	
		TLorentzVector piminus_4mom = piminus[j]->P4();
		//qa.qaP4("piminus_", piminus_4mom, fntpPiMinus);
		qa.qaCand("piminus_", piminus[j], fntpPiMinus);
		//qa.qaMcDiff("piminus_", piminus[j], fntpPiMinus);
		fntpPiMinus->DumpData();
		
		// Fill Histogram
		//hpiminus_mom->Fill(piminus_4mom.P());
	}
	
	// ***
	// ***	RhoTuple: Pion+
	// ***
	for (int j=0; j < piplus.GetLength(); ++j) {
	
		TLorentzVector piplus_4mom = piplus[j]->P4();
		//qa.qaP4("piplus_", piplus_4mom, fntpPiPlus);
		qa.qaCand("piplus_", piplus[j], fntpPiPlus);
		//qa.qaMcDiff("piplus_", piplus[j], fntpPiPlus);
		fntpPiPlus->DumpData();
		
		// Fill Histogram
		//hpiplus_mom->Fill(piplus_4mom.P());
	}
	
	// ***
	// ***	RhoTuple: Proton
	// ***
	for (int j=0; j < p.GetLength(); ++j) {
	
		TLorentzVector p_4mom = p[j]->P4();
		//qa.qaP4("proton_", p_4mom, fntpProton);
		qa.qaCand("proton_", p[j], fntpProton);
		//qa.qaMcDiff("proton_", p[j], fntpProton);
		fntpProton->DumpData();
		
		// Fill Histogram
		//hproton_mom->Fill(p_4mom.P());
		
	}

	// ***
	// ***	RhoTuple: AntiProton
	// ***
	for (int j=0; j<pbar.GetLength(); ++j) {
	
		TLorentzVector pbar_4mom = pbar[j]->P4();
		//qa.qaP4("antiproton_", pbar_4mom, fntpAntiProton);
		qa.qaCand("antiproton_", pbar[j], fntpAntiProton);
		//qa.qaMcDiff("antiproton_", pbar[j], fntpAntiProton);
		fntpAntiProton->DumpData();
		
		// Fill Histogram
		//hantiproton_mom->Fill(pbar_4mom.P());
	}
	
	
	// +++------------------------------------------------------------------+++
	// 					   Vertex Fitting (Lambda0, Lambda0Bar)
	// +++------------------------------------------------------------------+++
	
	// ***
	// *** do VERTEX FIT (lam0)
	// ***
	std::cout << "-I- Starting Vertex Fit with Lambda0     : " << lam0.GetLength() << std::endl;
	
	std::map<int, int> indexBestFitLam0;
	indexBestFitLam0 = VertexQaIndex(&lam0, 0.01);
	
	for (int j=0; j<lam0.GetLength(); ++j) {
		RhoKinVtxFitter vtxfitter(lam0[j]);
		vtxfitter.Fit();
		
		if (indexBestFitLam0[j]==1) {
				
			// Writting RhoTuple
			qa.qaFitter("FourMomFit_",&vtxfitter,fntpLambda,false);
			qa.qaCand("lambda_",lam0[j]->GetFit(),fntpLambda);
			fntpLambda->DumpData();
 		    
 		    // lam0_best
			lam0_best.Append(lam0[j]->GetFit());
			
			// Counter: lam0_vf (~lam0_best)
			lam0_vf_counter += lam0_best.GetLength();
		}
		
	}//VF
	
	// ***
	// *** TODO: RhoTuple: Lambda (-> Proton Pion-) loop
	// ***
	/*
	for (int j=0; j<lam0_best.GetLength(); ++j) {
	
	    TLorentzVector lam0_best_4mom = lam0_best[j]->P4();
		//qa.qaP4("lambda_", lam0_best_4mom, fntpLambda);
		qa.qaCand("lambda_", lam0_best[j], fntpLambda);
		//qa.qaMcDiff("lambda_", lam0_best[j], fntpLambda);
		fntpLambda->DumpData();
		
		// Fill Histogram
		hlam0_mom->Fill(lam0_best_4mom.P());
	}
	*/
		
	// ***
	// *** TODO: do MASS CONSTRAINT FIT (lam0)
	// ***
	/*
	for (int j=0; j<lam0.GetLength(); ++j) {
		RhoKinFitter mfitter(lam0[j]);					// instantiate the RhoKinFitter in psi(2S)
		mfitter.AddMassConstraint(fMass0_lam);			// add the mass constraint
		mfitter.Fit();									// do fit

		double chi2_m = mfitter.GetChi2();				// get chi2 of fit
		double prob_m = mfitter.GetProb();				// access probability of fit
		hlam0_chi2_mf->Fill(chi2_m);
		hlam0_prob_mf->Fill(prob_m);

		if ( prob_m > 0.01 ) {							// when good enough, fill some histo
			RhoCandidate *jfit = lam0[j]->GetFit();		// access the fitted cand
			hlam0_mass_mf->Fill(jfit->M());
		}
	}
	*/
	
	
	// ***
	// *** do VERTEX FIT (lam0bar)
	// ***
	std::cout << "-I- Starting Vertex Fit with Lambda0Bar  : " << lam0bar.GetLength() << std::endl;
	
	std::map<int, int> indexBestFitLam0Bar;
	indexBestFitLam0Bar = VertexQaIndex(&lam0bar, 0.01);

	for (int j=0; j<lam0bar.GetLength(); ++j) {

		RhoKinVtxFitter vtxfitter(lam0bar[j]);
		vtxfitter.Fit();

		if (indexBestFitLam0Bar[j]==1) {
		
			// Writting RhoTuple
			qa.qaFitter("FourMomFit_",&vtxfitter,fntpLambdaBar,false);
			qa.qaCand("lambdabar_",lam0bar[j]->GetFit(),fntpLambdaBar);
			fntpLambdaBar->DumpData();
			
			// lam0bar_best
			lam0bar_best.Append(lam0bar[j]->GetFit());
			
			// Counter: lam0bar_best (~ lam0bar_vf)
			lam0bar_vf_counter += lam0bar_best.GetLength();
		}
		
	}//VF
	
	
	// ***
	// *** TODO: RhoTuple: LambdaBar (-> AntiProton Pion+) loop
	// ***
	/*
	for (int j=0; j<lam0bar_best.GetLength(); ++j) {
		
		TLorentzVector lam0bar_best_4mom = lam0bar_best[j]->P4();
		//qa.qaP4("lambdabar_", lam0bar_best_4mom, fntpLambdaBar);
		qa.qaCand("lambdabar_", lam0bar_best[j], fntpLambdaBar);
		//qa.qaMcDiff("lambdabar_", lam0bar_best[j], fntpLambdaBar);
		fntpLambdaBar->DumpData();
		
		// Fill Histogram
		hlam0_mom->Fill(lam0bar_best_4mom.P());
	}
	*/
	
	// ***
	// *** TODO: do MASS CONSTRAINT FIT (lam0bar)
	// ***
	/*
	for (int j=0; j<lam0bar.GetLength(); ++j) {
		RhoKinFitter mfitter(lam0bar[j]);				// instantiate the RhoKinFitter in psi(2S)
		mfitter.AddMassConstraint(fMass0_lam);			// add the mass constraint
		mfitter.Fit();									// do fit

		double chi2_m = mfitter.GetChi2();				// get chi2 of fit
		double prob_m = mfitter.GetProb();				// access probability of fit
		hlam0bar_chi2_mf->Fill(chi2_m);
		hlam0bar_prob_mf->Fill(prob_m);

		if (prob_m > 0.01) {							// when good enough, fill some histo
			RhoCandidate *jfit = lam0bar[j]->GetFit();	// access the fitted cand
			hlam0bar_mass_mf->Fill(jfit->M());
		}
	}
	*/	
	
	pbarpsystem_best.Combine(lam0_best, lam0bar_best);
	pbarpsystem_best.SetType(88888);
	
	// ***
	// *** RhoTuple: pbarpsystem_best
	// ***
	for (int j=0; j<pbarpsystem_best.GetLength(); ++j) {
		qa.qaCand("pbarp_", pbarpsystem_best[j], fntpPbarP);
		//qa.qaComp("pbarp_", pbarpsystem_best[j], fntpPbarP, true, true);
		fntpPbarP->DumpData();
	}
	
	// Counter: pbarpsystem_best (~pbarpsystem_vf)
	pbarp_vf_counter +=pbarpsystem_best.GetLength();
	
				
	// +++------------------------------------------------------------------+++
	// 								4C Fitting (PbarPSystem)
	// +++------------------------------------------------------------------+++
	std::cout << "-I- Starting 4C Fit with pbarpsystem_best: " << pbarpsystem_best.GetLength() << std::endl;
		
	for (int j=0; j<pbarpsystem_best.GetLength(); ++j) {
		
		RhoKinFitter fitter(pbarpsystem_best[j]);					// instantiate the kin fitter in psi(2S)
		fitter.Add4MomConstraint(fIni);				    		// set 4 constraint
		bool IsGoodFit = fitter.Fit();		            		// do fit
		
		// Counter: All 4C Events
		++all4Ccount;
		
		if (IsGoodFit) {
			//if (prob_4c > 0.01) {
			
			// Fitted Candidate
			RhoCandidate *jfit = pbarpsystem_best[j]->GetFit();
			
			// Fill RhoTuple
			fntpBestPbarP->Column("McTruthMatch", (Bool_t) fAnalysis->McTruthMatch(pbarpsystem_best[j]));
			fntpBestPbarP->Column("FourMomFit_Convergence",    (Bool_t) IsGoodFit);
			qa.qaFitter("FourMomFit_",&fitter,fntpBestPbarP,false);
			//qa.qaCand("pbarp_", jfit, fntpBestPbarP);
			qa.qaComp("pbarp_", jfit, fntpBestPbarP, true, true);
			fntpBestPbarP->DumpData();
				
			//}
			
			// Counter: Good Fit Events
			++goodfit;
			
			std::cout << "-I- IsGoodFit (4C) Condition Has Been Passed." << std::endl;
		}
		else
			std::cout << "-I- IsGoodFit (4C) Condition Has Been Failed." << std::endl;
		
	}//4C
	
	// ***
	// ***	RhoTuple: PbarPsystem (-> Lambda LambdaBar) loop
	// ***
	/*
	for (int j=0; j<pbarpsystem_best.GetLength(); ++j) {
		qa.qaCand("pbarp_", pbarpsystem_best[j], fntpBestPbarP);
		//qa.qaComp("pbarp_", pbarpsystem_best[j], fntpBestPbarP, true, true);
		fntpBestPbarP->DumpData();
	}
	*/
	
	
	// Clean to reuse.
	p.Cleanup();
	piplus.Cleanup();
	pbar.Cleanup();
	piminus.Cleanup();
	
	lam0.Cleanup();
	lam0bar.Cleanup();
	pbarpsystem.Cleanup();
	
	lam0_best.Cleanup();
	lam0bar_best.Cleanup();
	pbarpsystem_best.Cleanup();
	
	std::cout << "-I- Event Has Been Processed." << std::endl;

}//end Exec()


void PndLLbarAnaTaskRGIS::Finish() {
	
	// Summary
	std::cout << std::endl;
	std::cout << "+++-------------------------------------------------------+++" << std::endl;
	std::cout << "Total No. of Events  : " << fEvtCount << std::endl;
	std::cout << "Total No. of p       : " << p_counter       << std::endl;
	std::cout << "Total No. of pbar    : " << pbar_counter    << std::endl;
	std::cout << "Total No. of piplus  : " << piplus_counter  << std::endl;
	std::cout << "Total No. of piminus : " << piminus_counter << std::endl;
	std::cout << std::endl;
	std::cout << "Total No. of lam0 (After MS)   : " << lam0_counter << std::endl;
	std::cout << "Total No. of lam0bar (After MS): " << lam0bar_counter << std::endl;
	std::cout << "Total No. of pbarp (After MS)  : " << pbarp_counter << std::endl;
	std::cout << std::endl;
	std::cout << "Total No. of lam0 (After VF)   : " << lam0_vf_counter << std::endl;
	std::cout << "Total No. of lam0bar (After VF): " << lam0bar_vf_counter << std::endl;
	std::cout << "Total No. of pbarp (After VF)  : " << pbarp_vf_counter << std::endl;
	std::cout << std::endl;
	std::cout << "Total No. of Events       : " << fEvtCount << std::endl;
	std::cout << "Events Passed To 4C Fit   : " << all4Ccount << std::endl;
	std::cout << "Events Passed TO IsGoodFit: " << goodfit << std::endl;
	std::cout << "Efficiency (w/ IsGoodFit) : " << ((float)goodfit/(float)fEvtCount)*100. << "%" << std::endl;
	std::cout << "+++-------------------------------------------------------+++" << std::endl;
	std::cout << std::endl;
	
	
	// ********************************************************************
	// *** 			       write out all the tuples		    			***
	// ********************************************************************
	
	// *** 
	// *** RhoTuples: Particles
	// ***
	std::cout << "-I- Started Writing RhoTuples (in Order)." << std::endl;
	
	fntpPiMinus->GetInternalTree()->Write();
	fntpPiPlus->GetInternalTree()->Write();
	fntpProton->GetInternalTree()->Write();
	fntpAntiProton->GetInternalTree()->Write();
	fntpLambda->GetInternalTree()->Write();
	fntpLambdaBar->GetInternalTree()->Write();
	//fntpPbarP->GetInternalTree()->Write();
	fntpBestPbarP->GetInternalTree()->Write();
	
	// *** 
	// *** Histograms: 4 Momentum
	// ***
	//hpiminus_mom->Write();
	//hpiplus_mom->Write();
	//hproton_mom->Write();
	//hantiproton_mom->Write();
		
	std::cout << "-I- Finished Writing Tuples and Histograms." << std::endl;

}// end FinishTask()


// Routine to only keep PID matched candidates in list
int PndLLbarAnaTaskRGIS::SelectTruePid(PndAnalysis *ana, RhoCandList &l) {

	int removed = 0;
	for (int ii=l.GetLength()-1; ii>=0; --ii) {
		if (!(ana->McTruthMatch(l[ii]))) {
			l.Remove(l[ii]);
			removed++;
		}
	}

	return removed;
}//end SelectTruePid()

// *** function to filter best rhocandidates.
std::map<int,int> PndLLbarAnaTaskRGIS::VertexQaIndex(RhoCandList* candList, Double_t prob_cut) {
    /** @brief  give back the order of the best chi2
     * @details give back the order of the best chi2!  1 means best, 2: second best (same with negative values for bad chi2 )
     * @details: Credit to Jenny Puetz for the code
     */
    std::map<double, int> chi2_good, chi2_bad;
    
    for (int j=0; j<candList->GetLength(); ++j) {
    
        RhoKinVtxFitter vtxfitter(candList->Get(j));
        vtxfitter.Fit();
        
        bool failedchi2 = TMath::IsNaN(vtxfitter.GetChi2());
        bool failedprob = TMath::IsNaN(vtxfitter.GetProb());
        
        if(!failedchi2 && !failedprob) {
            if (vtxfitter.GetProb() > prob_cut) { //Prob > 0.01
                chi2_good[vtxfitter.GetChi2()]=j;
            }
            else { //Prob <= 0.01
                chi2_bad[vtxfitter.GetChi2()]=j;
            }
        }
    }
    
    std::map<double, int>::iterator is_good, is_bad;
    std::map<int, int> indexBestFit;
    int running = 0;
    
    for (is_good = chi2_good.begin(); is_good != chi2_good.end(); is_good++, running++) {
        indexBestFit[is_good->second] = running + 1;
    }
    
    running =0;
    
    for (is_bad = chi2_bad.begin(); is_bad != chi2_bad.end(); is_bad++, running++) {
        indexBestFit[is_bad->second] = - (running + 1);
    }
    
    return indexBestFit;
    
}//end

ClassImp(PndLLbarAnaTaskRGIS)
