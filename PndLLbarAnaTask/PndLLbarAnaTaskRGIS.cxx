/*
 * PndLLbarAnaRGISTask.cxx
 *
 *	Created on: October 1, 2020
 *  		Author: Adeel Akram
 */
// Task Header
#include "PndLLbarAnaTaskRGIS.h"

// C++ Headers
#include <string>
#include <iostream>

// FAIR Headers
#include "FairRootManager.h"
#include "FairRunAna.h"
#include "FairRuntimeDb.h"
#include "FairRun.h"
#include "FairRuntimeDb.h"

// ROOT Headers
#include "TClonesArray.h"
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TParticlePDG.h"

// PandaRoot Headers
#include "PndAnalysis.h"
#include "PndPidCandidate.h"

// RHO Headers
#include "RhoCandidate.h"
#include "RhoHistogram/RhoTuple.h"
#include "RhoFactory.h"
#include "RhoMassParticleSelector.h"
#include "RhoDecayTreeFitter.h"
#include "PndRhoTupleQA.h"
#include "Rho4CFitter.h"
#include "RhoKinVtxFitter.h"
#include "RhoKinFitter.h"
#include "RhoKinHyperonVtxFitter.h"
#include "RhoKinHyperonFitter.h"
#include "RhoVtxPoca.h"


PndLLbarAnaTaskRGIS::PndLLbarAnaTaskRGIS() : FairTask("PndLLbarAnaTaskRGISIdeal") {

    fEvtCount = 0;
    fMass0_pion = 0.0;
    fMass0_proton = 0.0;
    fMass0_lam0 = 0.0;

    fBeamMom=1.642; 
    fSignalSample=true;
    fIsBestPID=true;

    fAnalysis = nullptr;
    fLambdaMassSel = nullptr;
    fPdgCode = nullptr;

    fntpPiMinus = nullptr;
    fntpPiPlus = nullptr;
    fntpProton = nullptr;
    fntpAntiProton = nullptr;
    fntpLambda = nullptr;
    fntpLambdaBar = nullptr;
    fntpPbarP = nullptr;
    fntpBestPbarP = nullptr;

}

PndLLbarAnaTaskRGIS::~PndLLbarAnaTaskRGIS() {
}

void PndLLbarAnaTaskRGIS::SetParContainers()
{
    // Get run and runtime database
    FairRun* run = FairRun::Instance();
    if (! run) Fatal("SetParContainers", "No analysis run");
}

InitStatus PndLLbarAnaTaskRGIS::Init() {

    fEvtCount = 0;  // Reset the event counter
    fAnalysis = new PndAnalysis();  // initialize analysis object
    fPdgCode  = new TDatabasePDG();  // PDG database

    // Particle Masses (pion, proton, lambda)
    fMass0_pion = ((TParticlePDG*)fPdgCode->GetParticle(-211))->Mass();
    fMass0_proton = ((TParticlePDG*)fPdgCode->GetParticle(-2212))->Mass();
    fMass0_lam0 = ((TParticlePDG*)fPdgCode->GetParticle(-3122))->Mass();

    // Cut on Invariant Mass: RhoMassParticleSelector("particle", center, width)
    fLambdaMassSel = new RhoMassParticleSelector("lambda0", fMass0_lam0, 0.3);  // +-0.05 GeV width

    // +++------------------------------------------------------------------+++
    //                             TLorentzVector
    // +++------------------------------------------------------------------+++
    /* 
    *
    * 4-momentum (p4) lorentz vector of the initial pbarpsystem (p4_ini = p4_beam + p4_target)
    * The initial Lorentz vector (px, py, pz, E) have to be conserved indiviually. That is
    * fIni = (p_beam + p_target, E_beam + E_target)
    * fIni = (0, 0, pz_beam, 1.891 + 0.938) = (0, 0, 1.642, 2.829)
    */

    // fIni = TLorentzVector(0., 0., fBeamMom, 2.829);

    // OR

    //Double_t fBeamEnergy = sqrt(fBeamMom*fBeamMom + fMass0_proton*fMass0_proton);
    //Double_t fTargetEnergy = sqrt(0. + fMass0_proton*fMass0_proton);
    //fIni = TLorentzVector(0., 0., fBeamMom, (fBeamEnergy + fTargetEnergy));   // 2.829

    // OR

    //*** Start: Walter Added
    fTarg.SetXYZT(0, 0, 0, fMass0_proton);
    fBeam.SetXYZT(0, 0, fBeamMom, sqrt(fMass0_proton*fMass0_proton + fBeamMom*fBeamMom));

    // fIni = Target + Beam
    fIni = fTarg+fBeam;

    // Lambda decay parameters
    alpha_lam = 0.732;
    alpha_lambar = -0.732;

    // *** End: Walter Added
    
    // +++------------------------------------------------------------------+++
    //                                  RhoTuples
    // +++------------------------------------------------------------------+++

    // *** 
    // *** RhoTuples (RhoCandidate)
    // *** 
    fntpPiMinus    = new RhoTuple("ntpPiMinus", "PiMinus Info.");                      // Before Mass Sel.
    fntpPiPlus 	   = new RhoTuple("ntpPiPlus", "PiPlus Info.");                        // Before Mass Sel.
    fntpProton	   = new RhoTuple("ntpProton", "Proton Info.");                        // Before Mass Sel.
    fntpAntiProton = new RhoTuple("ntpAntiProton", "AntiProton Info.");                // Before Mass Sel.
    fntpLambda	   = new RhoTuple("ntpLambda", "Lambda Info.");                        // After Vertex Fit
    fntpLambdaBar  = new RhoTuple("ntpLambdaBar", "LambdaBar Info.");                  // After Vertex Fit
    fntpPbarP	   = new RhoTuple("ntpPbarP", "PbarPSystem Info.");                    // After Vertex Fit
    fntpBestPbarP  = new RhoTuple("ntpBestPbarP", "Best PbarPSystem Info.");           // After 4C Fit

    //Walter added, MC tuples
    fntpMCTruth = new RhoTuple("ntpMCTruth", "MC truth info");

    // *** 
    // *** Histograms (4-momentum)
    // *** 
    hpiminus_mom 	= new TH1F("hpiminus_mom","#pi^{-} momentum",300,0.,3.);           //piminus hist.
    hpiplus_mom 	= new TH1F("hpiplus_mom","#pi^{+} momentum",300,0.,3.);            //piplus hist.
    hproton_mom 	= new TH1F("hproton_mom","p momentum",300,0.,3.);                  //proton hist.
    hantiproton_mom = new TH1F("hantiproton_mom","#bar{p} momentum",300,0.,3.);        //antiproton hist.
    //hlam0_mom 	    = new TH1F("hlam0_mom","#Lambda_{0} momentum",300,0.,3.);      //lam0 hist.
    //hlam0bar_mom    = new TH1F("hlam0bar_mom","#bar{#Lambda_{0}} momentum",300,0.,3.); //lam0bar hist.

    // ***
    // *** Set PID Criteria
    // ***
    if(fIsBestPID)
        std::cout << "-I- Using Best PID Criteria" << std::endl;
    else
        std::cout << "-I- Using Tight PID Criteria" << std::endl;


    std::cout << "-I- PndLLbarAnaTaskRGIS::Initialization Successful!" << std::endl;
    return kSUCCESS;

}// end Init()

void PndLLbarAnaTaskRGIS::Exec(Option_t*) {

    // *** Read Event
    fAnalysis->GetEventInTask();                // instead of GetEvent() used in the macro

    // *** Set Counter
    ++fEvtCount;

    //if ((fEvtCount%100)==0) std::cout << "-I- Processing Event: "<< fEvtCount << std::endl;
    //if (!(fEvtCount%100))   std::cout << "-I- Processing Event: "<< fEvtCount << std::endl;
    //	std::cout << "\n-I- Processing Event: " << fEvtCount << std::endl << std::endl;

    // *** RhoTupleQA
    PndRhoTupleQA qa(fAnalysis, 1.642);

    // *** RhoCandLists
    RhoCandList p, pbar, piplus, piminus, lam0, lam0bar, pbarpsystem, mclist;
    RhoCandList lam0_best, lam0bar_best,  pbarpsystem_best;

    // ***
    // -----  Select with ideal PID info
    // ***
    if(fIsBestPID) 
    {
	    fAnalysis->FillList(p,"ProtonBestPlus","PidAlgoIdealCharged");
	    fAnalysis->FillList(pbar,"ProtonBestMinus","PidAlgoIdealCharged");
	    fAnalysis->FillList(piplus,"PionBestPlus","PidAlgoIdealCharged");
	    fAnalysis->FillList(piminus,"PionBestMinus","PidAlgoIdealCharged");
	    fAnalysis->FillList(mclist,"McTruth");
    }
    else 
    {
	    fAnalysis->FillList(p,"ProtonTightPlus","PidAlgoIdealCharged");
	    fAnalysis->FillList(pbar,"ProtonTightMinus","PidAlgoIdealCharged");
	    fAnalysis->FillList(piplus,"PionTightPlus","PidAlgoIdealCharged");
	    fAnalysis->FillList(piminus,"PionTightMinus","PidAlgoIdealCharged");
	    fAnalysis->FillList(mclist,"McTruth");
    }

    // *** Select RhoCandidates with MCTruth Match (not for RGIS)
    //SelectTruePid(fAnalysis, p);
    //SelectTruePid(fAnalysis, pbar);
    //SelectTruePid(fAnalysis, piplus);
    //SelectTruePid(fAnalysis, piminus);

    // Counters: p, pbar, piplus and piminus
    p_counter += p.GetLength();
    pbar_counter += pbar.GetLength();
    piplus_counter += piplus.GetLength();
    piminus_counter += piminus.GetLength();


    // *** Start: Walter Added

    // *** Walter Added (MC Truth)
    
    // *** --------------------------- ***
    // *** Same all Monte Carlo Tracks ***
    // *** --------------------------- ***

    /*
     * Save true MC tracks. If background sample is analysed, 
     * there is no MC truth and a dummy is used instead.
     */
     
    RhoCandidate *mclambar, *mclam, *mcpip, *mcpim, *mcp, *mcpbar; //, *mcpbarp;
    RhoCandidate *dummyCand = new RhoCandidate();

    if (fSignalSample) {
        
        //mcpbarp = mclist[0];                // --     pbarp system
        mclambar = mclist[1];                 // d0     lam0bar
        mcpbar = mclist[1]->Daughter(0);      // d0d0   pbar (antiproton)
        mcpip = mclist[1]->Daughter(1);	      // d0d1   pi+
        mclam = mclist[2];                    // d1     lam0
        mcp = mclist[2]->Daughter(0);	      // d1d0   p (proton)
        mcpim = mclist[2]->Daughter(1);	      // d1d1   pi-

    }
    else {

        //mcpbarp = dummyCand;
        mclambar = dummyCand;
        mcpbar = dummyCand;
        mcpip = dummyCand;
        mclam = dummyCand;
        mcp = dummyCand;
        mcpim = dummyCand;
    }
    
    
    /*
    * If a dpm background sample is analysed, llbar events 
    * should be removed. This is done here.
    */

    // TODO: DPM Code. Not in my case.
     
    // *** ---------------------------------------
    // *** Walter Added (Weights for Polarization)
    // ***


    // *** ----------------------------------- ***
    // *** Boost particles to relevant systems ***
    // *** ----------------------------------- ***

    //Boost beam and target to CM frame
    TLorentzVector beamCM = fBeam;
    beamCM.Boost(-fIni.BoostVector());
    TLorentzVector targCM = fTarg;
    targCM.Boost(-fIni.BoostVector());

    //Boost Lambda to CM frame
    TLorentzVector MC_P4lam_CM = mclam->P4();
    MC_P4lam_CM.Boost(-fIni.BoostVector());

    //Boost Lambdabar to CM frame
    TLorentzVector MC_P4lambar_CM = mclambar->P4();
    MC_P4lambar_CM.Boost(-fIni.BoostVector());

    //Boost proton to Lambda rest frame
    TLorentzVector MC_P4p_lamCM = mcp->P4();
    MC_P4p_lamCM.Boost(-fIni.BoostVector());
    MC_P4p_lamCM.Boost(-MC_P4lam_CM.BoostVector());

    //Boost pbar to Lambdabar rest frame
    TLorentzVector MC_P4pbar_lambarCM = mcpbar->P4();
    MC_P4pbar_lambarCM.Boost(-fIni.BoostVector());
    MC_P4pbar_lambarCM.Boost(-MC_P4lambar_CM.BoostVector());

    // *** -------------------------- ***
    // *** Construct Reference frames ***
    // *** -------------------------- ***

    //Initial frame, lab system
    std::vector <TVector3> lab_RF;
    lab_RF.push_back(TVector3(1,0,0));
    lab_RF.push_back(TVector3(0,1,0));
    lab_RF.push_back(TVector3(0,0,1));

    //Lambdabar decay frame, lambdabar in rest
    std::vector <TVector3> MC_lambar_RF = GenerateUnitVectors(beamCM,MC_P4lambar_CM,MC_P4lambar_CM);

    //Lambda decay frame, lambda in rest
    std::vector <TVector3> MC_lam_RF = GenerateUnitVectors(beamCM,MC_P4lam_CM,MC_P4lambar_CM);

    TLorentzVector MC_P4pbar_rotlambarRF = TransformCoords(lab_RF,MC_lambar_RF,MC_P4pbar_lambarCM);
    //fntpMCTruth->Column("MC_d0d0_phi", (Float_t) MC_P4pbar_rotlambarRF.Phi(),    0.0f );
    //fntpMCTruth->Column("MC_d0d0_CosTheta", (Float_t) TMath::Cos(MC_P4pbar_rotlambarRF.Theta()),    0.0f );
    //fntpMCTruth->Column("MC_d0d0_kx", (Float_t) TMath::Cos(MC_P4pbar_lambarCM.Vect().Angle(MC_lambar_RF.at(0))),  0.0f );
    fntpMCTruth->Column("MC_d0d0_ky", (Float_t) TMath::Cos(MC_P4pbar_lambarCM.Vect().Angle(MC_lambar_RF.at(1))),  0.0f );
    //fntpMCTruth->Column("MC_d0d0_kz", (Float_t) TMath::Cos(MC_P4pbar_lambarCM.Vect().Angle(MC_lambar_RF.at(2))),  0.0f );

    TLorentzVector MC_P4p_rotlamRF = TransformCoords(lab_RF,MC_lam_RF,MC_P4p_lamCM);
    //fntpMCTruth->Column("MC_d1d0_phi", (Float_t) MC_P4p_rotlamRF.Phi(),    0.0f );
    //fntpMCTruth->Column("MC_d1d0_CosTheta", (Float_t) TMath::Cos(MC_P4p_rotlamRF.Theta()),    0.0f );
    //fntpMCTruth->Column("MC_d1d0_kx", (Float_t) TMath::Cos(MC_P4p_lamCM.Vect().Angle(MC_lam_RF.at(0))),  0.0f );
    fntpMCTruth->Column("MC_d1d0_ky", (Float_t) TMath::Cos(MC_P4p_lamCM.Vect().Angle(MC_lam_RF.at(1))),  0.0f );
    //fntpMCTruth->Column("MC_d1d0_kz", (Float_t) TMath::Cos(MC_P4p_lamCM.Vect().Angle(MC_lam_RF.at(2))),  0.0f );


    /*
    * If signal sample is analysed, weights should also be calculated for each event
    * If background is analysed, all weights should be 1
    */

    Double_t cmAngle = 999.;
    //TMatrixD Cij(3,3);
    //Double_t Py = 1.;
    //Double_t PyBar = 1.;
    Double_t Py2sin = 1.;
    Double_t PyBar2sin = 1.;
    //Double_t weightPy = 1.;
    //Double_t weightPyBar = 1.;
    Double_t weightPy2sin = 1.;
    Double_t weightPyBar2sin = 1.;
    //Double_t weightCxx = 1.;
    //Double_t weightCyy = 1.;
    //Double_t weightCzz = 1.;
    //Double_t weightCxz = 1.;
    //Double_t weightCzx = 1.;
    //Double_t weightCxxData = 1.;
    //Double_t weightCyyData = 1.;
    //Double_t weightCzzData = 1.;
    //Double_t weightCxzData = 1.;
    //Double_t weightCzxData = 1.;
    //Double_t eventWeight = 1.;
    
    if(fSignalSample){
        
        /*
        * Calculate CM angle, weights, spin variables with method of moments
        */
        
        cmAngle = beamCM.Vect().Angle(MC_P4lambar_CM.Vect());
        Py2sin = TMath::Sin(2.*cmAngle);
        PyBar2sin = TMath::Sin(2.*cmAngle);
        weightPy2sin = 1 + alpha_lam*Py2sin*TMath::Cos(MC_lam_RF.at(1).Angle(MC_P4p_lamCM.Vect()));
        weightPyBar2sin = 1 + alpha_lambar*PyBar2sin*TMath::Cos(MC_lambar_RF.at(1).Angle(MC_P4pbar_lambarCM.Vect()));
        
    }



    //fntpMCTruth->Column("weight",    (Float_t) eventWeight);
    //fntpMCTruth->Column("weightPy",    (Float_t) weightPy);
    //fntpMCTruth->Column("weightPyBar",    (Float_t) weightPyBar);
    fntpMCTruth->Column("weightPy2sin",    (Float_t) weightPy2sin);
    fntpMCTruth->Column("weightPyBar2sin",    (Float_t) weightPyBar2sin);
    //fntpMCTruth->Column("weightCxx",    (Float_t) weightCxx);
    //fntpMCTruth->Column("weightCyy",    (Float_t) weightCyy);
    //fntpMCTruth->Column("weightCzz",    (Float_t) weightCzz);
    //fntpMCTruth->Column("weightCxz",    (Float_t) weightCxz);
    //fntpMCTruth->Column("weightCzx",    (Float_t) weightCzx);
    //fntpMCTruth->Column("weightCxxData",    (Float_t) weightCxxData);
    //fntpMCTruth->Column("weightCyyData",    (Float_t) weightCyyData);
    //fntpMCTruth->Column("weightCzzData",    (Float_t) weightCzzData);
    //fntpMCTruth->Column("weightCxzData",    (Float_t) weightCxzData);
    //fntpMCTruth->Column("weightCzxData",    (Float_t) weightCzxData);


    qa.qaCand("MC_d0", mclambar, fntpMCTruth);
    qa.qaP4Cms("MC_d0", mclambar->P4(), fntpMCTruth);

    qa.qaCand("MC_d0d0", mcpbar, fntpMCTruth);
    qa.qaP4Cms("MC_d0d0", mcpbar->P4(), fntpMCTruth);

    qa.qaCand("MC_d0d1", mcpip, fntpMCTruth);
    qa.qaP4Cms("MC_d0d1", mcpip->P4(), fntpMCTruth);

    qa.qaCand("MC_d1", mclam, fntpMCTruth);
    qa.qaP4Cms("MC_d1", mclam->P4(), fntpMCTruth);

    qa.qaCand("MC_d1d0", mcp, fntpMCTruth);
    qa.qaP4Cms("MC_d1d0", mcp->P4(), fntpMCTruth);

    qa.qaCand("MC_d1d1", mcpim, fntpMCTruth);
    qa.qaP4Cms("MC_d1d1", mcpim->P4(), fntpMCTruth);

    fntpMCTruth->DumpData();

    // ***
    // *** End: Walter Added
    // *** ------------------------------

    // ***
    // -----  Combinatorics
    // ***
    lam0.Combine(p, piminus);
    lam0.SetType(3122);

    lam0bar.Combine(pbar, piplus);	
    lam0bar.SetType(-3122);


    // ***
    // -----  LambdaMassSel
    // ***

    //std::cout << "-I- Starting Mass Selection." << std::endl;
    lam0.Select(fLambdaMassSel);
    lam0bar.Select(fLambdaMassSel);

    pbarpsystem.Combine(lam0, lam0bar);
    pbarpsystem.SetType(88888);


    // Counters: lam0, lam0bar, pbarpsystem
    lam0_counter += lam0.GetLength();
    lam0bar_counter += lam0bar.GetLength();
    pbarp_counter += pbarpsystem.GetLength();


    // +++------------------------------------------------------------------+++
    // 								Writting RhoTuples
    // +++------------------------------------------------------------------+++

    // see https://panda-wiki.gsi.de/foswiki/bin/view/Computing/PandaRootAnalysisJuly13#UsingNtuples
    // for details on qaP4(), qaP4Cms, qaCand(), qaComp(), qaMcDiff() functions.

    // qaCand() ~ qaP4() + qaPos() + charge + pdgcode
    // qaComp() ~ qaCand & qaP4Cms for c,  qaCand & qaPid for d
    // qaComp() ~ {qaP4() + qaPos() + charge + pdgcode + qaP4Cms} for c,  
    //            {qaP4() + qaPos() + charge + pdgcode & qaPid} for d

    // qaMcDiff(): mcdiffP4 = c->P4() - mct->P4(), mcdiffPosition = c->GetPosition() - mct->GetPosition();, etc.


    // ***
    // -----  RhoTuple: Pion-
    // ***
    for (int j=0; j < piminus.GetLength(); ++j) {
        
        TLorentzVector piminus_4mom = piminus[j]->P4();
        //qa.qaP4("piminus_",piminus_4mom,fntpPiMinus);
        //qa.qaP4Cms("piminus_",piminus_4mom,fntpPiMinus);
        //qa.qaCand("piminus_",piminus[j],fntpPiMinus);
        
        // Let's use qaComp() instead.
        qa.qaComp("piminus_",piminus[j],fntpPiMinus,false,false);
        qa.qaMcDiff("piminus_",piminus[j],fntpPiMinus);
        
        // Fill NTuple
        fntpPiMinus->DumpData();
        
        // Fill Histogram
        hpiminus_mom->Fill(piminus_4mom.P());
    }

    // ***
    // -----  RhoTuple: Pion+
    // ***
    for (int j=0; j < piplus.GetLength(); ++j) {

        TLorentzVector piplus_4mom = piplus[j]->P4();
        //qa.qaP4("piplus_", piplus_4mom, fntpPiPlus);
        //qa.qaP4Cms("piplus_", piplus_4mom, fntpPiPlus);
        //qa.qaCand("piplus_", piplus[j], fntpPiPlus);
        
        // Let's use qaComp() instead.
        qa.qaComp("piplus_",piplus[j],fntpPiPlus,false,false);
        qa.qaMcDiff("piplus_", piplus[j], fntpPiPlus);
        
        // Fill NTuple
        fntpPiPlus->DumpData();
        
        // Fill Histogram
        hpiplus_mom->Fill(piplus_4mom.P());
    }

    // ***
    // -----  RhoTuple: Proton
    // ***
    for (int j=0; j < p.GetLength(); ++j) {

        TLorentzVector p_4mom = p[j]->P4();
        //qa.qaP4("proton_", p_4mom, fntpProton);
        //qa.qaP4Cms("proton_", p_4mom, fntpProton);
        //qa.qaCand("proton_", p[j], fntpProton);
        
        // Let's use qaComp() instead.
        qa.qaComp("proton_",p[j],fntpProton,false,false);
        qa.qaMcDiff("proton_", p[j], fntpProton);
        
        // Fill NTuple
        fntpProton->DumpData();
        
        // Fill Histogram
        hproton_mom->Fill(p_4mom.P());

    }

    // ***
    // -----  RhoTuple: AntiProton
    // ***
    for (int j=0; j<pbar.GetLength(); ++j) {

        TLorentzVector pbar_4mom = pbar[j]->P4();
        //qa.qaP4("antiproton_", pbar_4mom, fntpAntiProton);
        //qa.qaP4Cms("antiproton_", pbar_4mom, fntpAntiProton);
        //qa.qaCand("antiproton_", pbar[j], fntpAntiProton);
        
        // Let's use qaComp() instead.
        qa.qaComp("antiproton_",pbar[j],fntpAntiProton,false,false);
        qa.qaMcDiff("antiproton_", pbar[j], fntpAntiProton);
        
        // Fill NTuple
        fntpAntiProton->DumpData();
        
        // Fill Histogram
        hantiproton_mom->Fill(pbar_4mom.P());
    }


    // +++------------------------------------------------------------------+++
    // 					   Vertex Fitting (Lambda0, Lambda0Bar)
    // +++------------------------------------------------------------------+++

    // ***
    // -----  do VERTEX FIT (lam0)
    // ***

    // std::cout << "-I- Starting Vertex Fit with Lambda0     : " << lam0.GetLength() << std::endl;

    std::map<int, int> bestVtxFitLam0;
    bestVtxFitLam0 = VertexQaIndex(&lam0, 0.01);

    for (int j=0; j<lam0.GetLength(); ++j) {


        //VertexFit
        RhoKinVtxFitter vtxfitter(lam0[j]);
        vtxfitter.Fit();
        
        //Fill RhoTuple fntpLambda
        fntpLambda->Column("VtxFit_HowGood", (Int_t) bestVtxFitLam0[j]);
        qa.qaFitter("VtxFit_",&vtxfitter,fntpLambda,false);
        qa.qaVtx("VtxFit_",lam0[j]->GetFit(),fntpLambda);
        //qa.qaPull("VtxFit_pull_",lam0[j]->GetFit(),fntpLambda,false);
        //qa.qaComp("VtxFit_Comp_",lam0[j]->GetFit(),fntpLambda,false,false);
        //qa.qaMcDiff("d0_",lam0[j]->Daughter(0),fntpLambda);
        //qa.qaMcDiff("d1_",lam0[j]->Daughter(1),fntpLambda);
        //qa.qaMcDiff("d0_fit",lam0[j]->Daughter(0)->GetFit(),fntpLambda);
        //qa.qaMcDiff("d1_fit",lam0[j]->Daughter(1)->GetFit(),fntpLambda);
        
        
        //qa.qaCand("lambda_",lam0[j]->GetFit(),fntpLambda);
        
        // Let's use qaComp() instead.
        qa.qaComp("lambda_",lam0[j]->GetFit(),fntpLambda,false,false);
        
        
        //Keep Best Candidate
        if (bestVtxFitLam0[j]==1) {
            
            // lam0_best
            lam0_best.Append(lam0[j]->GetFit());

            // Counter: for lam0_best
            lam0_vf_counter += lam0_best.GetLength();
            
        }
        
        
        // Fill NTuple
        fntpLambda->DumpData();
        
    }//VtxFit


	// ***
	// *** TODO: RhoTuple: Lambda (-> Proton Pion-) loop
	// ***
	/*
	for (int j=0; j<lam0_best.GetLength(); ++j) {

	    TLorentzVector lam0_best_4mom = lam0_best[j]->P4();
		//qa.qaP4("lambda_", lam0_best_4mom, fntpLambda);
		qa.qaCand("lambda_", lam0_best[j], fntpLambda);
		//qa.qaMcDiff("lambda_", lam0_best[j], fntpLambda);
		fntpLambda->DumpData();

		// Fill Histogram
		hlam0_mom->Fill(lam0_best_4mom.P());
	}
	*/


	// ***
	// -----  TODO: do MASS CONSTRAINT FIT (lam0)
	// ***
	
	/*
	for (int j=0; j<lam0.GetLength(); ++j) {
		RhoKinFitter mfitter(lam0[j]);					   // instantiate the RhoKinFitter in psi(2S)
		mfitter.AddMassConstraint(fMass0_lam0);			   // add the mass constraint
		mfitter.Fit();									         // do fit

		double chi2_m = mfitter.GetChi2();				   // get chi2 of fit
		double prob_m = mfitter.GetProb();				   // access probability of fit
		hlam0_chi2_mf->Fill(chi2_m);
		hlam0_prob_mf->Fill(prob_m);

		if ( prob_m > 0.01 ) {							      // when good enough, fill some histo
			RhoCandidate *jfit = lam0[j]->GetFit();		// access the fitted cand
			hlam0_mass_mf->Fill(jfit->M());
		}
	}
	*/



    // ***
    // -----  do VERTEX FIT (lam0bar)
    // ***

    // std::cout << "-I- Starting Vertex Fit with Lambda0Bar  : " << lam0bar.GetLength() << std::endl;

    std::map<int, int> bestVtxFitLam0bar;
    bestVtxFitLam0bar = VertexQaIndex(&lam0bar, 0.01);

    for (int j=0; j<lam0bar.GetLength(); ++j) {
        
        //VertexFit
        RhoKinVtxFitter vtxfitter(lam0bar[j]);
        vtxfitter.Fit();

        //Fill RhoTuple fntpLambda
        fntpLambdaBar->Column("VtxFit_HowGood", (Int_t) bestVtxFitLam0bar[j]);
        qa.qaFitter("VtxFit_",&vtxfitter,fntpLambdaBar,false);
        qa.qaVtx("VtxFit_",lam0bar[j]->GetFit(),fntpLambdaBar);
        //qa.qaPull("VtxFit_pull_",lam0bar[j]->GetFit(),fntpLambdaBar,false);
        //qa.qaComp("VtxFit_Comp_",lam0bar[j]->GetFit(),fntpLambdaBar,false,false);
        //qa.qaMcDiff("d0_",lam0bar[j]->Daughter(0),fntpLambdaBar);
        //qa.qaMcDiff("d1_",lam0bar[j]->Daughter(1),fntpLambdaBar);
        //qa.qaMcDiff("d0_fit",lam0bar[j]->Daughter(0)->GetFit(),fntpLambdaBar);
        //qa.qaMcDiff("d1_fit",lam0bar[j]->Daughter(1)->GetFit(),fntpLambdaBar);
        
        
        //qa.qaCand("lambdabar_",lam0bar[j]->GetFit(),fntpLambdaBar);
        
        // Let's use qaComp() instead.
        qa.qaComp("lambdabar_",lam0bar[j]->GetFit(),fntpLambdaBar,false,false);
        
        
        //Keep Best Candidate
        if (bestVtxFitLam0bar[j]==1) {

            // lam0bar_best
            lam0bar_best.Append(lam0bar[j]->GetFit());

            // Counter: for lam0bar_best
            lam0bar_vf_counter += lam0bar_best.GetLength();
            
        }
        
        
        // Fill NTuple
        fntpLambdaBar->DumpData();
        
    }//VtxFit


	// ***
	// *** TODO: RhoTuple: LambdaBar (-> AntiProton Pion+) loop
	// ***
	
	/*
	for (int j=0; j<lam0bar_best.GetLength(); ++j) {

		TLorentzVector lam0bar_best_4mom = lam0bar_best[j]->P4();
		//qa.qaP4("lambdabar_", lam0bar_best_4mom, fntpLambdaBar);
		qa.qaCand("lambdabar_", lam0bar_best[j], fntpLambdaBar);
		//qa.qaMcDiff("lambdabar_", lam0bar_best[j], fntpLambdaBar);
		fntpLambdaBar->DumpData();

		// Fill Histogram
		hlam0_mom->Fill(lam0bar_best_4mom.P());
	}
	*/


	// ***
	// -----  TODO: do MASS CONSTRAINT FIT (lam0bar)
	// ***
	
	/*
	for (int j=0; j<lam0bar.GetLength(); ++j) {
		RhoKinFitter mfitter(lam0bar[j]);				   // instantiate the RhoKinFitter in psi(2S)
		mfitter.AddMassConstraint(fMass0_lam0);			   // add the mass constraint
		mfitter.Fit();									         // do fit

		double chi2_m = mfitter.GetChi2();				   // get chi2 of fit
		double prob_m = mfitter.GetProb();				   // access probability of fit
		hlam0bar_chi2_mf->Fill(chi2_m);
		hlam0bar_prob_mf->Fill(prob_m);

		if (prob_m > 0.01) {							         // when good enough, fill some histo
			RhoCandidate *jfit = lam0bar[j]->GetFit();	// access the fitted cand
			hlam0bar_mass_mf->Fill(jfit->M());
		}
	}
	*/
    
    
    // +++------------------------------------------------------------------+++
    //                             PbarPSystem
    // +++------------------------------------------------------------------+++
            
    // Adeel's Convention: d0=lam0, d1=lam0bar
    // pbarpsystem_best.Combine(lam0_best,lam0bar_best);

    // Walter's Convention: d0=lam0bar, d1=lam0 (see mc rhocand. above)
    // To make it consistant, lets switch the order in pbarpsystem_best.
    pbarpsystem_best.Combine(lam0bar_best,lam0_best); 
    pbarpsystem_best.SetType(88888);

    if (pbarpsystem_best.GetLength() > 1) {
        std::cout << "WARNING: More than one pbarp combi! Total: " << pbarpsystem_best.GetLength() << " Event: " << fEvtCount << std::endl;
    }

    // ***
    // -----  RhoTuple: pbarpsystem (fntpPbarP)
    // ***

    for (int j=0; j<pbarpsystem_best.GetLength(); ++j) {
        qa.qaCand("pbarp_", pbarpsystem_best[j], fntpPbarP);
        //qa.qaComp("pbarp_", pbarpsystem_best[j], fntpPbarP, true, true);
        fntpPbarP->DumpData();
    }


    // Counter: pbarpsystem_best (~pbarpsystem_vf)
    pbarp_vf_counter +=pbarpsystem_best.GetLength();


    // +++------------------------------------------------------------------+++
    //                          4C Fitting (PbarPSystem)
    // +++------------------------------------------------------------------+++
    //	std::cout << "-I- Starting 4C Fit with pbarpsystem_best: " << pbarpsystem_best.GetLength() << std::endl;

    for (int j=0; j<pbarpsystem_best.GetLength(); ++j) {
        
        //Kinematic Fit       
        RhoKinFitter FourMomFitter(pbarpsystem_best[j]);        // instantiate RhoKinFitter in psi(2S)
        FourMomFitter.Add4MomConstraint(fIni);                  // set 4 constraint
        //FourMomfitter.SetVerbose(true);                       // set verbose
        bool IsGoodFit = FourMomFitter.Fit();                   // perform fit

        // Counter: All 4C Events
        ++all4Ccount;

        //Fill RhoTuple if 4C is successful (IsGoodFit=True)
        if (IsGoodFit) {
            //if (prob_4c > 0.01) {

            // Fitted Candidate
            RhoCandidate *jfit = pbarpsystem_best[j]->GetFit();

            // Fill RhoTuple
            fntpBestPbarP->Column("McTruthMatch", (Bool_t) fAnalysis->McTruthMatch(pbarpsystem_best[j]));
            fntpBestPbarP->Column("FourMomFit_Convergence", (Bool_t) IsGoodFit);
            qa.qaFitter("FourMomFit_",&FourMomFitter,fntpBestPbarP,false);

            // *** Start: Walter added
            TLorentzVector fit_lam0bar_p4 = jfit->Daughter(0)->P4();
            TLorentzVector fit_lam0_p4 = jfit->Daughter(1)->P4();
            TLorentzVector fit_P4pbar_lambarCM = jfit->Daughter(0)->Daughter(0)->P4();
            TLorentzVector fit_piP4p_lamCM = jfit->Daughter(0)->Daughter(1)->P4();
            TLorentzVector fit_P4p_lamCM = jfit->Daughter(1)->Daughter(0)->P4();
            TLorentzVector fit_pim_p4 = jfit->Daughter(1)->Daughter(1)->P4();

            //Boost Lambda to CM frame
            fit_lam0_p4.Boost(-fIni.BoostVector());

            //Boost Lambdabar to CM frame
            fit_lam0bar_p4.Boost(-fIni.BoostVector());

            //Boost proton to Lambda rest frame
            fit_P4p_lamCM.Boost(-fIni.BoostVector());
            fit_P4p_lamCM.Boost(-fit_lam0_p4.BoostVector());

            //Boost pbar to Lambdabar rest frame
            fit_P4pbar_lambarCM.Boost(-fIni.BoostVector());
            fit_P4pbar_lambarCM.Boost(-fit_lam0bar_p4.BoostVector());

            //Lambda decay frame, lambda in rest
            std::vector <TVector3> fit_lam_RF = GenerateUnitVectors(beamCM,fit_lam0_p4,fit_lam0bar_p4);
            TLorentzVector rot_p_decayframe_exp = TransformCoords(lab_RF,fit_lam_RF,fit_P4p_lamCM);

            //Lambdabar decay frame, lambdabar in rest
            std::vector <TVector3> fit_lambar_RF = GenerateUnitVectors(beamCM,fit_lam0bar_p4,fit_lam0bar_p4);
            TLorentzVector rot_pbar_decayframe_exp = TransformCoords(lab_RF,fit_lambar_RF,fit_P4pbar_lambarCM);

            fntpBestPbarP->Column("d0d0_ky", (Float_t) TMath::Cos(fit_P4pbar_lambarCM.Vect().Angle(fit_lambar_RF.at(1))),  0.0f );
            fntpBestPbarP->Column("d1d0_ky", (Float_t) TMath::Cos(fit_P4p_lamCM.Vect().Angle(fit_lam_RF.at(1))),  0.0f );

            fntpBestPbarP->Column("MC_d0d0_ky", (Float_t) TMath::Cos(MC_P4pbar_lambarCM.Vect().Angle(MC_lambar_RF.at(1))),  0.0f );
            fntpBestPbarP->Column("MC_d1d0_ky", (Float_t) TMath::Cos(MC_P4p_lamCM.Vect().Angle(MC_lam_RF.at(1))),  0.0f );
            // *** End: Walter Added



            //qa.qaCand("pbarp_", jfit, fntpBestPbarP);
            qa.qaComp("pbarp_", jfit, fntpBestPbarP, true, true);



            // *** Start: Walter added
            qa.qaCand("MC_d0", mclambar, fntpBestPbarP);
            qa.qaP4Cms("MC_d0", mclambar->P4(), fntpBestPbarP);
            qa.qaCand("MC_d1", mclam, fntpBestPbarP);
            qa.qaP4Cms("MC_d1", mclam->P4(), fntpBestPbarP);

            qa.qaCand("MC_d0d0", mcpbar, fntpBestPbarP);
            qa.qaP4Cms("MC_d0d0", mcpbar->P4(), fntpBestPbarP);
            qa.qaCand("MC_d0d1", mcpip, fntpBestPbarP);
            qa.qaP4Cms("MC_d0d1", mcpip->P4(), fntpBestPbarP);

            qa.qaCand("MC_d1d0", mcp, fntpBestPbarP);
            qa.qaP4Cms("MC_d1d0", mcp->P4(), fntpBestPbarP);
            qa.qaCand("MC_d1d1", mcpim, fntpBestPbarP);
            qa.qaP4Cms("MC_d1d1", mcpim->P4(), fntpBestPbarP);			
            // *** End: Walter Added

            //Write RhoTuple fntpBestPbarP
            fntpBestPbarP->DumpData();


            //}

            // Counter: Good Fit Events
            ++goodfit;

            // std::cout << "-I- IsGoodFit (4C) Condition Has Been Passed." << std::endl;
        }

        //else {
        //    std::cout << "-I- IsGoodFit (4C) Condition Has Been Failed." << std::endl;
        //}


    }//4C


    // Clean to reuse.
    p.Cleanup();
    piplus.Cleanup();
    pbar.Cleanup();
    piminus.Cleanup();
    lam0.Cleanup();
    lam0bar.Cleanup();
    pbarpsystem.Cleanup();
    lam0_best.Cleanup();
    lam0bar_best.Cleanup();
    pbarpsystem_best.Cleanup();

    // std::cout << "-I- Event Has Been Processed." << std::endl;

}//end Exec()


void PndLLbarAnaTaskRGIS::Finish() {

    // Summary
    std::cout << std::endl;
    std::cout << "+++-------------------------------------------------------+++" << std::endl;
    std::cout << "      Total No. of Events: " << fEvtCount << std::endl;
    std::cout << "           Total No. of p: " << p_counter       << std::endl;
    std::cout << "        Total No. of pbar: " << pbar_counter    << std::endl;
    std::cout << "      Total No. of piplus: " << piplus_counter  << std::endl;
    std::cout << "     Total No. of piminus: " << piminus_counter << std::endl;
    std::cout << std::endl;
    std::cout << "   Total No. of lam0 (MS): " << lam0_counter << std::endl;
    std::cout << "   Total No. of lam0 (VF): " << lam0_vf_counter << std::endl;
    std::cout << "Total No. of lam0bar (MS): " << lam0bar_counter << std::endl;
    std::cout << "Total No. of lam0bar (VF): " << lam0bar_vf_counter << std::endl;
    std::cout << "  Total No. of pbarp (MS): " << pbarp_counter << std::endl;
    std::cout << "  Total No. of pbarp (VF): " << pbarp_vf_counter << std::endl;
    std::cout << std::endl;
    std::cout << "  Total No. of pbarp (MS): " << pbarp_counter << std::endl;
    std::cout << "  Total No. of pbarp (VF): " << pbarp_vf_counter << std::endl;
    std::cout << "  Total No. of pbarp (4C): " << all4Ccount << std::endl;
    std::cout << "Total No. of pbarp (Good): " << goodfit << std::endl;
    std::cout << "         Final Efficiency: " << ((float)goodfit/(float)fEvtCount)*100. << "%" << std::endl;
    std::cout << "+++-------------------------------------------------------+++" << std::endl;
    std::cout << std::endl;


    // ********************************************************************
    // *** 			       write out all the tuples		    			***
    // ********************************************************************

    // *** 
    // *** RhoTuples: Particles
    // ***
    std::cout << "-I- Started Writing RhoTuples (in Order)." << std::endl;

    fntpPiMinus->GetInternalTree()->Write();
    fntpPiPlus->GetInternalTree()->Write();
    fntpProton->GetInternalTree()->Write();
    fntpAntiProton->GetInternalTree()->Write();
    fntpLambda->GetInternalTree()->Write();
    fntpLambdaBar->GetInternalTree()->Write();
    fntpPbarP->GetInternalTree()->Write();
    fntpBestPbarP->GetInternalTree()->Write();
    fntpMCTruth->GetInternalTree()->Write();

    // *** 
    // *** Histograms: 4 Momentum
    // ***

    //hpiminus_mom->Write();
    //hpiplus_mom->Write();
    //hproton_mom->Write();
    //hantiproton_mom->Write();

    std::cout << "-I- Finished Writing Tuples and Histograms." << std::endl;


    // Following order matters.

    //freed memory (no memory leaks)
    delete fntpPiMinus;
    delete fntpPiPlus;
    delete fntpProton;
    delete fntpAntiProton;
    delete fntpLambda;
    delete fntpLambdaBar;
    delete fntpPbarP;
    delete fntpBestPbarP;
    delete fAnalysis;
    delete fLambdaMassSel;
    delete fPdgCode;


    // Point dangling to nullptr
    fntpPiMinus = nullptr;
    fntpPiPlus = nullptr;
    fntpProton = nullptr;
    fntpAntiProton = nullptr;
    fntpLambda = nullptr;
    fntpLambdaBar = nullptr;
    fntpPbarP = nullptr;
    fntpBestPbarP = nullptr;
    fAnalysis = nullptr;
    fLambdaMassSel = nullptr;
    fPdgCode = nullptr;
    
    std::cout << "-I- Finished Deleting Pointers." << std::endl;
    
    
}// end FinishTask()


// *** Routine to only keep PID matched candidates in list
int PndLLbarAnaTaskRGIS::SelectTruePid(PndAnalysis *ana, RhoCandList &l) {

	int removed = 0;
	for (int ii=l.GetLength()-1; ii>=0; --ii) {
		if (!(ana->McTruthMatch(l[ii]))) {
			l.Remove(l[ii]);
			removed++;
		}
	}

	return removed;
}//end SelectTruePid()


//*** Walter Added
std::map<int,int> PndLLbarAnaTaskRGIS::VertexQaIndex(RhoCandList* candList, Double_t prob_cut) {
	/* @brief  give back the order of the best chi2
	 * @details give back the order of the best chi2!  1 means best, 2: second best (same with negative values for bad chi2 )
	 * @details: Credit to Jenny Puetz for the code
	 */
	std::map<double, int> chi2_good, chi2_bad;

	for (int j=0; j<candList->GetLength(); ++j) {

		RhoKinVtxFitter vtxfitter(candList->Get(j));
		vtxfitter.Fit();

		bool failedchi2 = TMath::IsNaN(vtxfitter.GetChi2());
		bool failedprob = TMath::IsNaN(vtxfitter.GetProb());

		if(!failedchi2 && !failedprob) {
			if (vtxfitter.GetProb() > prob_cut) { //Prob > 0.01
				chi2_good[vtxfitter.GetChi2()]=j;
			}
			else { //Prob <= 0.01
				chi2_bad[vtxfitter.GetChi2()]=j;
			}
		}
	}

	std::map<double, int>::iterator is_good, is_bad;
	std::map<int, int> indexBestFit;
	int running = 0;

	for (is_good = chi2_good.begin(); is_good != chi2_good.end(); is_good++, running++) {
		indexBestFit[is_good->second] = running + 1;
	}

	running =0;

	for (is_bad = chi2_bad.begin(); is_bad != chi2_bad.end(); is_bad++, running++) {
		indexBestFit[is_bad->second] = - (running + 1);
	}

	return indexBestFit;

}//end


//*** Walter Added
std::vector<TVector3> PndLLbarAnaTaskRGIS::GenerateUnitVectors(TLorentzVector p_beam, TLorentzVector p_out, TLorentzVector p_ref) {

	TVector3 DecayFrameAxisZ = p_out.Vect();
	Double_t AxisZMag = DecayFrameAxisZ.Mag();
	DecayFrameAxisZ.SetX(DecayFrameAxisZ.X()/AxisZMag);
	DecayFrameAxisZ.SetY(DecayFrameAxisZ.Y()/AxisZMag);
	DecayFrameAxisZ.SetZ(DecayFrameAxisZ.Z()/AxisZMag);

	TVector3 DecayFrameAxisY  = p_beam.Vect().Cross(p_ref.Vect());//Note: Using lambar here
	Double_t AxisYMag = DecayFrameAxisY.Mag();
	DecayFrameAxisY.SetX(DecayFrameAxisY.X()/AxisYMag);
	DecayFrameAxisY.SetY(DecayFrameAxisY.Y()/AxisYMag);
	DecayFrameAxisY.SetZ(DecayFrameAxisY.Z()/AxisYMag);

	TVector3 DecayFrameAxisX  = DecayFrameAxisY.Cross(DecayFrameAxisZ);
	std::vector <TVector3> final;
	final.push_back(DecayFrameAxisX);
	final.push_back(DecayFrameAxisY);
	final.push_back(DecayFrameAxisZ);

	return final;
}


//*** Walter Added
TLorentzVector PndLLbarAnaTaskRGIS::TransformCoords(std::vector<TVector3> initframe, std::vector<TVector3> finalframe, TLorentzVector p) {

	TLorentzVector result;
	TVector3 p3 = p.Vect();

	p.SetX(finalframe.at(0).Dot(initframe.at(0)) * p3.X() +
			finalframe.at(0).Dot(initframe.at(1)) * p3.Y() +
			finalframe.at(0).Dot(initframe.at(2)) * p3.Z()
	);
	p.SetY(finalframe.at(1).Dot(initframe.at(0)) * p3.X() +
			finalframe.at(1).Dot(initframe.at(1)) * p3.Y() +
			finalframe.at(1).Dot(initframe.at(2)) * p3.Z()
	);
	p.SetZ(finalframe.at(2).Dot(initframe.at(0)) * p3.X() +
			finalframe.at(2).Dot(initframe.at(1)) * p3.Y() +
			finalframe.at(2).Dot(initframe.at(2)) * p3.Z()
	);

	return p;
}

ClassImp(PndLLbarAnaTaskRGIS)
