#!/usr/bin/env bash

# Run with defaults.
# e.g. reco_complete.C and pid_complete.C have default setting for 
# BarrelTrackFinder. One should use fRun->SetOptions(options) to
# use other trackers such as HoughTrackFinder etc, etc.

if((1)); then
  rm *.root
  rm *.log
  rm *.pdf
  rm core*
  rm *.dat
fi

# init pandaroot
# . $HOME"/fair/pandaroot_dev/build-April2021/config.sh"


#CONTAINER=$HOME/fair/stable/v12.0.3.sif     # FairSoft(nov20p1), FairRoot(18.6.3), PandaRoot(v12.0.3)
CONTAINER=$HOME/fair/stable/dev210810.sif    # FairSoft(nov20p1), FairRoot(18.6.3), PandaRoot(dev210810)
#CONTAINER=$HOME/fair/latest/dev210810.sif   # FairSoft(nov20p1), FairRoot(master), PandaRoot(dev210810)


prefix=llbar                                 # output file for naming.
nevt=100                                     # number of events.
dec=llbar_fwp.DEC                            # decay file OR keywords [fwp, bkg, dpm].
mom=1.642                                    # pbarp with 1.642 GeV/c.
IsSignal=true                                # signal/background for analysis task.


if test "$1" != ""; then
  nevt=$1
fi

if test "$2" != ""; then
  prefix=$2
fi

if test "$3" != ""; then
  dec=$3
fi


echo ""
echo "Events: $nevt"

echo "Started Simulation..."
singularity exec $CONTAINER root -l -b -q sim_complete.C\($nevt,\"$prefix\",\"$dec\"\) > sim.log 2>&1

echo "Started Digitization..."
singularity exec $CONTAINER root -l -b -q digi_complete.C\($nevt,\"$prefix\"\) > digi.log 2>&1

echo "Started BarrelTrackFiner..."
singularity exec $CONTAINER root -l -b -q reco_complete.C\($nevt,\"$prefix\"\) > reco.log 2>&1

echo "Started Particle Identification..."
singularity exec $CONTAINER root -l -b -q pid_complete.C\($nevt,\"$prefix\"\) > pid.log 2>&1

echo "Starting Analysis Task (User-defined)..."
singularity exec $CONTAINER root -l -q -b ana_ideal.C\($nevt,\"$prefix\",$IsSignal\) > ana.log 2>&1

echo "Script has finished..."
echo ""
