

### Optimization

- Two plot FoM, S/sqrt(B) and S/sqrt(S+B) one needs two CSV files _i.e._ `efwp1-optz.csv` (signal)
and `ebkg1-optz.csv` (bkg) that contain two columns `dvz`, `events`.

- First run root.sh with `ntpBestPbarP.C` that will dump info in above CSVs, change `f = new TFile("efwp1-BestPbarP.root");`
 for signal and background in constructor in the `ntpBestPbarP.h` 

- Then, us the appropriate notebook e.g. `optz-ext1.ipynb` to get plots.
