#!/bin/sh

if [ $# -lt 1 ]; then
  echo -e "\nMacro File is Necessary.\n"
  echo -e "USAGE: ./root.sh <macro.C>"
  exit 1
fi

FILE=$1
CONTAINER=$HOME/fair/containers/v12.0.3.sif
# singularity exec $CONTAINER root -l -b -q $FILE && exit
# singularity run $CONTAINER -c "root -l -b -q $FILE" && exit


for n in $(seq 0 0.2 10)
do
  singularity run $CONTAINER -c "root -l -q -b $FILE\($n\)"
done
