#!/bin/sh

if [ $# -lt 1 ]; then
  echo "\nPrefix is mandatory."
  echo "USAGE: ./ana_ip.sh <prefix> <from> <to>"
  echo "Example: ./ana_ip.sh efwp1"
  exit 1
fi

prefix=efwp1
from=1
to=500

if test "$1" != ""; then
  prefix=$1
fi
if test "$2" != ""; then
  from=$2
fi
if test "$3" != ""; then
  to=$3
fi

echo "Prefix    : $prefix"
echo "Start File: $from"
echo "End File  : $to"

# Terminate Script.
# exit 0;

LUSTRE_HOME="/lustre/panda/"$USER
. $LUSTRE_HOME"/CENTOS/dev-install/bin/config.sh" -p
root -l -b -q ana_ip.C\(\"$prefix\",$from,$to\) > ana_ip.log 2>&1

