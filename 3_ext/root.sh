#!/bin/sh

if [ $# -lt 1 ]; then
  echo -e "\nMacro File is Necessary.\n"
  echo -e "USAGE: ./root.sh <macro.C>"
  exit 1
fi

FILE=$1
# CONTAINER=$HOME/current/containers/pzfinder.sif  # dev, 18.6.3, nov20p1
CONTAINER=$HOME/fair/containers/v12.0.3.sif
NTP=0

#if((0)); then
if [ $NTP -eq 1 ]; then
  echo "Running Ntuples..."
  singularity run $CONTAINER -c "root -l -b -q ntpBestPbarP.C" && exit
  singularity run $CONTAINER -c "root -l -b -q ntpMCTruth.C" && exit
  singularity run $CONTAINER -c "root -l -b -q ntpLambda.C" && exit
  singularity run $CONTAINER -c "root -l -b -q ntpLambdaBar.C" && exit
  singularity run $CONTAINER -c "root -l -b -q ntpProton.C" && exit
  singularity run $CONTAINER -c "root -l -b -q ntpAntiProton.C" && exit
  singularity run $CONTAINER -c "root -l -b -q ntpPiPlus.C" && exit
  singularity run $CONTAINER -c "root -l -b -q ntpPiMinus.C" && exit
else
  echo "Running Macro..."
fi

singularity run $CONTAINER -c "root -l $FILE" && exit

