#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;

void plot_mcVtx() {

    //*** Open Histogram Root File
    TFile *f = TFile::Open("ntpMCTruth.root");
    if (!f) return;
    f->ls();
    
	//****************************** First histogram **************************
	TString hist_name="";
    
    //----- Kinematics (MC/REC of Final States)
    hist_name = "zRAll";
    
	TH1F* h1 = (TH1F*)f->Get(hist_name);
	
	// Fixing title
	h1->SetTitle(" ");

	// Fixing the X axis
	h1->GetXaxis()->SetLabelSize(0.055);
	h1->GetXaxis()->SetTitleSize(0.06);
	h1->GetXaxis()->SetTitleOffset(0.99);
	h1->GetXaxis()->CenterTitle();
	h1->GetXaxis()->SetTitle("Z-Vertex / cm");
	
	// Fixing the Y axis
	h1->GetYaxis()->SetLabelSize(0.055);
	h1->GetYaxis()->SetTitleSize(0.06);
	h1->GetYaxis()->SetTitleSize(0.06);
	h1->GetYaxis()->SetTitleOffset(0.9);
	h1->GetYaxis()->CenterTitle();
	h1->GetYaxis()->SetTitle("R-Vertex / cm");
    
    h1->SetStats(kFALSE);
    
    //*************************************************************************
    //                               TCanvas
    //*************************************************************************
	TCanvas * can = new TCanvas("c1","LLBar Decay Vertex");
	can->SetCanvasSize(1500, 1000);
   	can->SetWindowSize(1500, 1000);
	can->SetTopMargin(0.025);
	can->SetBottomMargin(0.15);
	can->SetLeftMargin(0.15);
	can->SetLogz();
	
	h1->Draw("colz");

	TLatex latexTextSTT(72,31,"STT");
	latexTextSTT.SetTextFont(62);
	latexTextSTT.SetTextSize(0.05);
	//latexTextSTT.SetTextColor(kGray+1);
	latexTextSTT.DrawClone();

	TLatex latexTextMVD(0.693,10.91,"MVD");
	latexTextMVD.SetTextFont(62);
	latexTextMVD.SetTextSize(0.03);
	//latexTextMVD.SetTextColor(kGray+1);
	latexTextMVD.DrawClone();

	TLatex latexTextGEM(191,20,"GEM");
	latexTextGEM.SetTextFont(62);
	latexTextGEM.SetTextSize(0.05);
	//latexTextGEM.SetTextColor(kGray+1);
	latexTextGEM.DrawClone();
	
	TLatex latexTextPND(190,46,"PANDA");
	latexTextPND.SetTextFont(42);
	latexTextPND.SetTextSize(0.06);
	latexTextPND.SetTextColor(kGray+1);
	latexTextPND.DrawClone();
	
	TLatex latexTextMC(190,42,"MC Simulation");
	latexTextMC.SetTextFont(42);
	latexTextMC.SetTextSize(0.06);
	latexTextMC.SetTextColor(kGray+1);
	latexTextMC.DrawClone();
	
	TLatex latexTextp(190,38,"p_{beam} = 1.642 GeV/c");
	latexTextp.SetTextFont(62);
	latexTextp.SetTextSize(0.04);
	latexTextp.SetTextColor(kGray+1);
	latexTextp.DrawClone();
	
	TLine *lineMVD1 = new TLine(-5,13.5,19,13.5);
	lineMVD1->SetLineWidth(2);
	lineMVD1->Draw();
	
	TLine *lineMVD2 = new TLine(19,0,19,13.5);
	lineMVD2->SetLineWidth(2);	
	lineMVD2->Draw();
	
	TLine *lineSTT1 = new TLine(-5,15,100,15);
	lineSTT1->SetLineWidth(2);	
	lineSTT1->Draw();
	
	TLine *lineSTT2 = new TLine(-5,42,100,42);
	lineSTT2->SetLineWidth(2);	
	lineSTT2->Draw();
	
	TLine *lineSTT3 = new TLine(100,15,100,42);
	lineSTT3->SetLineWidth(2);	
	lineSTT3->Draw();

	TLine *lineGEM1 = new TLine(117,0,117,45);
	lineGEM1->SetLineWidth(2);
	lineGEM1->Draw();
	
	TLine *lineGEM2 = new TLine(153,0,153,49);
	lineGEM2->SetLineWidth(2);	
	lineGEM2->Draw();
	
	TLine *lineGEM3 = new TLine(189,0,189,49);
	lineGEM3->SetLineWidth(2);	
	lineGEM3->Draw();

	can->SaveAs("vtx_mc_1642.png");
	can->Draw();	
}


