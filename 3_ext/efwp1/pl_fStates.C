/** Macro to plot MCTruth and RECO before performing the PndLLbarAnaTask
 *  Analysis. The MCTruth are stored in ntpMCTruth and the decay particle
 *  into ther corresponding ntuples e.g. proton > ntpProton.root, etc.
 *
 *  Same as pl_ntp.C except it takes MCTruth before analysis (ntpMCTruth.C)
 */
 
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include "DrawOpt.C"

void pl_fStates() {
    
    //-------------------------------------------------------------------------
    //                               Histograms
    //-------------------------------------------------------------------------
    
    TString hist_name="";
    
    //----- Kinematics (MC/REC of Final States)
    hist_name = "hpbar_MomTht";
    hist_name = "hp_MomTht";
    hist_name = "hpiminus_MomTht";
    hist_name = "hpiplus_MomTht";

    //hist_name = "hpbar_PzPt";
    //hist_name = "hp_PzPt";
    //hist_name = "hpiminus_PzPt";
    hist_name = "hpiplus_PzPt";
    
    //hist_name = "hpbar_xy";
    //hist_name = "hp_xy";
    //hist_name = "hpiminus_xy";
    //hist_name = "hpiplus_xy";
    
    //hist_name = "hpbar_mass";
    //hist_name = "hp_mass";
    //hist_name = "hpiminus_mass";
    //hist_name = "hpiplus_mass";
    
    //hist_name = "hpbar_zR";
    //hist_name = "hp_zR";
    //hist_name = "hpiplus_zR";
    //hist_name = "hpiminus_zR";
    
    //-------------------------------------------------------------------------
    //                              Final States (MC vs. REC)
    //-------------------------------------------------------------------------
    
    //*** Open Histograms ROOT File: MCTruth Match
    TFile *g = TFile::Open("ntpMCTruth.root");
    if (!g) return;
    g->ls();
    
    //*** Open Histograms ROOT File: Final States
    TFile *f=nullptr;
    
    if(hist_name.Contains("hpbar_"))
        f = TFile::Open("ntpAntiProton.root");  // Reco after Analysis
    if(hist_name.Contains("hpiminus_"))
        f = TFile::Open("ntpPiMinus.root");  // Reco after Analysis
    if(hist_name.Contains("hpiplus_"))
        f = TFile::Open("ntpPiPlus.root");  // Reco after Analysis
    if(hist_name.Contains("hp_"))
        f = TFile::Open("ntpProton.root");  // Reco after Analysis
    
    if (!f) return;
    f->ls();
    
    //*** Clone Histograms
    TH1D *h1 = (TH1D*)(g->Get(hist_name)); //ntpMCTruth
    TH1D *h1c = (TH1D*)h1->Clone("htemp");  
    TH1D *h2 = (TH1D*)(f->Get(hist_name)); //finalstates
    TH1D *h2c = (TH1D*)h2->Clone("htemp");  
    

    //-------------------------------------------------------------------------
    //                               Styling
    //-------------------------------------------------------------------------
    DrawOpt(h1c); DrawOpt(h2c);
       
    //-------------------------------------------------------------------------
    //                               Text, Latex, etc
    //-------------------------------------------------------------------------

    //*** TLatex::TLatex()
    TLatex tex;
    tex.SetTextAlign(13);
    tex.SetTextColor(kGray+3);
    tex.SetTextFont(42);
    tex.SetTextSize(0.08);

    
    //*************************************************************************
    //                               TCanvas
    //*************************************************************************
    
    /*
    TCanvas *c1 = new TCanvas("c1","MC vs. REC Histograms",0,0,1200,500);
    c1->Divide(2,1);
    c1->cd(1); gPad->SetLogy(false); h1c->Draw("colz");
    c1->cd(2); gPad->SetLogy(false); h2c->Draw("colz");
    c1->SaveAs(hist_name+"_MCREC.png");
    c1->Close();
    */
    
	//*************************************************************************
    //                               TCanvas
    //*************************************************************************
    
    //TCanvas *c2 = new TCanvas("c2","MCTruth",0,0,600,500);
    TCanvas * c2 = new TCanvas("c2","MCTruth");
	c2->SetCanvasSize(1200, 1000);
   	c2->SetWindowSize(1200, 1000);
	c2->SetTopMargin(0.025);
	c2->SetBottomMargin(0.15);
	c2->SetLeftMargin(0.15);

	// Draw Hist
    c2->SetLogy(false); c2->SetLogz(true);
    h1c->SetStats(kFALSE); h1c->Draw("colz");
    
    // Draw Latex
    if(hist_name.Contains("hpbar_"))
        tex.DrawLatex(1.5,175,"#bar{p}");
        
    if(hist_name.Contains("hp_"))
        tex.DrawLatex(1.5,175,"p");
        
    if(hist_name.Contains("hpiminus_"))
        tex.DrawLatex(1.5,175,"#pi^{-}");
        
    if(hist_name.Contains("hpiplus_"))
        tex.DrawLatex(1.5,175,"#pi^{+}");
    
    c2->Draw();
    c2->SaveAs(hist_name+"_MC.png");
    c2->Close();
    
    //*************************************************************************
    //                               TCanvas
    //*************************************************************************
    
    //TCanvas *c3 = new TCanvas("c3","RECO",0,0,600,500);
    TCanvas * c3 = new TCanvas("c3","RECO");
    c3->SetCanvasSize(1200, 1000);
   	c3->SetWindowSize(1200, 1000);
	c3->SetTopMargin(0.025);
	c3->SetBottomMargin(0.15);
	c3->SetLeftMargin(0.15);
	
	// Draw Hist
    c3->SetLogy(false); c3->SetLogz(true);
    h2c->SetStats(kFALSE);
    h2c->Draw("colz");

    // Draw Latex
    if(hist_name.Contains("hpbar_"))
        tex.DrawLatex(1.5,175,"#bar{p}");
        
    if(hist_name.Contains("hp_"))
        tex.DrawLatex(1.5,175,"p");
        
    if(hist_name.Contains("hpiminus_"))
        tex.DrawLatex(1.5,175,"#pi^{-}");
        
    if(hist_name.Contains("hpiplus_"))
        tex.DrawLatex(1.5,175,"#pi^{+}");
    
    c3->Draw();
    c3->SaveAs(hist_name+"_REC.png");
    c3->Close();

}//end-macro
