//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Apr 30 19:57:31 2021 by ROOT version 6.16/00
// from TTree ntpLambda/Lambda Info.
// found on file: efwp1-Lambda.root
//////////////////////////////////////////////////////////

#ifndef ntpLambda_h
#define ntpLambda_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class ntpLambda {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           VtxFit_HowGood;
   Float_t         VtxFit_chisq;
   Float_t         VtxFit_ndf;
   Float_t         VtxFit_prob;
   Float_t         VtxFit_vx;
   Float_t         VtxFit_vy;
   Float_t         VtxFit_vz;
   Float_t         VtxFit_len;
   Float_t         VtxFit_ctau;
   Float_t         VtxFit_ctaud;
   Float_t         lambda_px;
   Float_t         lambda_py;
   Float_t         lambda_pz;
   Float_t         lambda_e;
   Float_t         lambda_p;
   Float_t         lambda_tht;
   Float_t         lambda_phi;
   Float_t         lambda_pt;
   Float_t         lambda_m;
   Float_t         lambda_x;
   Float_t         lambda_y;
   Float_t         lambda_z;
   Float_t         lambda_l;
   Float_t         lambda_chg;
   Float_t         lambda_pdg;

   // List of branches
   TBranch        *b_VtxFit_HowGood;   //!
   TBranch        *b_VtxFit_chisq;   //!
   TBranch        *b_VtxFit_ndf;   //!
   TBranch        *b_VtxFit_prob;   //!
   TBranch        *b_VtxFit_vx;   //!
   TBranch        *b_VtxFit_vy;   //!
   TBranch        *b_VtxFit_vz;   //!
   TBranch        *b_VtxFit_len;   //!
   TBranch        *b_VtxFit_ctau;   //!
   TBranch        *b_VtxFit_ctaud;   //!
   TBranch        *b_lambda_px;   //!
   TBranch        *b_lambda_py;   //!
   TBranch        *b_lambda_pz;   //!
   TBranch        *b_lambda_e;   //!
   TBranch        *b_lambda_p;   //!
   TBranch        *b_lambda_tht;   //!
   TBranch        *b_lambda_phi;   //!
   TBranch        *b_lambda_pt;   //!
   TBranch        *b_lambda_m;   //!
   TBranch        *b_lambda_x;   //!
   TBranch        *b_lambda_y;   //!
   TBranch        *b_lambda_z;   //!
   TBranch        *b_lambda_l;   //!
   TBranch        *b_lambda_chg;   //!
   TBranch        *b_lambda_pdg;   //!

   ntpLambda(TTree *tree=0);
   virtual ~ntpLambda();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   TH1D *vtxf_chi2;
   TH1D *vtxf_prob;
   TH1D *vtxf_ndf;
      
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
   
};

#endif

#ifdef ntpLambda_cxx
ntpLambda::ntpLambda(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   TString path = "";
   TFile* hfile = TFile::Open(path+"ntpLambda.root", "RECREATE");
   
   vtxf_chi2 = new TH1D("vtxf_chi2","Chi-square (VxtFit);#chi^{2};Entries",200,0,300);
   vtxf_prob = new TH1D("vtxf_prob","Probability (VxtFit);probability;Entries",200,0,1);
   vtxf_ndf = new TH1D("vtxf_ndf","NDF (VxtFit);ndf;Entries",100,0,10);

   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------    

   // ---------------------------------------------------------------
   //                    This Block Shouldn't be Modified.
   // ---------------------------------------------------------------
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("efwp1-Lambda.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("efwp1-Lambda.root");
      }
      f->GetObject("ntpLambda",tree);

   }
   // Call Init()
   Init(tree);

   // Call Loop() 
   Loop();
   // ---------------------------------------------------------------
   //                    This Block Shouldn't be Modified.
   // ---------------------------------------------------------------
   
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   hfile->Write();
   hfile->Close();
   
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
  
}

ntpLambda::~ntpLambda()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ntpLambda::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ntpLambda::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ntpLambda::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("VtxFit_HowGood", &VtxFit_HowGood, &b_VtxFit_HowGood);
   fChain->SetBranchAddress("VtxFit_chisq", &VtxFit_chisq, &b_VtxFit_chisq);
   fChain->SetBranchAddress("VtxFit_ndf", &VtxFit_ndf, &b_VtxFit_ndf);
   fChain->SetBranchAddress("VtxFit_prob", &VtxFit_prob, &b_VtxFit_prob);
   fChain->SetBranchAddress("VtxFit_vx", &VtxFit_vx, &b_VtxFit_vx);
   fChain->SetBranchAddress("VtxFit_vy", &VtxFit_vy, &b_VtxFit_vy);
   fChain->SetBranchAddress("VtxFit_vz", &VtxFit_vz, &b_VtxFit_vz);
   fChain->SetBranchAddress("VtxFit_len", &VtxFit_len, &b_VtxFit_len);
   fChain->SetBranchAddress("VtxFit_ctau", &VtxFit_ctau, &b_VtxFit_ctau);
   fChain->SetBranchAddress("VtxFit_ctaud", &VtxFit_ctaud, &b_VtxFit_ctaud);
   fChain->SetBranchAddress("lambda_px", &lambda_px, &b_lambda_px);
   fChain->SetBranchAddress("lambda_py", &lambda_py, &b_lambda_py);
   fChain->SetBranchAddress("lambda_pz", &lambda_pz, &b_lambda_pz);
   fChain->SetBranchAddress("lambda_e", &lambda_e, &b_lambda_e);
   fChain->SetBranchAddress("lambda_p", &lambda_p, &b_lambda_p);
   fChain->SetBranchAddress("lambda_tht", &lambda_tht, &b_lambda_tht);
   fChain->SetBranchAddress("lambda_phi", &lambda_phi, &b_lambda_phi);
   fChain->SetBranchAddress("lambda_pt", &lambda_pt, &b_lambda_pt);
   fChain->SetBranchAddress("lambda_m", &lambda_m, &b_lambda_m);
   fChain->SetBranchAddress("lambda_x", &lambda_x, &b_lambda_x);
   fChain->SetBranchAddress("lambda_y", &lambda_y, &b_lambda_y);
   fChain->SetBranchAddress("lambda_z", &lambda_z, &b_lambda_z);
   fChain->SetBranchAddress("lambda_l", &lambda_l, &b_lambda_l);
   fChain->SetBranchAddress("lambda_chg", &lambda_chg, &b_lambda_chg);
   fChain->SetBranchAddress("lambda_pdg", &lambda_pdg, &b_lambda_pdg);
   Notify();
}

Bool_t ntpLambda::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ntpLambda::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ntpLambda::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ntpLambda_cxx
