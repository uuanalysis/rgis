#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;

bool CheckFile(TString fn);

void ana_vtx(TString prefix="llbar_fwp", Int_t from=1, Int_t to=1) {
    
    // USAGE: root -l -b -q 'ana_ip.C("llbar_fwp",1,1)'
    
    TString suffix = "sim"; 
    	
    // If Local Files
    TString inParFile = prefix+"_par.root";
	TString inSimFile = prefix+"_sim.root";
    TString outFile = "out.root";
    
    // Else Cluster Files
    /*
	TString inParFile = TString::Format("%s_%d_par.root", prefix.Data(), from);
	TString inSimFile = TString::Format("%s_%d_%s.root", prefix.Data(), from, suffix.Data());
	TString outFile = TString::Format("%s_ip.root", prefix.Data()); 
	*/
	 
	TString pidParFile = TString(gSystem->Getenv("VMCWORKDIR"))+"/macro/params/all.par";	
    TFile *out = TFile::Open(prefix+"_ip.root", "RECREATE");  //Output for Histograms (Outside of FairTask)
    
    // ************************************************************************
    //				   Access TObject using FairRunAna, FairLinks
    // ************************************************************************
    
    // *** Timer
    TStopwatch timer;
    timer.Start();
    
    // *** FairRunAna
 	FairLogger::GetLogger()->SetLogToFile(kFALSE);
	FairRunAna *fRun = new FairRunAna();
	FairFileSource *fSrc = new FairFileSource(inSimFile);  
    
    // add multiple files w/ format: prefix_num_suffix.root
  	for (int i=from+1; i<=to; ++i) {
        TString fname = TString::Format("%s_%d_%s.root", prefix.Data(), i, suffix.Data());
        if (CheckFile(fname)) fSrc->AddFile(fname);
  	}
	
	fRun->SetSource(fSrc);
    fRun->SetUseFairLinks(kTRUE);
    
    // *** FairRuntimeDb
    FairRuntimeDb* rtdb = fRun->GetRuntimeDb();	
	FairParRootFileIo* parIO = new FairParRootFileIo();
	parIO->open(inParFile);
	FairParAsciiFileIo* parIOPid = new FairParAsciiFileIo();
	parIOPid->open(pidParFile.Data(),"in");
    
    rtdb->setFirstInput(parIO);
	rtdb->setSecondInput(parIOPid);
	rtdb->setOutput(parIO);
	rtdb->setContainersStatic();
	fRun->SetOutputFile(outFile);
    
    fRun->Init();
        
    // FairRootManager::Instance()
    FairRootManager* ioman = FairRootManager::Instance();
    
    
    // ------------------------------------------------------------------------
    //					  			TClonesArrays
    // ------------------------------------------------------------------------
    TClonesArray* mcTrackArray = (TClonesArray*) ioman->GetObject("MCTrack");

    // ------------------------------------------------------------------------
    //					  				FairLinks
    // ------------------------------------------------------------------------
    FairMultiLinkedData fLinks;


    // ------------------------------------------------------------------------
    //					  			Histograms
    // ------------------------------------------------------------------------
    
    //***
	//----- Primary Vertices
	//***
		
	//Primary Vertex: rz-plane
	TH2F *prodvtx = new TH2F("prodvtx","Primary Vertex (#Lambda #bar{#Lambda}) @1.642 GeV;z/cm;r/cm",1000,-537,1080.5,500,0,10);
	TH2F *lam0_prodvtx = new TH2F("lam0_prodvtx","Primary Vertex (#Lambda) @1.642 GeV;z/cm;r/cm",1000,-537,1080.5,500,0,10);
    TH2F *lam0bar_prodvtx = new TH2F("lam0bar_prodvtx","Primary Vertex (#bar{#Lambda}) @1.642 GeV;z/cm;r/cm",1000,-537,1080.5,500,0,10);

	//Primary Vertex: z-projection
	TH1F *prodvtx_z = new TH1F("prodvtx_z","Primary Vertex (#Lambda #bar{#Lambda}) @1.642 GeV;z/cm;Entries",1000,-537,1080.5);
	prodvtx_z->GetXaxis()->SetTitle("z [cm]");
	prodvtx_z->GetYaxis()->SetTitle("Entries");
	
	TH1F *lam0_prodvtx_z = new TH1F("lam0_prodvtx_z","Primary Vertex (#Lambda) @1.642 GeV;z/cm;Entries",1000,-537,1080.5);
	lam0_prodvtx_z->GetXaxis()->SetTitle("z [cm]");
	lam0_prodvtx_z->GetYaxis()->SetTitle("Entries");
	
    TH1F *lam0bar_prodvtx_z = new TH1F("lam0bar_prodvtx_z","Primary Vertex (#bar{#Lambda}) @1.642 GeV;z/cm;Entries",1000,-537,1080.5);
    lam0bar_prodvtx_z->GetXaxis()->SetTitle("z [cm]");
	lam0bar_prodvtx_z->GetYaxis()->SetTitle("Entries");
	


    //***
	//----- Decay Vertices
	//***
	
	//Decay Vertex: rz-plane
	TH2F *decayvtx = new TH2F("decayvtx","Decay Vertex (#Lambda #bar{#Lambda}) @1.642 GeV;z/cm;r/cm",1000,-537,1080.5,500,0,25);
	TH2F *lam0_decayvtx = new TH2F("lam0_decayvtx","Decay Vertex (#Lambda) @1.642 GeV;z/cm;r/cm",1000,-537,1080.5,500,0,25);
	TH2F *lam0bar_decayvtx = new TH2F("lam0bar_decayvtx","Decay Vertex (#bar{#Lambda}) @1.642 GeV;z/cm;r/cm",1000,-537,1080.5,500,0,25);

	
	//Decay Vertex: rz-plane for MVD, STT
	TH2F *decayvtx_MVD = new TH2F("decayvtx_MVD","Decay Vertex (#Lambda #bar{#Lambda}, MVD) @1.642 GeV",1000,-150,500,500,0,50);
	decayvtx_MVD->GetXaxis()->SetTitle("z [cm]");
	decayvtx_MVD->GetYaxis()->SetTitle("r [cm]");
	
	TH2F *decayvtx_STT = new TH2F("decayvtx_STT","Decay Vertex (#Lambda #bar{#Lambda}, STT) @1.642 GeV",1000,-150,500,500,0,50);
	decayvtx_STT->GetXaxis()->SetTitle("z [cm]");
	decayvtx_STT->GetYaxis()->SetTitle("r [cm]");
	
    
    //Unique Particles (default: ascending)
    set<int> particles;      
    
    //Counters
	int pbarp=0, lam=0, lambar=0, p=0, pbar=0, piminus=0, piplus=0;
	int vertex_MVD=0, vertex_STT=0, vertex_inside_STT=0;
	int counter=0;
	
	
    // ------------------------------------------------------------------------
    //					  			Event Loop
    // ------------------------------------------------------------------------
    
    Int_t ev_entries = Int_t((ioman->GetInChain())->GetEntries());
    for (Int_t event=0; event < 10000; ++event) {
    
	    ioman->ReadEvent(event);
	    
	    //std::cout << "Reading Event: " << event << std::endl;
	    //std::cout << "Size of mcTrackArray: " << mcTrackArray->GetEntries() << std::endl;
	    
	    		//MCTrackLoop:
		for (Int_t mc=0; mc < mcTrackArray->GetEntries(); mc++) {
		
			PndMCTrack *mcTrack = (PndMCTrack *) mcTrackArray->At(mc);
			
			/*
		    * NOTE: We don't have a function to ask a MC track for its decay vertex,
		    * only its starting point, therefore you need to ask the protons for their
		    * starting point or vertex to get the decay vertex of the lambda (c.c.).
		    *
		    *          i.e decay_vertex(lamda) := start_vertex(proton/pion)
		    */
		    
		    /*
		    * IsGeneratorCreated(): For all EvtGen Tracks (Lambdas, Protons, Pions)
		    *    IsGeneratorLast(): For daughters only (Protons, Pions). Both works
		    */
		    
			if (mcTrack->IsGeneratorCreated()) {
			    
			    //Counter
			    counter++;
	        
	            //Add Particles (Unique)
	            particles.insert(mcTrack->GetPdgCode());
	        
	            //Exclude Mother Particle
	            if (mcTrack->GetPdgCode()==88888)   //this is pbarpsystem
	            {
	                pbarp++;
	                continue;
		        }        
			
				// ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
				//              Tasks for Particles & Anti-particles
				// ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
		        if (mcTrack->GetPdgCode()==3122 | mcTrack->GetPdgCode()==-3122)  // Lambda, Anti-lambda
			    //if (mcTrack->GetPdgCode()==2212 | mcTrack->GetPdgCode()==-2212)  // Proton, Anti-proton
			    //if (mcTrack->GetPdgCode()==211 | mcTrack->GetPdgCode()==-211)  // Pion+, Pion-
                {   
                    
					//Get the StartVertex (TVector3)
					TVector3 vertexpos = mcTrack->GetStartVertex();
					
					//Primary Vertex
					prodvtx->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
					prodvtx_z->Fill(mcTrack->GetStartVertex().Z());
					
					/*
					//Decay Vertex inside MVD
					if (vertexpos.Perp()>=0 && vertexpos.Perp()<13.5) { // r[cm]
						if (vertexpos.Z()>-20 && vertexpos.Z()<19) { // z[cm]
						
							decayvtx_MVD->Fill(vertexpos.Z(), vertexpos.Perp());
							vertex_MVD++;
						}
						
					}//endif
					
					//Decay Vertex inside STT
					if (vertexpos.Perp()>15 && vertexpos.Perp()<42) {
						if (vertexpos.Z()>-55 && vertexpos.Z()<110) {
						
							decayvtx_STT->Fill(vertexpos.Z(), vertexpos.Perp());
							vertex_STT++;
						}
					}//endif
					
					//Count Entries for upto STT Coverage
					if (vertexpos.Perp()>=0 && vertexpos.Perp()<42) {
						if (vertexpos.Z()>-55 && vertexpos.Z()<110) {
						
							vertex_inside_STT++;
						}
					}//endif
					*/
					
				}//endif-PdgCode
				
				// ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
				//                 Tasks for Individual Particles
				// ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
			    switch (mcTrack->GetPdgCode()) 
			    {
			        //lam0
					case 3122:
						lam0_prodvtx->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
                        lam0_prodvtx_z->Fill(mcTrack->GetStartVertex().Z());
						lam++;
						break;
					
					//lam0bar
					case -3122:
					    lam0bar_prodvtx->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
                        lam0bar_prodvtx_z->Fill(mcTrack->GetStartVertex().Z());
                        lambar++;
						break;
					
					//p
					case 2212:
					    decayvtx->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
					    lam0_decayvtx->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
						p++;
						break;
					
					//pbar
					case -2212:
					    decayvtx->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
					    lam0bar_decayvtx->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
						pbar++;
						break;
					
					//pi+	
					case 211:
						piplus++;
						break;
					
					//pi-	
					case -211:
						piminus++;
						break;
						
					default:
						;;
						
				}//endswitch-PdgCode
				
		    }//endif-IsGenerator
	    }//endfor-MCTrack
	}//endfor-event

	// ------------------------------------------------------------------------
	//					  				Plotting
	// ------------------------------------------------------------------------	
	
	//Draw Primary Vertex with XY Projection
	//Histogram projections always contain double values !
    //TH1* hx = h2->ProjectionX(); // ! TH1D, not TH1F
    //TH1* hy = h2->ProjectionY(); // ! TH1D, not TH1F
    
	TCanvas *c1 = new TCanvas("c1","Vertex Distribution",1500,500);
	
	c1->Divide(3,1);
	c1->cd(1); gPad->SetLogz(); prodvtx->Draw("colz");
	c1->cd(2); gPad->SetLogz(); prodvtx->ProjectionX()->Draw("bar");
	c1->cd(3); gPad->SetLogz(); prodvtx->ProjectionY()->Draw("bar");
	c1->SaveAs("c1.png");
    c1->Close();
	        
    TCanvas *c2 = new TCanvas("c2","Vertex Distribution",1500,500);
    c2->Divide(3,1);
    c2->cd(1); prodvtx_z->Draw();
    c2->cd(2); fDensityProfile->Draw();
    c2->cd(3); prodvtx_z->Draw();
    c2->SaveAs("c2.png");
    c2->Close();
    
	// ------------------------------------------------------------------------
	//					  				Write
	// ------------------------------------------------------------------------
	out->cd();
	
	// production vertex
	prodvtx->Write();
	lam0_prodvtx->Write();
	lam0bar_prodvtx->Write();
	
	prodvtx_z->Write();
	lam0_prodvtx_z->Write();
	lam0bar_prodvtx_z->Write();
	
	// decay vertex
	decayvtx->Write();
	lam0_decayvtx->Write();
	lam0bar_decayvtx->Write();
	
	//decayvtx_MVD->Write();
	//decayvtx_STT->Write();
	
	
	out->Save();

    // ************************************************************************
    //				   				  End Timer
    // ************************************************************************
    timer.Stop();
    Double_t rtime = timer.RealTime();
    Double_t ctime = timer.CpuTime();
    cout << endl << endl;

    // ------------------------------------------------------------------------
    std::cout << "     Total Lambda Count: " << lam << std::endl;
    std::cout << "Total Anti-lambda Count: " << lambar << std::endl;
    std::cout << "     Total Proton Count: " << p << std::endl;
    std::cout << "Total Anti-proton Count: " << pbar << std::endl;
    std::cout << "  Total Pion Plus Count: " << piplus << std::endl;
    std::cout << " Total Pion Minus Count: " << piminus << std::endl;
    std::cout << "    Total Primary Count: " << (lam+lambar+p+pbar+piminus+piplus) << std::endl;
    std::cout << "      Total PbarP Count: " << pbarp << std::endl;
    std::cout << "   Total Particle Count: " << (pbarp+lam+lambar+p+pbar+piminus+piplus) << std::endl;
    
    std::cout << "\nParticles: ";
    
    // Iterator for the set
    set<int> :: iterator it;
    
    // Print the elements of the set
    for(it=particles.begin(); it!=particles.end(); it++)
        std::cout << *it <<" ";
        
    std::cout << std::endl;
	// ------------------------------------------------------------------------
    
    cout << "\nMacro finished succesfully." << endl;
    cout << "Output file is " << outFile << endl;
    cout << "Parameter file is " << inParFile << endl;
    cout << "Real time " << rtime << " s, CPU time " << ctime << " s" << endl;

    
}//end-macro


bool CheckFile(TString fn) {

    bool fileok=true;
    TFile fff(fn); 
    if (fff.IsZombie()) fileok=false;

    TTree *t=(TTree*)fff.Get("cbmsim");
    if (t==0x0) fileok=false;

    if (!fileok) std::cout << "Skipping Broken File: '"<< fn << "'" << std::endl;
    return fileok;
}

