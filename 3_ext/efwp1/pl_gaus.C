#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>

#include "dGausFit.C"
#include "DrawOpt.C"


// https://root.cern/manual/fitting/
// https://root-forum.cern.ch/t/fitting-with-two-gaussian-curves/26390/7

void pl_gaus() {

    /**
    * @brief Performes a Double Gaussian Fit
    */

    //*** Open Root File Containing Histograms
    TFile *f = TFile::Open("ntpBestPbarP.root");
    if (!f) return;
    f->ls();

    //*** Fetch Specific Histograms from the Root File
    TString hist_name = "";
    hist_name = "hlam0_m_cut1";
    hist_name = "hlam0bar_m_cut1";
    
    TH1D *h1 = (TH1D*)(f->Get(hist_name));

    //*** Styling
    DrawOpt(h1);

    //*** Cloning
    TH1D *hist = (TH1D*)h1->Clone("htemp");
 
    //*************************************************************************
    //                          Double Gaussian Fitting
    //*************************************************************************
    
    // Let's get Double Gaussian Fit Function from doubleGaussFit(). One needs 
    // to do extra work to plot all Gaussian functions along side a histogram.
    // TF1 *dGaus=doubleGaussFit(hist, 0.01623, 0.03623, kFALSE);
    
    // OR, use explicity here
    //double mean = hist->GetMean();
    //double inner = 0.01623, outer=0.03623;
    
    double parameters[6] = {0,0,0,0,0,0};
    
    //*** 1st/Inner Gaussian
    TF1 *G1 = new TF1 ("G1","gaus",1.095,1.125);
    //TF1 *G1 = new TF1 ("G1","gaus",1.105,1.127);
    G1->SetLineColor(kMagenta);
    G1->SetLineStyle(3);
    hist->Fit(G1, "Q0R");
    
    G1->GetParameters(&parameters[0]);
    
    //*** 2nd/Outer Gaussian
    TF1 *G2 = new TF1 ("G2","gaus",1.105,1.135);
    //TF1 *G2 = new TF1 ("G2","gaus",1.08,1.16);
    G2->SetLineColor(kCyan);
    G2->SetLineStyle(4);
    hist->Fit(G2, "Q0R");
    
    G2->GetParameters(&parameters[3]);
    
    //*** 3rd/Combined Gaussian
    TF1 *G3 = new TF1 ("G3","gaus(0)+gaus(3)",1.095,1.135);
    //TF1 *G3 = new TF1 ("G3","gaus(0)+gaus(3)",1.08,1.16);
    G3->SetParameters(parameters);
    
    //*** Params (Default)
    // p0=const1, p1=mean1, p2=sigma1, p3=const2, p4=mean2, p5=sigma2
    
    //*** Params (User-defined)
    G3->SetParName(0, "c_{1}");
    G3->SetParName(1, "#mu_{1}");
    G3->SetParName(2, "#sigma_{1}");
    G3->SetParName(3, "c_{2}");
    G3->SetParName(4, "#mu_{2}");
    G3->SetParName(5, "#sigma_{2}");
    
    G3->SetLineColor(kRed);
    G3->SetLineStyle(1);    
    hist->Fit(G3,"Q0R");
    
    
    //*************************************************************************
    // 									TCanvas
    //*************************************************************************
    TCanvas *c1 = new TCanvas("c1","Single Gaussian Fits (G1, G2)",0,0,1500,500);
    c1->Divide(3,1);
    c1->cd(1); hist->Draw(); G1->Draw("SAME");
    c1->cd(2); hist->Draw(); G2->Draw("SAME");
    c1->cd(3); hist->Draw(); G1->Draw("SAME"); G2->Draw("SAME"); 
    //c1->SaveAs(("gauss"+hist_name+"_1.png"));
    c1->Close();

    TCanvas *c2 = new TCanvas("c2","Double Gaussian Fits (G3)",0,0,600,500);    
    hist->Draw();
    G3->Draw("SAME");
    c2->SaveAs(("gauss_"+hist_name+".png"));
    //c2->Close();
   
}//end-macro
