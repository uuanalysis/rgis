//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Apr 30 19:57:09 2021 by ROOT version 6.16/00
// from TTree ntpLambdaBar/LambdaBar Info.
// found on file: ebkg1-LambdaBar.root
//////////////////////////////////////////////////////////

#ifndef ntpLambdaBar_h
#define ntpLambdaBar_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class ntpLambdaBar {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           VtxFit_HowGood;
   Float_t         VtxFit_chisq;
   Float_t         VtxFit_ndf;
   Float_t         VtxFit_prob;
   Float_t         VtxFit_vx;
   Float_t         VtxFit_vy;
   Float_t         VtxFit_vz;
   Float_t         VtxFit_len;
   Float_t         VtxFit_ctau;
   Float_t         VtxFit_ctaud;
   Float_t         lambdabar_px;
   Float_t         lambdabar_py;
   Float_t         lambdabar_pz;
   Float_t         lambdabar_e;
   Float_t         lambdabar_p;
   Float_t         lambdabar_tht;
   Float_t         lambdabar_phi;
   Float_t         lambdabar_pt;
   Float_t         lambdabar_m;
   Float_t         lambdabar_x;
   Float_t         lambdabar_y;
   Float_t         lambdabar_z;
   Float_t         lambdabar_l;
   Float_t         lambdabar_chg;
   Float_t         lambdabar_pdg;

   // List of branches
   TBranch        *b_VtxFit_HowGood;   //!
   TBranch        *b_VtxFit_chisq;   //!
   TBranch        *b_VtxFit_ndf;   //!
   TBranch        *b_VtxFit_prob;   //!
   TBranch        *b_VtxFit_vx;   //!
   TBranch        *b_VtxFit_vy;   //!
   TBranch        *b_VtxFit_vz;   //!
   TBranch        *b_VtxFit_len;   //!
   TBranch        *b_VtxFit_ctau;   //!
   TBranch        *b_VtxFit_ctaud;   //!
   TBranch        *b_lambdabar_px;   //!
   TBranch        *b_lambdabar_py;   //!
   TBranch        *b_lambdabar_pz;   //!
   TBranch        *b_lambdabar_e;   //!
   TBranch        *b_lambdabar_p;   //!
   TBranch        *b_lambdabar_tht;   //!
   TBranch        *b_lambdabar_phi;   //!
   TBranch        *b_lambdabar_pt;   //!
   TBranch        *b_lambdabar_m;   //!
   TBranch        *b_lambdabar_x;   //!
   TBranch        *b_lambdabar_y;   //!
   TBranch        *b_lambdabar_z;   //!
   TBranch        *b_lambdabar_l;   //!
   TBranch        *b_lambdabar_chg;   //!
   TBranch        *b_lambdabar_pdg;   //!

   ntpLambdaBar(TTree *tree=0);
   virtual ~ntpLambdaBar();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   TH1D *vtxf_chi2;
   TH1D *vtxf_prob;
   TH1D *vtxf_ndf;
      
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
};

#endif

#ifdef ntpLambdaBar_cxx
ntpLambdaBar::ntpLambdaBar(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   TString path = "";
   TFile* hfile = TFile::Open(path+"ntpLambdaBar.root", "RECREATE");
   
   vtxf_chi2 = new TH1D("vtxf_chi2","Chi-square (VxtFit);#chi^{2};Entries",200,0,300);
   vtxf_prob = new TH1D("vtxf_prob","Probability (VxtFit);probability;Entries",200,0,1);
   vtxf_ndf = new TH1D("vtxf_ndf","NDF (VxtFit);ndf;Entries",100,0,10);

   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------    

   // ---------------------------------------------------------------
   //                    This Block Shouldn't be Modified.
   // ---------------------------------------------------------------
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("ebkg1-LambdaBar.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("ebkg1-LambdaBar.root");
      }
      f->GetObject("ntpLambdaBar",tree);

   }
   // Call Init()
   Init(tree);

   // Call Loop() 
   Loop();
   // ---------------------------------------------------------------
   //                    This Block Shouldn't be Modified.
   // ---------------------------------------------------------------
   
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   hfile->Write();
   hfile->Close();
   
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
}

ntpLambdaBar::~ntpLambdaBar()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ntpLambdaBar::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ntpLambdaBar::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ntpLambdaBar::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("VtxFit_HowGood", &VtxFit_HowGood, &b_VtxFit_HowGood);
   fChain->SetBranchAddress("VtxFit_chisq", &VtxFit_chisq, &b_VtxFit_chisq);
   fChain->SetBranchAddress("VtxFit_ndf", &VtxFit_ndf, &b_VtxFit_ndf);
   fChain->SetBranchAddress("VtxFit_prob", &VtxFit_prob, &b_VtxFit_prob);
   fChain->SetBranchAddress("VtxFit_vx", &VtxFit_vx, &b_VtxFit_vx);
   fChain->SetBranchAddress("VtxFit_vy", &VtxFit_vy, &b_VtxFit_vy);
   fChain->SetBranchAddress("VtxFit_vz", &VtxFit_vz, &b_VtxFit_vz);
   fChain->SetBranchAddress("VtxFit_len", &VtxFit_len, &b_VtxFit_len);
   fChain->SetBranchAddress("VtxFit_ctau", &VtxFit_ctau, &b_VtxFit_ctau);
   fChain->SetBranchAddress("VtxFit_ctaud", &VtxFit_ctaud, &b_VtxFit_ctaud);
   fChain->SetBranchAddress("lambdabar_px", &lambdabar_px, &b_lambdabar_px);
   fChain->SetBranchAddress("lambdabar_py", &lambdabar_py, &b_lambdabar_py);
   fChain->SetBranchAddress("lambdabar_pz", &lambdabar_pz, &b_lambdabar_pz);
   fChain->SetBranchAddress("lambdabar_e", &lambdabar_e, &b_lambdabar_e);
   fChain->SetBranchAddress("lambdabar_p", &lambdabar_p, &b_lambdabar_p);
   fChain->SetBranchAddress("lambdabar_tht", &lambdabar_tht, &b_lambdabar_tht);
   fChain->SetBranchAddress("lambdabar_phi", &lambdabar_phi, &b_lambdabar_phi);
   fChain->SetBranchAddress("lambdabar_pt", &lambdabar_pt, &b_lambdabar_pt);
   fChain->SetBranchAddress("lambdabar_m", &lambdabar_m, &b_lambdabar_m);
   fChain->SetBranchAddress("lambdabar_x", &lambdabar_x, &b_lambdabar_x);
   fChain->SetBranchAddress("lambdabar_y", &lambdabar_y, &b_lambdabar_y);
   fChain->SetBranchAddress("lambdabar_z", &lambdabar_z, &b_lambdabar_z);
   fChain->SetBranchAddress("lambdabar_l", &lambdabar_l, &b_lambdabar_l);
   fChain->SetBranchAddress("lambdabar_chg", &lambdabar_chg, &b_lambdabar_chg);
   fChain->SetBranchAddress("lambdabar_pdg", &lambdabar_pdg, &b_lambdabar_pdg);
   Notify();
}

Bool_t ntpLambdaBar::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ntpLambdaBar::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ntpLambdaBar::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ntpLambdaBar_cxx
