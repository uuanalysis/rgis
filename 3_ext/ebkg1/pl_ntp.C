#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

//using namespace std;
using std::cout;
using std::endl;

void DrawOpt(TH1D* hist);

void pl_ntp() {

    /**
    * Macro to fit histogram in root file with 
    * Single Gaussian or Double Gaussian functions.
    */

    //*** Open Histogram Root File
    TFile *f = TFile::Open("ntpBestPbarP.root");
    if (!f) return;
    f->ls();

    //gStyle->SetOptStat(1101);                // SetOptStat(1) == SetOptStat(1111)
    //gStyle->SetOptFit(0111);                 // SetOptFit(1) == SetOptFit(111)
    //gStyle->SetTitleSize(0.1,"t");
    //gStyle->SetStatW(0.2);
    //gStyle->SetLabelOffset(1.2);
    //gStyle->SetLabelFont(5);

    //*************************************************************************
    //                               Histograms
    //*************************************************************************
    TString cut = "";
    TString hist_name = "";
    
    //----- Before Final Cuts.
    hist_name = "hlam0_m";             
    hist_name = "hlam0bar_m";
    hist_name = "hpbarp_m";
    //hist_name = "hpbarp_chi2";
    //hist_name = "hpbarp_prob";
    //hist_name = "h_dvzsum_llbar";
    //hist_name = "h_m_llbar";  // 2D

    //----- After Final Cuts.
    //hist_name = "hlam0_m_cut3";             
    //hist_name = "hlam0bar_m_cut3";
    //hist_name = "hlam0_thtcm";
    //hist_name = "hlam0bar_thtcm";
    
    
    
    TH1D *h1 = (TH1D*)(f->Get(hist_name));

    //Mean & STD of Mass
    Double_t mm = h1->GetMean(); 
    Double_t rms= h1->GetRMS();
    
    std::cout <<"Mean: " << mm << " RMS: " << rms << std::endl;
    //*************************************************************************
    //                               Styling
    //*************************************************************************

    DrawOpt(h1);

    //*************************************************************************
    // 									   TCanvas
    //*************************************************************************

    TCanvas *c1 = new TCanvas("c1","Plotting",0,0,600,500);
    //c1->SetGridx();
    //c1->SetGridy();
    //c1->SetLeftMargin(0.10);
    //c1->SetRightMargin(0.10);
    //c1->SetTopMargin(0.11);
    //c1->SetBottomMargin(0.11);
    c1->SetLogy();
    //c1->SetLogz();
    h1->Draw("COLZ");
    
    
    
    //Draw Lines: TLine::TLine(Double_t x1, Double_t y1, Double_t x2, Double_t y2)
    
    //Line#1:
	TLine *line1 = new TLine(100,0,100,10000);     // Chi2
	//TLine *line1 = new TLine(2.0,0,2.0,19000);      // z_lam0 + z_lam0bar
	//TLine *line1 = new TLine(1.101,0.,1.101,100000);      // mass (lam0/lam0bar)
	line1->SetLineColor(2);
	line1->SetLineWidth(1.0);
	//line1->Draw();
    
    //Line#2:
    TLine *line2 = new TLine(1.130,0.,1.130,100000);  // mass (lam0/lam0bar)
    //TLine *line2 = new TLine(1.130,0.,1.130,1.8);  // mass (lam0/lam0bar)
	line2->SetLineColor(2);
	line2->SetLineWidth(1.0);
	//line2->Draw();
	
	//Line#3:
    TLine *line3 = new TLine(0.8,1.101,1.8,1.101);  // mass (lam0/lam0bar)
	line3->SetLineColor(2);
	line3->SetLineWidth(1.0);
	//line3->Draw();
	
	//Line#4:
    TLine *line4 = new TLine(0.8,1.131,1.8,1.131);  // mass (lam0/lam0bar)
	line4->SetLineColor(2);
	line4->SetLineWidth(1.0);
	//line4->Draw();
	
    //Draw Labels: TLatex::TLatex(Double_t x, Double_t y, const char *text)
	//TLatex latex1(100,15,"#chi^{2} < 100");
	//latex1.SetTextFont(42);
	//latex1.SetTextColor(2);
	//latex1.SetTextSize(0.04);
	//latex1.DrawClone();
	
	//OR, try new 
	TLatex latex;
    latex.SetTextSize(0.025);
    latex.SetTextAlign(13);                       //align at top
    //latex.DrawLatex(105,10000,"#chi^{2} < 100");   // Chi2
	
    c1->Draw();
    c1->SaveAs((cut+hist_name+".png"));
    //c1->Close();



}//end-macro


void DrawOpt(TH1D* hist)
{
    /*
    *
    * Funtion to apply styling on histograms.	
    *
    */

    // *** Number of Bins (X-axis)
    Int_t xbins = hist->GetNbinsX();

    //*** Covert to TString for Labels using std::to_string() method from <strings>.
    //*** For integers std::to_string works fine, for floats use std::stringstream.
    TString nbins(std::to_string(xbins));

    //std::cout << "Number of Bins (X-axis): " << xbins << std::endl;
    std::cout << "Number of Bins (X-axis): " << nbins << std::endl;

    // *** Bin Width (X-axis)
    Float_t xbin_width = hist->GetBinWidth(0);


    //*** Covert to TString for Labels using std::stringstream class & str() method from <sstream>.
    //*** The std::stringstream has advantage over std::to_string() in case of floats and doubles.

    std::ostringstream oss;
    oss << std::setprecision(2) << std::noshowpoint << xbin_width;

    //std::string str = oss.str();   // oss.str() returns std::string
    //TString bin_width(str);        // std::string to TString

    TString bin_width(oss.str());

    //std::cout << "Bin Width (X-axis): " << xbin_width << std::endl;
    std::cout << "Bin Width (X-axis): " << bin_width << std::endl;

    // ***	
    // *** --- Fix the X-axis
    // ***
    //hist->GetXaxis()->SetRangeUser(2.25,2.45);
    hist->GetXaxis()->SetLabelSize(0.02);
    hist->GetXaxis()->SetTitleSize(0.03);
    hist->GetXaxis()->SetTitleOffset(1.25);
    hist->GetXaxis()->CenterTitle();

    // Fix X-axis Labels
    TString xlabel = "";
    xlabel = "m(#Lambda) /GeV/c^{2}";             // mass/GeV/c^{2}
    //xlabel = "m(#bar{#Lambda}) /GeV/c^{2}";       // mass/GeV/c^{2}
    //xlabel = "m(#bar{p}p) /GeV/c^{2}";            // mass/GeV/c^{2}
    //xlabel = "#chi^{2}";                            // chi2 (4c)
    //xlabel = "prob";                            // prob (4c)
    //xlabel = "z_{#Lambda} + z_{#bar{#Lambda}}";
    //xlabel = "z(p#pi^{-})";
    //hist->GetXaxis()->SetTitle(xlabel);

    // ***
    // *** --- Fix the Y-axis
    // ***

    //hist->GetYaxis()->SetRangeUser(0,900);
    hist->GetYaxis()->SetLabelSize(0.02);
    hist->GetYaxis()->SetTitleSize(0.03);
    hist->GetYaxis()->SetTitleOffset(1.25);
    hist->GetYaxis()->CenterTitle();

    // Fix Y-axis Labels
    TString ylabel = "";
    ylabel = "dN/dM/"+bin_width+" /GeV/c^{2}";    // dN/dM/bin_width/GeV/c^{2}
    //ylabel = "Entries/GeV/c^{2}";                 // Entries/GeV/c^{2}
    //ylabel = "Entries";
    //ylabel = "z(#bar{p}#pi^{+})";
    //hist->GetYaxis()->SetTitle(ylabel);


    // ***
    // *** --- Fix the Title
    // ***
    TString title = "";

    //title = "m(#Lambda_{0})";
    //title = "#bar{#Lambda_{0}}: Mass";
    //title = "#bar{p}p: Mass";
    //title = "z_{#Lambda} + z_{#bar{#Lambda}}";
    //title = "p #bar{p} #rightarrow p#pi^{-} #bar{p}#pi^{+}";
    //title = "p #bar{p} #rightarrow #Lambda#bar{#Lambda} #rightarrow p#pi^{-} #bar{p}#pi^{+}";
    //title = "Signal Sample (Extended Target)";
    title = "Background Sample (Extended Target)";
    
    hist->SetTitle(title);
    hist->SetTitleSize(0.1,"t");                 // gStyle->SetTitleSize(0.3,“t”);


    // ***
    // *** Fix the StatBox ***
    // ***

    hist->SetStats(kTRUE);
    gStyle->SetStatX(0.89);
    gStyle->SetStatY(0.89);


    // ***
    // *** Fixing the Layout
    // ***

    hist->SetLineColor(kBlue);
    hist->SetFillColor(kCyan);
    hist->SetLineWidth(1);


}//end-DrawOpt

