#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;

bool CheckFile(TString fn);
void ana_ip(TString prefix="efwp1", Int_t from=1, Int_t to=500) {
    
    /*
     * @brief: analyse the IP from MC (sim.root) for primary or 
     * secondary vertex of the final state particles in reaction.
     * THIS will be the main 'ana_ip.C' macro for this purpose.
     */

    TString input = "", path = "";
    //path = "./efwp1/";
    path = "./"+prefix+"/";
    TString parFile  = TString::Format(path+"%s_%d_par.root", prefix.Data(), from);
    TString simFile  = TString::Format(path+"%s_%d_sim.root", prefix.Data(), from);
    //TString digiFile = TString::Format(path+"%s_%d_digi.root", prefix.Data(), from);
    //TString recoFile = TString::Format(path+"%s_%d_reco.root", prefix.Data(), from);
    TString outFile  = "out.root";  // SetOutputFile() of the FairRunAna
    TFile *out = TFile::Open(prefix+"_ip.root", "RECREATE"); //output for the Histograms
    
    // *** Timer
    TStopwatch timer;
    timer.Start();

    // ************************************************************************
    //                 Access TObject using FairRunAna, FairLinks
    // ************************************************************************

    // *** PndMasterRunAna (Jenny)
    // FIXME: see how multiple files can be added

    /*
    PndMasterRunAna *fRun= new PndMasterRunAna();
    fRun->SetInput(input);
    fRun->AddFriend(recoFile);
    fRun->AddFriend(simFile);
    fRun->AddFriend(digiFile);
    fRun->SetOutput(outFile);
    fRun->SetParamAsciiFile(parAsciiFile);
    fRun->Setup(prefix);
    fRun->SetUseFairLinks(kTRUE);
    fRun->Init();
    */

    // ------------------------------------------------------------------------
    //				   Access TObject using FairRunAna, FairLinks
    // ------------------------------------------------------------------------

    // FairRunAna
    FairLogger::GetLogger()->SetLogToFile(kFALSE);
    FairRunAna *fRun = new FairRunAna();
    fRun->SetUseFairLinks(kTRUE);

    // (1) Add First File to FairFileSource
    FairFileSource *fSrc = new FairFileSource(simFile);


    // (2) Add Rest Files to FairFileSource
    for (int i=from+1; i<=to; ++i) {
        
        TString simFile  = TString::Format(path+"%s_%d_sim.root", prefix.Data(), i);
        std::cout << "Adding File: " << simFile << std::endl;
        if (CheckFile(simFile)) fSrc->AddFile(simFile);
        
    }

    // Add File Source
    fRun->SetSource(fSrc);

    // Add Output File to FairRootFileSink
    FairRootFileSink *fSink = new FairRootFileSink(outFile);
    fRun->SetSink(fSink);

    // FairRuntimeDb
    FairRuntimeDb* rtdb = fRun->GetRuntimeDb();
    FairParRootFileIo* parInput1 = new FairParRootFileIo();
    parInput1->open(parFile.Data());

    // FairParAsciiFileIo
    FairParAsciiFileIo* parIo1 = new FairParAsciiFileIo();
    TString allDigiFile = gSystem->Getenv("VMCWORKDIR"); 
    allDigiFile += "/macro/params/all.par";
    parIo1->open(allDigiFile.Data(), "in");
    rtdb->setFirstInput(parInput1);
    rtdb->setSecondInput(parIo1);

    // FairRunAna::Init()
    fRun->Init();

    // FairRootManager::Instance()
    FairRootManager* ioman = FairRootManager::Instance();

    // ------------------------------------------------------------------------
    //                                  TClonesArrays
    // ------------------------------------------------------------------------
    TClonesArray* mcTrackArray = (TClonesArray*) ioman->GetObject("MCTrack");

    // ------------------------------------------------------------------------
    //					  				FairLinks
    // ------------------------------------------------------------------------
    FairMultiLinkedData fLinks;

    // ------------------------------------------------------------------------
    //					  			Histograms
    // ------------------------------------------------------------------------
    
    //Normalization
    Int_t norm=0;
    
    //Unique Particles
    set<int> particles;

    //Particle Counters
    int pbarp=0, lam=0, lambar=0, p=0, pbar=0, piminus=0, piplus=0;
    int vertex_MVD=0, vertex_STT=0, vertex_inside_STT=0;
    int counter=0;

    //Production Vertex: rz-plane
    TH2D *hprodvtx_lam0 = new TH2D("hprodvtx_lam0","Primary Vertex (#Lambda) @1.642 GeV;z/cm;R/cm",1000,-537,1080.5,500,0,10);
    TH2D *hprodvtx_lam0bar = new TH2D("hprodvtx_lam0bar","Primary Vertex (#bar{#Lambda}) @1.642 GeV;z/cm;R/cm",1000,-537,1080.5,500,0,10);
    TH2D *hprodvtx_llbar = new TH2D("hprodvtx_llbar","Primary Vertex (#Lambda #bar{#Lambda}) @1.642 GeV;z/cm;R/cm",1000,-537,1080.5,500,0,10);
    
    TH2D *hprodvtx_p = new TH2D("hprodvtx_p","Primary Vertex (p) @1.642 GeV;z/cm;R/cm",1000,-537,1080.5,500,0,10);
    TH2D *hprodvtx_pbar = new TH2D("hprodvtx_pbar","Primary Vertex (#bar{p}) @1.642 GeV;z/cm;R/cm",1000,-537,1080.5,500,0,10);
    TH2D *hprodvtx_pbarp = new TH2D("hprodvtx_pbarp","Primary Vertex (p#bar{p}) @1.642 GeV;z/cm;R/cm",1000,-537,1080.5,500,0,10);

    //Production Vertex: z-projection
    TH1D *hprodvtx_z_lam0 = new TH1D("hprodvtx_z_lam0","Primary Vertex (#Lambda) @1.642 GeV;z/cm;Entries",1000,-537,1080.5);
    TH1D *hprodvtx_z_lam0bar = new TH1D("hprodvtx_z_lam0bar","Primary Vertex (#bar{#Lambda}) @1.642 GeV;z/cm;Entries",1000,-537,1080.5);
    TH1D *hprodvtx_zm_lam0bar = new TH1D("hprodvtx_zm_lam0bar","Primary Vertex (#bar{#Lambda}) @1.642 GeV;z/cm;Entries",1000,-10,10);
    TH1D *hprodvtx_z_llbar = new TH1D("hprodvtx_z_llbar","Primary Vertex (#Lambda #bar{#Lambda}) @1.642 GeV;z/cm;Entries",1000,-537,1080.5);
    
    TH1D *hprodvtx_z_p = new TH1D("hprodvtx_z_p","Primary Vertex (p) @1.642 GeV;z/cm;Entries",1000,-537,1080.5);
    TH1D *hprodvtx_z_pbar = new TH1D("hprodvtx_z_pbar","Primary Vertex (#bar{p}) @1.642 GeV;z/cm;Entries",1000,-537,1080.5);
    TH1D *hprodvtx_z_pbarp = new TH1D("hprodvtx_z_pbarp","Primary Vertex (p #bar{p}) @1.642 GeV;z/cm;Entries",1000,-537,1080.5);
    
    TH1D *hprodvtx_zm_pbar = new TH1D("hprodvtx_zm_pbar","Primary Vertex (#bar{p}) @1.642 GeV;z/cm;Entries",1000,-5,5);
    TH1D *hprodvtx_zn_pbar = new TH1D("hprodvtx_zn_pbar","Primary Vertex (#bar{p}) @1.642 GeV;z/cm;Entries",1000,-5,5);

    
    // ------------------------------------------------------------------------
    //					  			Event Loop
    // ------------------------------------------------------------------------
    std::cout << "\nSample Type: " << prefix << std::endl;
    
    Int_t ev_entries = Int_t((ioman->GetInChain())->GetEntries());
    for (Int_t event=0; event < ev_entries; ++event) 
    {
	    ioman->ReadEvent(event);
	    if (!(event%10000)) std::cout << "-I- Processing Event No.: "<< event << std::endl;
	    
	    for (Int_t mc=0; mc < mcTrackArray->GetEntries(); mc++) {
		    PndMCTrack *mcTrack = (PndMCTrack *) mcTrackArray->At(mc);
		    if (mcTrack->IsGeneratorCreated()) {
		        
                //Counter
                counter++;

                //Add Particles (Unique)
                particles.insert(mcTrack->GetPdgCode());

                //std::cout << "\nMC Index: " << mc << std::endl;
                //std::cout << "Particle: " << mcTrack->GetPdgCode() << std::endl;
                //std::cout << "GetStartVertex().Z(): " << mcTrack->GetStartVertex().Z() << std::endl;
                //std::cout << "mcTrack->GetNPoints(): " << mcTrack->GetNPoints(DetectorId::kSTT) << std::endl;
                //mcTrack->Print();

                // ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
                //                   Exclude Mother Particle
                // ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
                if (mcTrack->GetPdgCode()==88888){

                    //this is pbarpsystem
                    pbarp++; continue;

                }//endif

                // ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
                //              Tasks for Particles & Anti-particles
                // ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
                if (mcTrack->GetPdgCode()==3122 | mcTrack->GetPdgCode()==-3122){  

                    //Production Vertex
                    hprodvtx_llbar->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
                    hprodvtx_z_llbar->Fill(mcTrack->GetStartVertex().Z());
                    
                }//endif

                if (mcTrack->GetPdgCode()==2212 | mcTrack->GetPdgCode()==-2212){
                
                    //Production Vertex
                    hprodvtx_pbarp->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
                    hprodvtx_z_pbarp->Fill(mcTrack->GetStartVertex().Z());
                    
                }//endif
                
                // ----- ----- ----- ----- ----- ----- ----- ----- ----- -----
                //                 Tasks for Individual Particles
                // ----- ----- ----- ----- ----- ----- ----- ----- ----- -----

                switch (mcTrack->GetPdgCode()) {
                
                    case 3122://lambda
                        //std::cout << "MC Index: " << mc << std::endl;
                        //std::cout << "Particle: " << mcTrack->GetPdgCode() << std::endl;
                        //std::cout << "GetStartVertex().Z(): " << mcTrack->GetStartVertex().Z() << std::endl;
                        hprodvtx_lam0->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
                        hprodvtx_z_lam0->Fill(mcTrack->GetStartVertex().Z());
                        lam++;
                        break;

                    case -3122://lambdabar
                        //std::cout << "MC Index: " << mc << std::endl;
                        //std::cout << "Particle: " << mcTrack->GetPdgCode() << std::endl;
                        //std::cout << "GetStartVertex().Z(): " << mcTrack->GetStartVertex().Z() << std::endl;
                        hprodvtx_lam0bar->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
                        hprodvtx_z_lam0bar->Fill(mcTrack->GetStartVertex().Z());
                        hprodvtx_zm_lam0bar->Fill(mcTrack->GetStartVertex().Z());
                        lambar++;
                        break;

                    case 2212://proton
                        hprodvtx_p->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
                        hprodvtx_z_p->Fill(mcTrack->GetStartVertex().Z());
                        p++;
                        break;

                    case -2212://antiproton
                        hprodvtx_pbar->Fill(mcTrack->GetStartVertex().Z(), mcTrack->GetStartVertex().Perp());
                        hprodvtx_z_pbar->Fill(mcTrack->GetStartVertex().Z());
                        hprodvtx_zm_pbar->Fill(mcTrack->GetStartVertex().Z());
                        pbar++;
                        
                        //normalization (from z=-0.1 to z=0)
                        if (mcTrack->GetStartVertex().Z() > -0.1 && mcTrack->GetStartVertex().Z() < 1.0) {
                            hprodvtx_zn_pbar->Fill(mcTrack->GetStartVertex().Z());
                            norm++;
                        }
                        break;

                }//endswitch
				
		    }//endif-IsGenerator
		    
	    }//endfor-MCTrack
	    
    }//endfor-event

    // ------------------------------------------------------------------------
    //					  				Plotting
    // ------------------------------------------------------------------------

    //Draw Primary Vertex with XY Projection
    //Histogram projections always contain double values !
    //TH1* hx = hprodvtx_llbar->ProjectionX(); // ! TH1D, not TH1D
    //TH1* hy = hprodvtx_llbar->ProjectionY(); // ! TH1D, not TH1D

    /*
    TCanvas *c1 = new TCanvas("c1","lam0, lam0bar",1800,600);
    c1->Divide(3,1);
    c1->cd(1); gPad->SetLogz(); hprodvtx_llbar->Draw("colz");
    c1->cd(2); gPad->SetGridx(); gPad->SetGridy(); hprodvtx_llbar->ProjectionX()->Draw("bar");
    c1->cd(3); gPad->SetGridx(); gPad->SetGridy(); hprodvtx_llbar->ProjectionY()->Draw("bar");
    c1->SaveAs("prodvtx_llbar.png");
    //c1->Close();


    TCanvas *c2 = new TCanvas("c2","lam0, lam0bar",1800,600);
    c2->Divide(3,1);
    c2->cd(1); hprodvtx_z_llbar->Draw("bar");
    c2->cd(2); hprodvtx_z_lam0->Draw("bar");
    c2->cd(3); hprodvtx_z_lam0bar->Draw("bar");
    c2->SaveAs("prodvtx_llbar_z.png");
    //c2->Close();
    

    TCanvas *c3 = new TCanvas("c3","lam0",750,600);
    hprodvtx_z_lam0->GetXaxis()->SetRangeUser(-100,200);
    hprodvtx_z_lam0->SetLineColor(kBlue);
    hprodvtx_z_lam0->SetFillColor(kGreen+3);
    hprodvtx_z_lam0->Draw();
    //c3->SaveAs("prodvtx_lam0_z.png");
    c3->Close();

    TCanvas *c4 = new TCanvas("c4","lam0bar",750,600);
    hprodvtx_z_lam0bar->GetXaxis()->SetRangeUser(-100,200);
    hprodvtx_z_lam0bar->SetLineColor(kBlue);
    hprodvtx_z_lam0bar->SetFillColor(kGreen+3);
    c4->SetLogy();
    hprodvtx_z_lam0bar->Draw();
    //c4->SaveAs("prodvtx_lam0bar_z.png");
    c4->Close();
    */
    
    // ------------------------------------------------------------------------
    //					  				Storing
    // ------------------------------------------------------------------------
    out->cd();
    
    // ------- rz
    hprodvtx_lam0->Write();
    hprodvtx_lam0bar->Write();
    hprodvtx_llbar->Write();
    
    hprodvtx_p->Write();
    hprodvtx_pbar->Write();
    hprodvtx_pbarp->Write();
    
    // ------- z
    hprodvtx_z_lam0->Write();
    hprodvtx_z_lam0bar->Write();
    hprodvtx_z_llbar->Write();
    
    hprodvtx_z_p->Write();
    hprodvtx_z_pbar->Write();
    hprodvtx_z_pbarp->Write();
    
    //z_zoomed
    hprodvtx_zm_lam0bar->Write();
    hprodvtx_zm_pbar->Write();
    hprodvtx_zn_pbar->Write();
    
    out->Save();

    // ************************************************************************
    //				   				  End Timer
    // ************************************************************************
    timer.Stop();
    Double_t rtime = timer.RealTime();
    Double_t ctime = timer.CpuTime();
    cout << endl << endl;

    // ------------------------------------------------------------------------
    std::cout << "     Total Lambda Count: " << lam << std::endl;
    std::cout << "Total Anti-lambda Count: " << lambar << std::endl;
    std::cout << "     Total Proton Count: " << p << std::endl;
    std::cout << "Total Anti-proton Count: " << pbar << std::endl;
    std::cout << "  Total Pion Plus Count: " << piplus << std::endl;
    std::cout << " Total Pion Minus Count: " << piminus << std::endl;
    std::cout << "    Total Primary Count: " << (lam+lambar+p+pbar+piminus+piplus) << std::endl;
    std::cout << "      Total PbarP Count: " << pbarp << std::endl;
    std::cout << "   Total Particle Count: " << (pbarp+lam+lambar+p+pbar+piminus+piplus) << std::endl;
    std::cout << "   Normalization Factor: " << norm << std::endl;

    std::cout << "\nParticles: ";

    // Iterator for the set
    set<int> :: iterator it;

    // Print the elements of the set
    for(it=particles.begin(); it!=particles.end(); it++)
        std::cout << *it <<" ";
        
    std::cout << std::endl;
    // ------------------------------------------------------------------------

    cout << "\nMacro finished succesfully." << endl;
    cout << "Output file is " << outFile << endl;
    cout << "Parameter file is " << parFile << endl;
    cout << "Real time " << rtime << " s, CPU time " << ctime << " s" << endl;

}//end-macro

bool CheckFile(TString fn) {

    bool fileok=true;
    TFile fff(fn); 
    if (fff.IsZombie()) fileok=false;

    TTree *t=(TTree*)fff.Get("pndsim");
    if (t==0x0) fileok=false;

    if (!fileok) std::cout << "Skipping Broken File: '"<< fn << "'" << std::endl;
    return fileok;
}

