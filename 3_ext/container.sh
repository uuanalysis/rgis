#!/bin/sh

if [ $# -lt 1 ]; then
  echo -e "\nMacro File is Necessary.\n"
  echo -e "USAGE: ./container.sh <macro.C>"
  exit 1
fi

FILE=$1
#CONTAINER=$HOME/fair/v12.0.3.sif      # pandaroot v12.0.3 container.
CONTAINER=$HOME/fair/dev.sif           # pandaroot dev (Aug 1, 2021) container.

# Either invoke default-command in runscript, OR run a command in container from outside. 
# Note "-c" flag required if non-interactive shell is invoked inside runscript. For example,

# Let's use "run" to run default commands in "runscript"
# singularity run $CONTAINER root -l -q -b $FILE  && exit         # Works w/ interactive shell
# singularity run $CONTAINER -c "root -l -q -b $FILE" && exit     # Works w/ non-interactive shell

# Let's use "exec" to run a command inside container
singularity exec $CONTAINER root -l -q -b $FILE && exit
