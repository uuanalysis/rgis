

// SampleInteractionVertex
double fRlimit=2.0;                   // Radius of beam pipe (hard limit)
double fRsigma=0.0;                   // Radius of beam profile (Gaussian sigma)
double fDrDz=0.0;                     // Radius increase slope from (0,0,0)
double fDmin=0.0;                     // Minimum density
double fDmax=0.0;                     // Maximum density
bool fDoExtended=true;                // Extended or Non-extended


void DrawOpt(TH1F* fHist);
void DrawOptG(TGraph* fGraph);
TVector3 SampleInteractionVertex(TGraph *fCDFi);

void dProfile_V2() {

    // --------------------------------------------------------------
    //                          Read Density Profile
    // --------------------------------------------------------------

    std::cout << "Reading Density File" << std::endl;

    // Build TGraph from File
    TGraph* fdGraph = new TGraph("./RestGas/restgasthickness_210421_Normal_IP_no_Cryopump.txt"); //NormalIP
    //TGraph* fdGraph = new TGraph("./RestGas/restgasthickness_210421_Normal_IP_with_Cryopump.txt"); //NormalIP+Cryo
    //TGraph* fdGraph = new TGraph("./RestGas/restgasthickness_210629_Normal_IP_with_Cryopump.txt"); //NormalIP+Cryo (Update)
    //TGraph* fdGraph = new TGraph("./RestGas/restgasthickness_210421_Big_IP_no_Cryopump.txt"); //BigIP
    //TGraph* fdGraph = new TGraph("./RestGas/restgasthickness_210421_Big_IP_with_Cryopump.txt");  //BigIP+Cryo
    
    // Make fdGraph
    fdGraph->SetTitle("Measured Target & Rest Gas Density Profile;z/cm;#rho /10^{12} atoms/cm^{2}");
    //fdGraph->Print();
    
    int npoints = fdGraph->GetN();
    double ozmin, ozmax, odmin, odmax;
    fdGraph->ComputeRange(ozmin, odmin, ozmax, odmax); // x: z /cm  y: density /units
    
    // Make fdGraphCorr
    TGraph *fdGraphCorr = new TGraph(npoints);
    fdGraphCorr->SetTitle("Measured Target & Rest Gas Density Profile;z /cm;#rho /10^{12} atoms/cm^{2}");
    
    // Make fCDF & fCDFi
    TGraph *fCDF = new TGraph(npoints);
    fCDF->SetTitle("Measured Target & Rest Gas Density Profile (CDF);z /cm;#Sigma#rho /10^{12} atoms/cm^{2}");
    
    TGraph *fCDFi = new TGraph(npoints);
    fCDFi->SetTitle("Measured Target & Rest Gas Density Profile (Inverted CDF);#Sigma#rho /10^{12} atoms/cm^{2};z /cm");
    
    
    double x=0., y=0., ysum=0., dcor=0.;
    double xlast=-5.530045000000000073e+02;
    for (int i=0; i<npoints; i++) {
    
        // fetch one point
        fdGraph->GetPoint(i,x,y);           // Get Point
        ysum+=y;                            // Running ysum
        fCDF->SetPoint(i,x,ysum);           // Fill CDF  
        fCDFi->SetPoint(i,ysum,x);          // Fill Inverted CDF to acces x by random y
        dcor=y/(x-xlast);                   // Density correction ??? 
        fdGraphCorr->SetPoint(i,x,dcor);    // Fill Corrected/Scaled fdGraph
        xlast=x;                            // Recursive xlast
        
        //std::cout << "z = " << x << "   d = " << y << "   dcor = " << dcor << std::endl;
    }
    
    // --------------------------------------------------------------
    //                    Lets Sample 10000 IP Points
    // --------------------------------------------------------------
    double zmin, zmax, dmin, dmax;
    fCDFi->ComputeRange(dmin, zmin, dmax, zmax); // x: density  y: z /cm

    TH1F *hz = new TH1F("hz","Sampled Target & Rest Gas Density Profile;z /cm;#rho /10^{12} atoms/cm^{2}",1000,-600,1075);
    
    for(int i=0; i<1000000; i++) {
        
        //TVector3 result = SampleInteractionVertex();
        //hz->Fill(result.Z());
        
        //OR, directly access 'z'
        double drand = gRandom->Uniform(dmin,dmax);                  // random cummulated density
        double z = fCDFi->Eval(drand);                        // random from z profile
        hz->Fill(z);
        
        //double z = fCDFi->Eval(drand,nullptr,"S");          // random from z profile with TSpline3
        //hz->Fill(z);
        
        //std::cout << "drand = " << drand << "   z = " << z << std::endl;
    }
    
    //---- Normalization:
    //if (hz->GetSumw2N()==0) 
    
    //Double_t scale = 1./hz->GetEntries();  // Method#2
    //Double_t scale = 1./hz->Integral();  // Method#4
    //Double_t scale = 1./hz->Integral("width");  // Method#7&8
    //hz->Sumw2(kFALSE);
    //hz->Scale(scale); 
    
    // --------------------------------------------------------------
    //                          Plotting
    // --------------------------------------------------------------
    
    /*
    TCanvas *c1 = new TCanvas("c1","c1",0,0,1500,500);
    c1->Divide(3,1);
    
    c1->cd(1); 
    gPad->SetLogy(true); 
    fdGraph->Draw(); // Raw
    
    c1->cd(2); 
    gPad->SetLogy(false);
    fCDF->Draw(); // CDF
    
    c1->cd(3); 
    gPad->SetLogy(false); 
    fCDFi->Draw(); // Inverted CDF
    
    c1->Print("dProfile.png");
    c1->Close();
    */
    
    std::cout << "zmin: " << zmin << " zmax: " << zmax << std::endl;
    
    TCanvas *c2 = new TCanvas("c2","c2",0,0,1000,500);
    c2->Divide(2,1);
    
    c2->cd(1); 
    gPad->SetLogy(true);
    fdGraphCorr->GetXaxis()->SetRangeUser(zmin-29.9,zmax+0.22); // along X
    fdGraphCorr->GetHistogram()->SetMaximum(1e4);     // along y
    fdGraphCorr->GetHistogram()->SetMinimum(1e-4);    // along y
    DrawOptG(fdGraphCorr);
    fdGraphCorr->Draw();
    
    c2->cd(2); 
    gPad->SetLogy(true); DrawOpt(hz); hz->Draw();
    
    //c2->Print("zProfile.png");
    c2->Close();
    
    
    TCanvas *c3 = new TCanvas("c3","c3",0,0,600,500);
    gPad->SetLogy(true);
    fdGraphCorr->GetXaxis()->SetRangeUser(zmin-29.9,zmax+0.22); // along X
    fdGraphCorr->GetHistogram()->SetMaximum(1e4);     // along y
    fdGraphCorr->GetHistogram()->SetMinimum(1e-5);    // along y
    DrawOptG(fdGraphCorr);
    fdGraphCorr->Draw();
    c3->Print("NormalIP.png");
    //c3->Close();
    
}//end-macro

// Helper Functions:
TVector3 SampleInteractionVertex(TGraph *fCDFi) {

    if (fDoExtended) {
    
        // Actual calculation of a random interaction vertex
        double x, y, z;
        double r = 2 * fRlimit;
        double drand = gRandom->Uniform(fDmin, fDmax);        // random cummulated density
        z = fCDFi->Eval(drand);                        // --> random from z profile
        while (r > fRlimit) {
            r = gRandom->Gaus(0, fRsigma + fabs(z) * fDrDz);  // increase beam spread by opning slope
        }

        gRandom->Circle(x, y, r);                             // random phi angle, directly converted to (x,y)
        TVector3 result(x, y, z);
        return result;
        
    } else {
    
        TVector3 zero(0., 0., 0.);
        return zero;
    }
}//end

void DrawOptG(TGraph *fGraph) {

    /*
    @brief: TGraph Decoration
    */
    
    //fGraph->GetXaxis()->SetRangeUser(zmin-29.9,zmax+0.22); // range along X
    fGraph->GetXaxis()->SetLabelFont(43);
    fGraph->GetXaxis()->SetLabelSize(15);
    fGraph->GetXaxis()->SetLabelOffset(0.01);
    fGraph->GetXaxis()->SetTitleFont(43);
    fGraph->GetXaxis()->SetTitleSize(18);
    fGraph->GetXaxis()->SetTitleOffset(1.25);
    //fGraph->GetXaxis()->CenterTitle();
    
    //fGraph->GetHistogram()->SetMaximum(1e4);     // range along y (max value)
    //fGraph->GetHistogram()->SetMinimum(1e-5);    // range along y (min value)
    fGraph->GetYaxis()->SetLabelFont(43);
    fGraph->GetYaxis()->SetLabelSize(15);
    fGraph->GetYaxis()->SetLabelOffset(0.01);
    fGraph->GetYaxis()->SetTitleFont(43);
    fGraph->GetYaxis()->SetTitleSize(18);
    fGraph->GetYaxis()->SetTitleOffset(1.25);
    //fGraph->GetYaxis()->CenterTitle();
    
    fGraph->SetTitle("Measured Target & Rest Gas Density Profile;z /cm;#rho /10^{12} atoms/cm^{2}");
    
    //fGraph->SetLineColor(kBlue);
    //fGraph->SetFillColor(kCyan);
    //fGraph->SetLineWidth(1);

}

void DrawOpt(TH1F *fHist) {
    
    /*
    @brief: TH1 Decoration
    */
    
    fHist->GetXaxis()->SetLabelFont(43);
    fHist->GetXaxis()->SetLabelSize(13);
    fHist->GetXaxis()->SetLabelOffset(0.01);
    fHist->GetXaxis()->SetTitleFont(43);
    fHist->GetXaxis()->SetTitleSize(15);
    fHist->GetXaxis()->SetTitleOffset(1.25);
    //fHist->GetXaxis()->CenterTitle();
    
    
    fHist->GetYaxis()->SetLabelFont(43);
    fHist->GetYaxis()->SetLabelSize(13);
    fHist->GetYaxis()->SetLabelOffset(0.01);
    fHist->GetYaxis()->SetTitleFont(43);
    fHist->GetYaxis()->SetTitleSize(15);
    fHist->GetYaxis()->SetTitleOffset(1.5);
    //fHist->GetYaxis()->CenterTitle();
    
    fHist->SetTitleSize(0.1,"t");
    fHist->SetStats(kTRUE);
    gStyle->SetStatX(0.89);
    gStyle->SetStatY(0.89);
    
    //fHist->SetLineColor(kBlue);
    //fHist->SetFillColor(kCyan);
    //fHist->SetLineWidth(1);

}

