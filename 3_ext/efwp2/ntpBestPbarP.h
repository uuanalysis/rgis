//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Apr 10 23:20:40 2021 by ROOT version 6.16/00
// from TTree ntpBestPbarP/Best PbarPSystem Info.
// found on file: efwp2-BestPbarP.root
//////////////////////////////////////////////////////////

#ifndef ntpBestPbarP_h
#define ntpBestPbarP_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class ntpBestPbarP {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UChar_t         McTruthMatch;
   UChar_t         FourMomFit_Convergence;
   Float_t         FourMomFit_chisq;
   Float_t         FourMomFit_ndf;
   Float_t         FourMomFit_prob;
   Float_t         d0d0_ky;
   Float_t         d1d0_ky;
   Float_t         MC_d0d0_ky;
   Float_t         MC_d1d0_ky;
   Float_t         pbarp_px;
   Float_t         pbarp_py;
   Float_t         pbarp_pz;
   Float_t         pbarp_e;
   Float_t         pbarp_p;
   Float_t         pbarp_tht;
   Float_t         pbarp_phi;
   Float_t         pbarp_pt;
   Float_t         pbarp_m;
   Float_t         pbarp_x;
   Float_t         pbarp_y;
   Float_t         pbarp_z;
   Float_t         pbarp_l;
   Float_t         pbarp_chg;
   Float_t         pbarp_pdg;
   Float_t         pbarp_pxcm;
   Float_t         pbarp_pycm;
   Float_t         pbarp_pzcm;
   Float_t         pbarp_ecm;
   Float_t         pbarp_pcm;
   Float_t         pbarp_thtcm;
   Float_t         pbarp_phicm;
   Float_t         pbarp_covpxpx;
   Float_t         pbarp_covpxpy;
   Float_t         pbarp_covpxpz;
   Float_t         pbarp_covpxe;
   Float_t         pbarp_covpypy;
   Float_t         pbarp_covpypz;
   Float_t         pbarp_covpye;
   Float_t         pbarp_covpzpz;
   Float_t         pbarp_covpze;
   Float_t         pbarp_covee;
   Float_t         pbarp_pullpx;
   Float_t         pbarp_pullpy;
   Float_t         pbarp_pullpz;
   Float_t         pbarp_pulle;
   Float_t         pbarp_pullx;
   Float_t         pbarp_pully;
   Float_t         pbarp_pullz;
   Float_t         pbarp_pullpxpy;
   Float_t         pbarp_pullpxpz;
   Float_t         pbarp_pullpypz;
   Float_t         pbarp_pullpxe;
   Float_t         pbarp_pullpye;
   Float_t         pbarp_pullpze;
   Float_t         pbarp_pullxy;
   Float_t         pbarp_pullxz;
   Float_t         pbarp_pullyz;
   Float_t         pbarp_mct;
   Float_t         pbarp_mdif;
   Float_t         pbarp_oang;
   Float_t         pbarp_decang;
   Float_t         pbarp_cdecang;
   Float_t         pbarp_d0px;
   Float_t         pbarp_d0py;
   Float_t         pbarp_d0pz;
   Float_t         pbarp_d0e;
   Float_t         pbarp_d0p;
   Float_t         pbarp_d0tht;
   Float_t         pbarp_d0phi;
   Float_t         pbarp_d0pt;
   Float_t         pbarp_d0m;
   Float_t         pbarp_d0x;
   Float_t         pbarp_d0y;
   Float_t         pbarp_d0z;
   Float_t         pbarp_d0l;
   Float_t         pbarp_d0chg;
   Float_t         pbarp_d0pdg;
   Float_t         pbarp_d0pxcm;
   Float_t         pbarp_d0pycm;
   Float_t         pbarp_d0pzcm;
   Float_t         pbarp_d0ecm;
   Float_t         pbarp_d0pcm;
   Float_t         pbarp_d0thtcm;
   Float_t         pbarp_d0phicm;
   Float_t         pbarp_d0covpxpx;
   Float_t         pbarp_d0covpxpy;
   Float_t         pbarp_d0covpxpz;
   Float_t         pbarp_d0covpxe;
   Float_t         pbarp_d0covpypy;
   Float_t         pbarp_d0covpypz;
   Float_t         pbarp_d0covpye;
   Float_t         pbarp_d0covpzpz;
   Float_t         pbarp_d0covpze;
   Float_t         pbarp_d0covee;
   Float_t         pbarp_d0pullpx;
   Float_t         pbarp_d0pullpy;
   Float_t         pbarp_d0pullpz;
   Float_t         pbarp_d0pulle;
   Float_t         pbarp_d0pullx;
   Float_t         pbarp_d0pully;
   Float_t         pbarp_d0pullz;
   Float_t         pbarp_d0pullpxpy;
   Float_t         pbarp_d0pullpxpz;
   Float_t         pbarp_d0pullpypz;
   Float_t         pbarp_d0pullpxe;
   Float_t         pbarp_d0pullpye;
   Float_t         pbarp_d0pullpze;
   Float_t         pbarp_d0pullxy;
   Float_t         pbarp_d0pullxz;
   Float_t         pbarp_d0pullyz;
   Float_t         pbarp_d0mct;
   Float_t         pbarp_d0mdif;
   Float_t         pbarp_d0oang;
   Float_t         pbarp_d0decang;
   Float_t         pbarp_d0cdecang;
   Float_t         pbarp_d0d0px;
   Float_t         pbarp_d0d0py;
   Float_t         pbarp_d0d0pz;
   Float_t         pbarp_d0d0e;
   Float_t         pbarp_d0d0p;
   Float_t         pbarp_d0d0tht;
   Float_t         pbarp_d0d0phi;
   Float_t         pbarp_d0d0pt;
   Float_t         pbarp_d0d0m;
   Float_t         pbarp_d0d0x;
   Float_t         pbarp_d0d0y;
   Float_t         pbarp_d0d0z;
   Float_t         pbarp_d0d0l;
   Float_t         pbarp_d0d0chg;
   Float_t         pbarp_d0d0pdg;
   Float_t         pbarp_d0d0pxcm;
   Float_t         pbarp_d0d0pycm;
   Float_t         pbarp_d0d0pzcm;
   Float_t         pbarp_d0d0ecm;
   Float_t         pbarp_d0d0pcm;
   Float_t         pbarp_d0d0thtcm;
   Float_t         pbarp_d0d0phicm;
   Float_t         pbarp_d0d0covpxpx;
   Float_t         pbarp_d0d0covpxpy;
   Float_t         pbarp_d0d0covpxpz;
   Float_t         pbarp_d0d0covpxe;
   Float_t         pbarp_d0d0covpypy;
   Float_t         pbarp_d0d0covpypz;
   Float_t         pbarp_d0d0covpye;
   Float_t         pbarp_d0d0covpzpz;
   Float_t         pbarp_d0d0covpze;
   Float_t         pbarp_d0d0covee;
   Float_t         pbarp_d0d0pullpx;
   Float_t         pbarp_d0d0pullpy;
   Float_t         pbarp_d0d0pullpz;
   Float_t         pbarp_d0d0pulle;
   Float_t         pbarp_d0d0pullx;
   Float_t         pbarp_d0d0pully;
   Float_t         pbarp_d0d0pullz;
   Float_t         pbarp_d0d0pullpxpy;
   Float_t         pbarp_d0d0pullpxpz;
   Float_t         pbarp_d0d0pullpypz;
   Float_t         pbarp_d0d0pullpxe;
   Float_t         pbarp_d0d0pullpye;
   Float_t         pbarp_d0d0pullpze;
   Float_t         pbarp_d0d0pullxy;
   Float_t         pbarp_d0d0pullxz;
   Float_t         pbarp_d0d0pullyz;
   Float_t         pbarp_d0d0mct;
   Float_t         pbarp_d0d0pide;
   Float_t         pbarp_d0d0pidmu;
   Float_t         pbarp_d0d0pidpi;
   Float_t         pbarp_d0d0pidk;
   Float_t         pbarp_d0d0pidp;
   Float_t         pbarp_d0d0pidmax;
   Float_t         pbarp_d0d0pidbest;
   Float_t         pbarp_d0d0trpdg;
   Float_t         pbarp_d0d1px;
   Float_t         pbarp_d0d1py;
   Float_t         pbarp_d0d1pz;
   Float_t         pbarp_d0d1e;
   Float_t         pbarp_d0d1p;
   Float_t         pbarp_d0d1tht;
   Float_t         pbarp_d0d1phi;
   Float_t         pbarp_d0d1pt;
   Float_t         pbarp_d0d1m;
   Float_t         pbarp_d0d1x;
   Float_t         pbarp_d0d1y;
   Float_t         pbarp_d0d1z;
   Float_t         pbarp_d0d1l;
   Float_t         pbarp_d0d1chg;
   Float_t         pbarp_d0d1pdg;
   Float_t         pbarp_d0d1pxcm;
   Float_t         pbarp_d0d1pycm;
   Float_t         pbarp_d0d1pzcm;
   Float_t         pbarp_d0d1ecm;
   Float_t         pbarp_d0d1pcm;
   Float_t         pbarp_d0d1thtcm;
   Float_t         pbarp_d0d1phicm;
   Float_t         pbarp_d0d1covpxpx;
   Float_t         pbarp_d0d1covpxpy;
   Float_t         pbarp_d0d1covpxpz;
   Float_t         pbarp_d0d1covpxe;
   Float_t         pbarp_d0d1covpypy;
   Float_t         pbarp_d0d1covpypz;
   Float_t         pbarp_d0d1covpye;
   Float_t         pbarp_d0d1covpzpz;
   Float_t         pbarp_d0d1covpze;
   Float_t         pbarp_d0d1covee;
   Float_t         pbarp_d0d1pullpx;
   Float_t         pbarp_d0d1pullpy;
   Float_t         pbarp_d0d1pullpz;
   Float_t         pbarp_d0d1pulle;
   Float_t         pbarp_d0d1pullx;
   Float_t         pbarp_d0d1pully;
   Float_t         pbarp_d0d1pullz;
   Float_t         pbarp_d0d1pullpxpy;
   Float_t         pbarp_d0d1pullpxpz;
   Float_t         pbarp_d0d1pullpypz;
   Float_t         pbarp_d0d1pullpxe;
   Float_t         pbarp_d0d1pullpye;
   Float_t         pbarp_d0d1pullpze;
   Float_t         pbarp_d0d1pullxy;
   Float_t         pbarp_d0d1pullxz;
   Float_t         pbarp_d0d1pullyz;
   Float_t         pbarp_d0d1mct;
   Float_t         pbarp_d0d1pide;
   Float_t         pbarp_d0d1pidmu;
   Float_t         pbarp_d0d1pidpi;
   Float_t         pbarp_d0d1pidk;
   Float_t         pbarp_d0d1pidp;
   Float_t         pbarp_d0d1pidmax;
   Float_t         pbarp_d0d1pidbest;
   Float_t         pbarp_d0d1trpdg;
   Float_t         pbarp_d1px;
   Float_t         pbarp_d1py;
   Float_t         pbarp_d1pz;
   Float_t         pbarp_d1e;
   Float_t         pbarp_d1p;
   Float_t         pbarp_d1tht;
   Float_t         pbarp_d1phi;
   Float_t         pbarp_d1pt;
   Float_t         pbarp_d1m;
   Float_t         pbarp_d1x;
   Float_t         pbarp_d1y;
   Float_t         pbarp_d1z;
   Float_t         pbarp_d1l;
   Float_t         pbarp_d1chg;
   Float_t         pbarp_d1pdg;
   Float_t         pbarp_d1pxcm;
   Float_t         pbarp_d1pycm;
   Float_t         pbarp_d1pzcm;
   Float_t         pbarp_d1ecm;
   Float_t         pbarp_d1pcm;
   Float_t         pbarp_d1thtcm;
   Float_t         pbarp_d1phicm;
   Float_t         pbarp_d1covpxpx;
   Float_t         pbarp_d1covpxpy;
   Float_t         pbarp_d1covpxpz;
   Float_t         pbarp_d1covpxe;
   Float_t         pbarp_d1covpypy;
   Float_t         pbarp_d1covpypz;
   Float_t         pbarp_d1covpye;
   Float_t         pbarp_d1covpzpz;
   Float_t         pbarp_d1covpze;
   Float_t         pbarp_d1covee;
   Float_t         pbarp_d1pullpx;
   Float_t         pbarp_d1pullpy;
   Float_t         pbarp_d1pullpz;
   Float_t         pbarp_d1pulle;
   Float_t         pbarp_d1pullx;
   Float_t         pbarp_d1pully;
   Float_t         pbarp_d1pullz;
   Float_t         pbarp_d1pullpxpy;
   Float_t         pbarp_d1pullpxpz;
   Float_t         pbarp_d1pullpypz;
   Float_t         pbarp_d1pullpxe;
   Float_t         pbarp_d1pullpye;
   Float_t         pbarp_d1pullpze;
   Float_t         pbarp_d1pullxy;
   Float_t         pbarp_d1pullxz;
   Float_t         pbarp_d1pullyz;
   Float_t         pbarp_d1mct;
   Float_t         pbarp_d1mdif;
   Float_t         pbarp_d1oang;
   Float_t         pbarp_d1decang;
   Float_t         pbarp_d1cdecang;
   Float_t         pbarp_d1d0px;
   Float_t         pbarp_d1d0py;
   Float_t         pbarp_d1d0pz;
   Float_t         pbarp_d1d0e;
   Float_t         pbarp_d1d0p;
   Float_t         pbarp_d1d0tht;
   Float_t         pbarp_d1d0phi;
   Float_t         pbarp_d1d0pt;
   Float_t         pbarp_d1d0m;
   Float_t         pbarp_d1d0x;
   Float_t         pbarp_d1d0y;
   Float_t         pbarp_d1d0z;
   Float_t         pbarp_d1d0l;
   Float_t         pbarp_d1d0chg;
   Float_t         pbarp_d1d0pdg;
   Float_t         pbarp_d1d0pxcm;
   Float_t         pbarp_d1d0pycm;
   Float_t         pbarp_d1d0pzcm;
   Float_t         pbarp_d1d0ecm;
   Float_t         pbarp_d1d0pcm;
   Float_t         pbarp_d1d0thtcm;
   Float_t         pbarp_d1d0phicm;
   Float_t         pbarp_d1d0covpxpx;
   Float_t         pbarp_d1d0covpxpy;
   Float_t         pbarp_d1d0covpxpz;
   Float_t         pbarp_d1d0covpxe;
   Float_t         pbarp_d1d0covpypy;
   Float_t         pbarp_d1d0covpypz;
   Float_t         pbarp_d1d0covpye;
   Float_t         pbarp_d1d0covpzpz;
   Float_t         pbarp_d1d0covpze;
   Float_t         pbarp_d1d0covee;
   Float_t         pbarp_d1d0pullpx;
   Float_t         pbarp_d1d0pullpy;
   Float_t         pbarp_d1d0pullpz;
   Float_t         pbarp_d1d0pulle;
   Float_t         pbarp_d1d0pullx;
   Float_t         pbarp_d1d0pully;
   Float_t         pbarp_d1d0pullz;
   Float_t         pbarp_d1d0pullpxpy;
   Float_t         pbarp_d1d0pullpxpz;
   Float_t         pbarp_d1d0pullpypz;
   Float_t         pbarp_d1d0pullpxe;
   Float_t         pbarp_d1d0pullpye;
   Float_t         pbarp_d1d0pullpze;
   Float_t         pbarp_d1d0pullxy;
   Float_t         pbarp_d1d0pullxz;
   Float_t         pbarp_d1d0pullyz;
   Float_t         pbarp_d1d0mct;
   Float_t         pbarp_d1d0pide;
   Float_t         pbarp_d1d0pidmu;
   Float_t         pbarp_d1d0pidpi;
   Float_t         pbarp_d1d0pidk;
   Float_t         pbarp_d1d0pidp;
   Float_t         pbarp_d1d0pidmax;
   Float_t         pbarp_d1d0pidbest;
   Float_t         pbarp_d1d0trpdg;
   Float_t         pbarp_d1d1px;
   Float_t         pbarp_d1d1py;
   Float_t         pbarp_d1d1pz;
   Float_t         pbarp_d1d1e;
   Float_t         pbarp_d1d1p;
   Float_t         pbarp_d1d1tht;
   Float_t         pbarp_d1d1phi;
   Float_t         pbarp_d1d1pt;
   Float_t         pbarp_d1d1m;
   Float_t         pbarp_d1d1x;
   Float_t         pbarp_d1d1y;
   Float_t         pbarp_d1d1z;
   Float_t         pbarp_d1d1l;
   Float_t         pbarp_d1d1chg;
   Float_t         pbarp_d1d1pdg;
   Float_t         pbarp_d1d1pxcm;
   Float_t         pbarp_d1d1pycm;
   Float_t         pbarp_d1d1pzcm;
   Float_t         pbarp_d1d1ecm;
   Float_t         pbarp_d1d1pcm;
   Float_t         pbarp_d1d1thtcm;
   Float_t         pbarp_d1d1phicm;
   Float_t         pbarp_d1d1covpxpx;
   Float_t         pbarp_d1d1covpxpy;
   Float_t         pbarp_d1d1covpxpz;
   Float_t         pbarp_d1d1covpxe;
   Float_t         pbarp_d1d1covpypy;
   Float_t         pbarp_d1d1covpypz;
   Float_t         pbarp_d1d1covpye;
   Float_t         pbarp_d1d1covpzpz;
   Float_t         pbarp_d1d1covpze;
   Float_t         pbarp_d1d1covee;
   Float_t         pbarp_d1d1pullpx;
   Float_t         pbarp_d1d1pullpy;
   Float_t         pbarp_d1d1pullpz;
   Float_t         pbarp_d1d1pulle;
   Float_t         pbarp_d1d1pullx;
   Float_t         pbarp_d1d1pully;
   Float_t         pbarp_d1d1pullz;
   Float_t         pbarp_d1d1pullpxpy;
   Float_t         pbarp_d1d1pullpxpz;
   Float_t         pbarp_d1d1pullpypz;
   Float_t         pbarp_d1d1pullpxe;
   Float_t         pbarp_d1d1pullpye;
   Float_t         pbarp_d1d1pullpze;
   Float_t         pbarp_d1d1pullxy;
   Float_t         pbarp_d1d1pullxz;
   Float_t         pbarp_d1d1pullyz;
   Float_t         pbarp_d1d1mct;
   Float_t         pbarp_d1d1pide;
   Float_t         pbarp_d1d1pidmu;
   Float_t         pbarp_d1d1pidpi;
   Float_t         pbarp_d1d1pidk;
   Float_t         pbarp_d1d1pidp;
   Float_t         pbarp_d1d1pidmax;
   Float_t         pbarp_d1d1pidbest;
   Float_t         pbarp_d1d1trpdg;
   Float_t         MC_d0px;
   Float_t         MC_d0py;
   Float_t         MC_d0pz;
   Float_t         MC_d0e;
   Float_t         MC_d0p;
   Float_t         MC_d0tht;
   Float_t         MC_d0phi;
   Float_t         MC_d0pt;
   Float_t         MC_d0m;
   Float_t         MC_d0x;
   Float_t         MC_d0y;
   Float_t         MC_d0z;
   Float_t         MC_d0l;
   Float_t         MC_d0chg;
   Float_t         MC_d0pdg;
   Float_t         MC_d0pxcm;
   Float_t         MC_d0pycm;
   Float_t         MC_d0pzcm;
   Float_t         MC_d0ecm;
   Float_t         MC_d0pcm;
   Float_t         MC_d0thtcm;
   Float_t         MC_d0phicm;
   Float_t         MC_d1px;
   Float_t         MC_d1py;
   Float_t         MC_d1pz;
   Float_t         MC_d1e;
   Float_t         MC_d1p;
   Float_t         MC_d1tht;
   Float_t         MC_d1phi;
   Float_t         MC_d1pt;
   Float_t         MC_d1m;
   Float_t         MC_d1x;
   Float_t         MC_d1y;
   Float_t         MC_d1z;
   Float_t         MC_d1l;
   Float_t         MC_d1chg;
   Float_t         MC_d1pdg;
   Float_t         MC_d1pxcm;
   Float_t         MC_d1pycm;
   Float_t         MC_d1pzcm;
   Float_t         MC_d1ecm;
   Float_t         MC_d1pcm;
   Float_t         MC_d1thtcm;
   Float_t         MC_d1phicm;
   Float_t         MC_d0d0px;
   Float_t         MC_d0d0py;
   Float_t         MC_d0d0pz;
   Float_t         MC_d0d0e;
   Float_t         MC_d0d0p;
   Float_t         MC_d0d0tht;
   Float_t         MC_d0d0phi;
   Float_t         MC_d0d0pt;
   Float_t         MC_d0d0m;
   Float_t         MC_d0d0x;
   Float_t         MC_d0d0y;
   Float_t         MC_d0d0z;
   Float_t         MC_d0d0l;
   Float_t         MC_d0d0chg;
   Float_t         MC_d0d0pdg;
   Float_t         MC_d0d0pxcm;
   Float_t         MC_d0d0pycm;
   Float_t         MC_d0d0pzcm;
   Float_t         MC_d0d0ecm;
   Float_t         MC_d0d0pcm;
   Float_t         MC_d0d0thtcm;
   Float_t         MC_d0d0phicm;
   Float_t         MC_d0d1px;
   Float_t         MC_d0d1py;
   Float_t         MC_d0d1pz;
   Float_t         MC_d0d1e;
   Float_t         MC_d0d1p;
   Float_t         MC_d0d1tht;
   Float_t         MC_d0d1phi;
   Float_t         MC_d0d1pt;
   Float_t         MC_d0d1m;
   Float_t         MC_d0d1x;
   Float_t         MC_d0d1y;
   Float_t         MC_d0d1z;
   Float_t         MC_d0d1l;
   Float_t         MC_d0d1chg;
   Float_t         MC_d0d1pdg;
   Float_t         MC_d0d1pxcm;
   Float_t         MC_d0d1pycm;
   Float_t         MC_d0d1pzcm;
   Float_t         MC_d0d1ecm;
   Float_t         MC_d0d1pcm;
   Float_t         MC_d0d1thtcm;
   Float_t         MC_d0d1phicm;
   Float_t         MC_d1d0px;
   Float_t         MC_d1d0py;
   Float_t         MC_d1d0pz;
   Float_t         MC_d1d0e;
   Float_t         MC_d1d0p;
   Float_t         MC_d1d0tht;
   Float_t         MC_d1d0phi;
   Float_t         MC_d1d0pt;
   Float_t         MC_d1d0m;
   Float_t         MC_d1d0x;
   Float_t         MC_d1d0y;
   Float_t         MC_d1d0z;
   Float_t         MC_d1d0l;
   Float_t         MC_d1d0chg;
   Float_t         MC_d1d0pdg;
   Float_t         MC_d1d0pxcm;
   Float_t         MC_d1d0pycm;
   Float_t         MC_d1d0pzcm;
   Float_t         MC_d1d0ecm;
   Float_t         MC_d1d0pcm;
   Float_t         MC_d1d0thtcm;
   Float_t         MC_d1d0phicm;
   Float_t         MC_d1d1px;
   Float_t         MC_d1d1py;
   Float_t         MC_d1d1pz;
   Float_t         MC_d1d1e;
   Float_t         MC_d1d1p;
   Float_t         MC_d1d1tht;
   Float_t         MC_d1d1phi;
   Float_t         MC_d1d1pt;
   Float_t         MC_d1d1m;
   Float_t         MC_d1d1x;
   Float_t         MC_d1d1y;
   Float_t         MC_d1d1z;
   Float_t         MC_d1d1l;
   Float_t         MC_d1d1chg;
   Float_t         MC_d1d1pdg;
   Float_t         MC_d1d1pxcm;
   Float_t         MC_d1d1pycm;
   Float_t         MC_d1d1pzcm;
   Float_t         MC_d1d1ecm;
   Float_t         MC_d1d1pcm;
   Float_t         MC_d1d1thtcm;
   Float_t         MC_d1d1phicm;

   // List of branches
   TBranch        *b_McTruthMatch;   //!
   TBranch        *b_FourMomFit_Convergence;   //!
   TBranch        *b_FourMomFit_chisq;   //!
   TBranch        *b_FourMomFit_ndf;   //!
   TBranch        *b_FourMomFit_prob;   //!
   TBranch        *b_d0d0_ky;   //!
   TBranch        *b_d1d0_ky;   //!
   TBranch        *b_MC_d0d0_ky;   //!
   TBranch        *b_MC_d1d0_ky;   //!
   TBranch        *b_pbarp_px;   //!
   TBranch        *b_pbarp_py;   //!
   TBranch        *b_pbarp_pz;   //!
   TBranch        *b_pbarp_e;   //!
   TBranch        *b_pbarp_p;   //!
   TBranch        *b_pbarp_tht;   //!
   TBranch        *b_pbarp_phi;   //!
   TBranch        *b_pbarp_pt;   //!
   TBranch        *b_pbarp_m;   //!
   TBranch        *b_pbarp_x;   //!
   TBranch        *b_pbarp_y;   //!
   TBranch        *b_pbarp_z;   //!
   TBranch        *b_pbarp_l;   //!
   TBranch        *b_pbarp_chg;   //!
   TBranch        *b_pbarp_pdg;   //!
   TBranch        *b_pbarp_pxcm;   //!
   TBranch        *b_pbarp_pycm;   //!
   TBranch        *b_pbarp_pzcm;   //!
   TBranch        *b_pbarp_ecm;   //!
   TBranch        *b_pbarp_pcm;   //!
   TBranch        *b_pbarp_thtcm;   //!
   TBranch        *b_pbarp_phicm;   //!
   TBranch        *b_pbarp_covpxpx;   //!
   TBranch        *b_pbarp_covpxpy;   //!
   TBranch        *b_pbarp_covpxpz;   //!
   TBranch        *b_pbarp_covpxe;   //!
   TBranch        *b_pbarp_covpypy;   //!
   TBranch        *b_pbarp_covpypz;   //!
   TBranch        *b_pbarp_covpye;   //!
   TBranch        *b_pbarp_covpzpz;   //!
   TBranch        *b_pbarp_covpze;   //!
   TBranch        *b_pbarp_covee;   //!
   TBranch        *b_pbarp_pullpx;   //!
   TBranch        *b_pbarp_pullpy;   //!
   TBranch        *b_pbarp_pullpz;   //!
   TBranch        *b_pbarp_pulle;   //!
   TBranch        *b_pbarp_pullx;   //!
   TBranch        *b_pbarp_pully;   //!
   TBranch        *b_pbarp_pullz;   //!
   TBranch        *b_pbarp_pullpxpy;   //!
   TBranch        *b_pbarp_pullpxpz;   //!
   TBranch        *b_pbarp_pullpypz;   //!
   TBranch        *b_pbarp_pullpxe;   //!
   TBranch        *b_pbarp_pullpye;   //!
   TBranch        *b_pbarp_pullpze;   //!
   TBranch        *b_pbarp_pullxy;   //!
   TBranch        *b_pbarp_pullxz;   //!
   TBranch        *b_pbarp_pullyz;   //!
   TBranch        *b_pbarp_mct;   //!
   TBranch        *b_pbarp_mdif;   //!
   TBranch        *b_pbarp_oang;   //!
   TBranch        *b_pbarp_decang;   //!
   TBranch        *b_pbarp_cdecang;   //!
   TBranch        *b_pbarp_d0px;   //!
   TBranch        *b_pbarp_d0py;   //!
   TBranch        *b_pbarp_d0pz;   //!
   TBranch        *b_pbarp_d0e;   //!
   TBranch        *b_pbarp_d0p;   //!
   TBranch        *b_pbarp_d0tht;   //!
   TBranch        *b_pbarp_d0phi;   //!
   TBranch        *b_pbarp_d0pt;   //!
   TBranch        *b_pbarp_d0m;   //!
   TBranch        *b_pbarp_d0x;   //!
   TBranch        *b_pbarp_d0y;   //!
   TBranch        *b_pbarp_d0z;   //!
   TBranch        *b_pbarp_d0l;   //!
   TBranch        *b_pbarp_d0chg;   //!
   TBranch        *b_pbarp_d0pdg;   //!
   TBranch        *b_pbarp_d0pxcm;   //!
   TBranch        *b_pbarp_d0pycm;   //!
   TBranch        *b_pbarp_d0pzcm;   //!
   TBranch        *b_pbarp_d0ecm;   //!
   TBranch        *b_pbarp_d0pcm;   //!
   TBranch        *b_pbarp_d0thtcm;   //!
   TBranch        *b_pbarp_d0phicm;   //!
   TBranch        *b_pbarp_d0covpxpx;   //!
   TBranch        *b_pbarp_d0covpxpy;   //!
   TBranch        *b_pbarp_d0covpxpz;   //!
   TBranch        *b_pbarp_d0covpxe;   //!
   TBranch        *b_pbarp_d0covpypy;   //!
   TBranch        *b_pbarp_d0covpypz;   //!
   TBranch        *b_pbarp_d0covpye;   //!
   TBranch        *b_pbarp_d0covpzpz;   //!
   TBranch        *b_pbarp_d0covpze;   //!
   TBranch        *b_pbarp_d0covee;   //!
   TBranch        *b_pbarp_d0pullpx;   //!
   TBranch        *b_pbarp_d0pullpy;   //!
   TBranch        *b_pbarp_d0pullpz;   //!
   TBranch        *b_pbarp_d0pulle;   //!
   TBranch        *b_pbarp_d0pullx;   //!
   TBranch        *b_pbarp_d0pully;   //!
   TBranch        *b_pbarp_d0pullz;   //!
   TBranch        *b_pbarp_d0pullpxpy;   //!
   TBranch        *b_pbarp_d0pullpxpz;   //!
   TBranch        *b_pbarp_d0pullpypz;   //!
   TBranch        *b_pbarp_d0pullpxe;   //!
   TBranch        *b_pbarp_d0pullpye;   //!
   TBranch        *b_pbarp_d0pullpze;   //!
   TBranch        *b_pbarp_d0pullxy;   //!
   TBranch        *b_pbarp_d0pullxz;   //!
   TBranch        *b_pbarp_d0pullyz;   //!
   TBranch        *b_pbarp_d0mct;   //!
   TBranch        *b_pbarp_d0mdif;   //!
   TBranch        *b_pbarp_d0oang;   //!
   TBranch        *b_pbarp_d0decang;   //!
   TBranch        *b_pbarp_d0cdecang;   //!
   TBranch        *b_pbarp_d0d0px;   //!
   TBranch        *b_pbarp_d0d0py;   //!
   TBranch        *b_pbarp_d0d0pz;   //!
   TBranch        *b_pbarp_d0d0e;   //!
   TBranch        *b_pbarp_d0d0p;   //!
   TBranch        *b_pbarp_d0d0tht;   //!
   TBranch        *b_pbarp_d0d0phi;   //!
   TBranch        *b_pbarp_d0d0pt;   //!
   TBranch        *b_pbarp_d0d0m;   //!
   TBranch        *b_pbarp_d0d0x;   //!
   TBranch        *b_pbarp_d0d0y;   //!
   TBranch        *b_pbarp_d0d0z;   //!
   TBranch        *b_pbarp_d0d0l;   //!
   TBranch        *b_pbarp_d0d0chg;   //!
   TBranch        *b_pbarp_d0d0pdg;   //!
   TBranch        *b_pbarp_d0d0pxcm;   //!
   TBranch        *b_pbarp_d0d0pycm;   //!
   TBranch        *b_pbarp_d0d0pzcm;   //!
   TBranch        *b_pbarp_d0d0ecm;   //!
   TBranch        *b_pbarp_d0d0pcm;   //!
   TBranch        *b_pbarp_d0d0thtcm;   //!
   TBranch        *b_pbarp_d0d0phicm;   //!
   TBranch        *b_pbarp_d0d0covpxpx;   //!
   TBranch        *b_pbarp_d0d0covpxpy;   //!
   TBranch        *b_pbarp_d0d0covpxpz;   //!
   TBranch        *b_pbarp_d0d0covpxe;   //!
   TBranch        *b_pbarp_d0d0covpypy;   //!
   TBranch        *b_pbarp_d0d0covpypz;   //!
   TBranch        *b_pbarp_d0d0covpye;   //!
   TBranch        *b_pbarp_d0d0covpzpz;   //!
   TBranch        *b_pbarp_d0d0covpze;   //!
   TBranch        *b_pbarp_d0d0covee;   //!
   TBranch        *b_pbarp_d0d0pullpx;   //!
   TBranch        *b_pbarp_d0d0pullpy;   //!
   TBranch        *b_pbarp_d0d0pullpz;   //!
   TBranch        *b_pbarp_d0d0pulle;   //!
   TBranch        *b_pbarp_d0d0pullx;   //!
   TBranch        *b_pbarp_d0d0pully;   //!
   TBranch        *b_pbarp_d0d0pullz;   //!
   TBranch        *b_pbarp_d0d0pullpxpy;   //!
   TBranch        *b_pbarp_d0d0pullpxpz;   //!
   TBranch        *b_pbarp_d0d0pullpypz;   //!
   TBranch        *b_pbarp_d0d0pullpxe;   //!
   TBranch        *b_pbarp_d0d0pullpye;   //!
   TBranch        *b_pbarp_d0d0pullpze;   //!
   TBranch        *b_pbarp_d0d0pullxy;   //!
   TBranch        *b_pbarp_d0d0pullxz;   //!
   TBranch        *b_pbarp_d0d0pullyz;   //!
   TBranch        *b_pbarp_d0d0mct;   //!
   TBranch        *b_pbarp_d0d0pide;   //!
   TBranch        *b_pbarp_d0d0pidmu;   //!
   TBranch        *b_pbarp_d0d0pidpi;   //!
   TBranch        *b_pbarp_d0d0pidk;   //!
   TBranch        *b_pbarp_d0d0pidp;   //!
   TBranch        *b_pbarp_d0d0pidmax;   //!
   TBranch        *b_pbarp_d0d0pidbest;   //!
   TBranch        *b_pbarp_d0d0trpdg;   //!
   TBranch        *b_pbarp_d0d1px;   //!
   TBranch        *b_pbarp_d0d1py;   //!
   TBranch        *b_pbarp_d0d1pz;   //!
   TBranch        *b_pbarp_d0d1e;   //!
   TBranch        *b_pbarp_d0d1p;   //!
   TBranch        *b_pbarp_d0d1tht;   //!
   TBranch        *b_pbarp_d0d1phi;   //!
   TBranch        *b_pbarp_d0d1pt;   //!
   TBranch        *b_pbarp_d0d1m;   //!
   TBranch        *b_pbarp_d0d1x;   //!
   TBranch        *b_pbarp_d0d1y;   //!
   TBranch        *b_pbarp_d0d1z;   //!
   TBranch        *b_pbarp_d0d1l;   //!
   TBranch        *b_pbarp_d0d1chg;   //!
   TBranch        *b_pbarp_d0d1pdg;   //!
   TBranch        *b_pbarp_d0d1pxcm;   //!
   TBranch        *b_pbarp_d0d1pycm;   //!
   TBranch        *b_pbarp_d0d1pzcm;   //!
   TBranch        *b_pbarp_d0d1ecm;   //!
   TBranch        *b_pbarp_d0d1pcm;   //!
   TBranch        *b_pbarp_d0d1thtcm;   //!
   TBranch        *b_pbarp_d0d1phicm;   //!
   TBranch        *b_pbarp_d0d1covpxpx;   //!
   TBranch        *b_pbarp_d0d1covpxpy;   //!
   TBranch        *b_pbarp_d0d1covpxpz;   //!
   TBranch        *b_pbarp_d0d1covpxe;   //!
   TBranch        *b_pbarp_d0d1covpypy;   //!
   TBranch        *b_pbarp_d0d1covpypz;   //!
   TBranch        *b_pbarp_d0d1covpye;   //!
   TBranch        *b_pbarp_d0d1covpzpz;   //!
   TBranch        *b_pbarp_d0d1covpze;   //!
   TBranch        *b_pbarp_d0d1covee;   //!
   TBranch        *b_pbarp_d0d1pullpx;   //!
   TBranch        *b_pbarp_d0d1pullpy;   //!
   TBranch        *b_pbarp_d0d1pullpz;   //!
   TBranch        *b_pbarp_d0d1pulle;   //!
   TBranch        *b_pbarp_d0d1pullx;   //!
   TBranch        *b_pbarp_d0d1pully;   //!
   TBranch        *b_pbarp_d0d1pullz;   //!
   TBranch        *b_pbarp_d0d1pullpxpy;   //!
   TBranch        *b_pbarp_d0d1pullpxpz;   //!
   TBranch        *b_pbarp_d0d1pullpypz;   //!
   TBranch        *b_pbarp_d0d1pullpxe;   //!
   TBranch        *b_pbarp_d0d1pullpye;   //!
   TBranch        *b_pbarp_d0d1pullpze;   //!
   TBranch        *b_pbarp_d0d1pullxy;   //!
   TBranch        *b_pbarp_d0d1pullxz;   //!
   TBranch        *b_pbarp_d0d1pullyz;   //!
   TBranch        *b_pbarp_d0d1mct;   //!
   TBranch        *b_pbarp_d0d1pide;   //!
   TBranch        *b_pbarp_d0d1pidmu;   //!
   TBranch        *b_pbarp_d0d1pidpi;   //!
   TBranch        *b_pbarp_d0d1pidk;   //!
   TBranch        *b_pbarp_d0d1pidp;   //!
   TBranch        *b_pbarp_d0d1pidmax;   //!
   TBranch        *b_pbarp_d0d1pidbest;   //!
   TBranch        *b_pbarp_d0d1trpdg;   //!
   TBranch        *b_pbarp_d1px;   //!
   TBranch        *b_pbarp_d1py;   //!
   TBranch        *b_pbarp_d1pz;   //!
   TBranch        *b_pbarp_d1e;   //!
   TBranch        *b_pbarp_d1p;   //!
   TBranch        *b_pbarp_d1tht;   //!
   TBranch        *b_pbarp_d1phi;   //!
   TBranch        *b_pbarp_d1pt;   //!
   TBranch        *b_pbarp_d1m;   //!
   TBranch        *b_pbarp_d1x;   //!
   TBranch        *b_pbarp_d1y;   //!
   TBranch        *b_pbarp_d1z;   //!
   TBranch        *b_pbarp_d1l;   //!
   TBranch        *b_pbarp_d1chg;   //!
   TBranch        *b_pbarp_d1pdg;   //!
   TBranch        *b_pbarp_d1pxcm;   //!
   TBranch        *b_pbarp_d1pycm;   //!
   TBranch        *b_pbarp_d1pzcm;   //!
   TBranch        *b_pbarp_d1ecm;   //!
   TBranch        *b_pbarp_d1pcm;   //!
   TBranch        *b_pbarp_d1thtcm;   //!
   TBranch        *b_pbarp_d1phicm;   //!
   TBranch        *b_pbarp_d1covpxpx;   //!
   TBranch        *b_pbarp_d1covpxpy;   //!
   TBranch        *b_pbarp_d1covpxpz;   //!
   TBranch        *b_pbarp_d1covpxe;   //!
   TBranch        *b_pbarp_d1covpypy;   //!
   TBranch        *b_pbarp_d1covpypz;   //!
   TBranch        *b_pbarp_d1covpye;   //!
   TBranch        *b_pbarp_d1covpzpz;   //!
   TBranch        *b_pbarp_d1covpze;   //!
   TBranch        *b_pbarp_d1covee;   //!
   TBranch        *b_pbarp_d1pullpx;   //!
   TBranch        *b_pbarp_d1pullpy;   //!
   TBranch        *b_pbarp_d1pullpz;   //!
   TBranch        *b_pbarp_d1pulle;   //!
   TBranch        *b_pbarp_d1pullx;   //!
   TBranch        *b_pbarp_d1pully;   //!
   TBranch        *b_pbarp_d1pullz;   //!
   TBranch        *b_pbarp_d1pullpxpy;   //!
   TBranch        *b_pbarp_d1pullpxpz;   //!
   TBranch        *b_pbarp_d1pullpypz;   //!
   TBranch        *b_pbarp_d1pullpxe;   //!
   TBranch        *b_pbarp_d1pullpye;   //!
   TBranch        *b_pbarp_d1pullpze;   //!
   TBranch        *b_pbarp_d1pullxy;   //!
   TBranch        *b_pbarp_d1pullxz;   //!
   TBranch        *b_pbarp_d1pullyz;   //!
   TBranch        *b_pbarp_d1mct;   //!
   TBranch        *b_pbarp_d1mdif;   //!
   TBranch        *b_pbarp_d1oang;   //!
   TBranch        *b_pbarp_d1decang;   //!
   TBranch        *b_pbarp_d1cdecang;   //!
   TBranch        *b_pbarp_d1d0px;   //!
   TBranch        *b_pbarp_d1d0py;   //!
   TBranch        *b_pbarp_d1d0pz;   //!
   TBranch        *b_pbarp_d1d0e;   //!
   TBranch        *b_pbarp_d1d0p;   //!
   TBranch        *b_pbarp_d1d0tht;   //!
   TBranch        *b_pbarp_d1d0phi;   //!
   TBranch        *b_pbarp_d1d0pt;   //!
   TBranch        *b_pbarp_d1d0m;   //!
   TBranch        *b_pbarp_d1d0x;   //!
   TBranch        *b_pbarp_d1d0y;   //!
   TBranch        *b_pbarp_d1d0z;   //!
   TBranch        *b_pbarp_d1d0l;   //!
   TBranch        *b_pbarp_d1d0chg;   //!
   TBranch        *b_pbarp_d1d0pdg;   //!
   TBranch        *b_pbarp_d1d0pxcm;   //!
   TBranch        *b_pbarp_d1d0pycm;   //!
   TBranch        *b_pbarp_d1d0pzcm;   //!
   TBranch        *b_pbarp_d1d0ecm;   //!
   TBranch        *b_pbarp_d1d0pcm;   //!
   TBranch        *b_pbarp_d1d0thtcm;   //!
   TBranch        *b_pbarp_d1d0phicm;   //!
   TBranch        *b_pbarp_d1d0covpxpx;   //!
   TBranch        *b_pbarp_d1d0covpxpy;   //!
   TBranch        *b_pbarp_d1d0covpxpz;   //!
   TBranch        *b_pbarp_d1d0covpxe;   //!
   TBranch        *b_pbarp_d1d0covpypy;   //!
   TBranch        *b_pbarp_d1d0covpypz;   //!
   TBranch        *b_pbarp_d1d0covpye;   //!
   TBranch        *b_pbarp_d1d0covpzpz;   //!
   TBranch        *b_pbarp_d1d0covpze;   //!
   TBranch        *b_pbarp_d1d0covee;   //!
   TBranch        *b_pbarp_d1d0pullpx;   //!
   TBranch        *b_pbarp_d1d0pullpy;   //!
   TBranch        *b_pbarp_d1d0pullpz;   //!
   TBranch        *b_pbarp_d1d0pulle;   //!
   TBranch        *b_pbarp_d1d0pullx;   //!
   TBranch        *b_pbarp_d1d0pully;   //!
   TBranch        *b_pbarp_d1d0pullz;   //!
   TBranch        *b_pbarp_d1d0pullpxpy;   //!
   TBranch        *b_pbarp_d1d0pullpxpz;   //!
   TBranch        *b_pbarp_d1d0pullpypz;   //!
   TBranch        *b_pbarp_d1d0pullpxe;   //!
   TBranch        *b_pbarp_d1d0pullpye;   //!
   TBranch        *b_pbarp_d1d0pullpze;   //!
   TBranch        *b_pbarp_d1d0pullxy;   //!
   TBranch        *b_pbarp_d1d0pullxz;   //!
   TBranch        *b_pbarp_d1d0pullyz;   //!
   TBranch        *b_pbarp_d1d0mct;   //!
   TBranch        *b_pbarp_d1d0pide;   //!
   TBranch        *b_pbarp_d1d0pidmu;   //!
   TBranch        *b_pbarp_d1d0pidpi;   //!
   TBranch        *b_pbarp_d1d0pidk;   //!
   TBranch        *b_pbarp_d1d0pidp;   //!
   TBranch        *b_pbarp_d1d0pidmax;   //!
   TBranch        *b_pbarp_d1d0pidbest;   //!
   TBranch        *b_pbarp_d1d0trpdg;   //!
   TBranch        *b_pbarp_d1d1px;   //!
   TBranch        *b_pbarp_d1d1py;   //!
   TBranch        *b_pbarp_d1d1pz;   //!
   TBranch        *b_pbarp_d1d1e;   //!
   TBranch        *b_pbarp_d1d1p;   //!
   TBranch        *b_pbarp_d1d1tht;   //!
   TBranch        *b_pbarp_d1d1phi;   //!
   TBranch        *b_pbarp_d1d1pt;   //!
   TBranch        *b_pbarp_d1d1m;   //!
   TBranch        *b_pbarp_d1d1x;   //!
   TBranch        *b_pbarp_d1d1y;   //!
   TBranch        *b_pbarp_d1d1z;   //!
   TBranch        *b_pbarp_d1d1l;   //!
   TBranch        *b_pbarp_d1d1chg;   //!
   TBranch        *b_pbarp_d1d1pdg;   //!
   TBranch        *b_pbarp_d1d1pxcm;   //!
   TBranch        *b_pbarp_d1d1pycm;   //!
   TBranch        *b_pbarp_d1d1pzcm;   //!
   TBranch        *b_pbarp_d1d1ecm;   //!
   TBranch        *b_pbarp_d1d1pcm;   //!
   TBranch        *b_pbarp_d1d1thtcm;   //!
   TBranch        *b_pbarp_d1d1phicm;   //!
   TBranch        *b_pbarp_d1d1covpxpx;   //!
   TBranch        *b_pbarp_d1d1covpxpy;   //!
   TBranch        *b_pbarp_d1d1covpxpz;   //!
   TBranch        *b_pbarp_d1d1covpxe;   //!
   TBranch        *b_pbarp_d1d1covpypy;   //!
   TBranch        *b_pbarp_d1d1covpypz;   //!
   TBranch        *b_pbarp_d1d1covpye;   //!
   TBranch        *b_pbarp_d1d1covpzpz;   //!
   TBranch        *b_pbarp_d1d1covpze;   //!
   TBranch        *b_pbarp_d1d1covee;   //!
   TBranch        *b_pbarp_d1d1pullpx;   //!
   TBranch        *b_pbarp_d1d1pullpy;   //!
   TBranch        *b_pbarp_d1d1pullpz;   //!
   TBranch        *b_pbarp_d1d1pulle;   //!
   TBranch        *b_pbarp_d1d1pullx;   //!
   TBranch        *b_pbarp_d1d1pully;   //!
   TBranch        *b_pbarp_d1d1pullz;   //!
   TBranch        *b_pbarp_d1d1pullpxpy;   //!
   TBranch        *b_pbarp_d1d1pullpxpz;   //!
   TBranch        *b_pbarp_d1d1pullpypz;   //!
   TBranch        *b_pbarp_d1d1pullpxe;   //!
   TBranch        *b_pbarp_d1d1pullpye;   //!
   TBranch        *b_pbarp_d1d1pullpze;   //!
   TBranch        *b_pbarp_d1d1pullxy;   //!
   TBranch        *b_pbarp_d1d1pullxz;   //!
   TBranch        *b_pbarp_d1d1pullyz;   //!
   TBranch        *b_pbarp_d1d1mct;   //!
   TBranch        *b_pbarp_d1d1pide;   //!
   TBranch        *b_pbarp_d1d1pidmu;   //!
   TBranch        *b_pbarp_d1d1pidpi;   //!
   TBranch        *b_pbarp_d1d1pidk;   //!
   TBranch        *b_pbarp_d1d1pidp;   //!
   TBranch        *b_pbarp_d1d1pidmax;   //!
   TBranch        *b_pbarp_d1d1pidbest;   //!
   TBranch        *b_pbarp_d1d1trpdg;   //!
   TBranch        *b_MC_d0px;   //!
   TBranch        *b_MC_d0py;   //!
   TBranch        *b_MC_d0pz;   //!
   TBranch        *b_MC_d0e;   //!
   TBranch        *b_MC_d0p;   //!
   TBranch        *b_MC_d0tht;   //!
   TBranch        *b_MC_d0phi;   //!
   TBranch        *b_MC_d0pt;   //!
   TBranch        *b_MC_d0m;   //!
   TBranch        *b_MC_d0x;   //!
   TBranch        *b_MC_d0y;   //!
   TBranch        *b_MC_d0z;   //!
   TBranch        *b_MC_d0l;   //!
   TBranch        *b_MC_d0chg;   //!
   TBranch        *b_MC_d0pdg;   //!
   TBranch        *b_MC_d0pxcm;   //!
   TBranch        *b_MC_d0pycm;   //!
   TBranch        *b_MC_d0pzcm;   //!
   TBranch        *b_MC_d0ecm;   //!
   TBranch        *b_MC_d0pcm;   //!
   TBranch        *b_MC_d0thtcm;   //!
   TBranch        *b_MC_d0phicm;   //!
   TBranch        *b_MC_d1px;   //!
   TBranch        *b_MC_d1py;   //!
   TBranch        *b_MC_d1pz;   //!
   TBranch        *b_MC_d1e;   //!
   TBranch        *b_MC_d1p;   //!
   TBranch        *b_MC_d1tht;   //!
   TBranch        *b_MC_d1phi;   //!
   TBranch        *b_MC_d1pt;   //!
   TBranch        *b_MC_d1m;   //!
   TBranch        *b_MC_d1x;   //!
   TBranch        *b_MC_d1y;   //!
   TBranch        *b_MC_d1z;   //!
   TBranch        *b_MC_d1l;   //!
   TBranch        *b_MC_d1chg;   //!
   TBranch        *b_MC_d1pdg;   //!
   TBranch        *b_MC_d1pxcm;   //!
   TBranch        *b_MC_d1pycm;   //!
   TBranch        *b_MC_d1pzcm;   //!
   TBranch        *b_MC_d1ecm;   //!
   TBranch        *b_MC_d1pcm;   //!
   TBranch        *b_MC_d1thtcm;   //!
   TBranch        *b_MC_d1phicm;   //!
   TBranch        *b_MC_d0d0px;   //!
   TBranch        *b_MC_d0d0py;   //!
   TBranch        *b_MC_d0d0pz;   //!
   TBranch        *b_MC_d0d0e;   //!
   TBranch        *b_MC_d0d0p;   //!
   TBranch        *b_MC_d0d0tht;   //!
   TBranch        *b_MC_d0d0phi;   //!
   TBranch        *b_MC_d0d0pt;   //!
   TBranch        *b_MC_d0d0m;   //!
   TBranch        *b_MC_d0d0x;   //!
   TBranch        *b_MC_d0d0y;   //!
   TBranch        *b_MC_d0d0z;   //!
   TBranch        *b_MC_d0d0l;   //!
   TBranch        *b_MC_d0d0chg;   //!
   TBranch        *b_MC_d0d0pdg;   //!
   TBranch        *b_MC_d0d0pxcm;   //!
   TBranch        *b_MC_d0d0pycm;   //!
   TBranch        *b_MC_d0d0pzcm;   //!
   TBranch        *b_MC_d0d0ecm;   //!
   TBranch        *b_MC_d0d0pcm;   //!
   TBranch        *b_MC_d0d0thtcm;   //!
   TBranch        *b_MC_d0d0phicm;   //!
   TBranch        *b_MC_d0d1px;   //!
   TBranch        *b_MC_d0d1py;   //!
   TBranch        *b_MC_d0d1pz;   //!
   TBranch        *b_MC_d0d1e;   //!
   TBranch        *b_MC_d0d1p;   //!
   TBranch        *b_MC_d0d1tht;   //!
   TBranch        *b_MC_d0d1phi;   //!
   TBranch        *b_MC_d0d1pt;   //!
   TBranch        *b_MC_d0d1m;   //!
   TBranch        *b_MC_d0d1x;   //!
   TBranch        *b_MC_d0d1y;   //!
   TBranch        *b_MC_d0d1z;   //!
   TBranch        *b_MC_d0d1l;   //!
   TBranch        *b_MC_d0d1chg;   //!
   TBranch        *b_MC_d0d1pdg;   //!
   TBranch        *b_MC_d0d1pxcm;   //!
   TBranch        *b_MC_d0d1pycm;   //!
   TBranch        *b_MC_d0d1pzcm;   //!
   TBranch        *b_MC_d0d1ecm;   //!
   TBranch        *b_MC_d0d1pcm;   //!
   TBranch        *b_MC_d0d1thtcm;   //!
   TBranch        *b_MC_d0d1phicm;   //!
   TBranch        *b_MC_d1d0px;   //!
   TBranch        *b_MC_d1d0py;   //!
   TBranch        *b_MC_d1d0pz;   //!
   TBranch        *b_MC_d1d0e;   //!
   TBranch        *b_MC_d1d0p;   //!
   TBranch        *b_MC_d1d0tht;   //!
   TBranch        *b_MC_d1d0phi;   //!
   TBranch        *b_MC_d1d0pt;   //!
   TBranch        *b_MC_d1d0m;   //!
   TBranch        *b_MC_d1d0x;   //!
   TBranch        *b_MC_d1d0y;   //!
   TBranch        *b_MC_d1d0z;   //!
   TBranch        *b_MC_d1d0l;   //!
   TBranch        *b_MC_d1d0chg;   //!
   TBranch        *b_MC_d1d0pdg;   //!
   TBranch        *b_MC_d1d0pxcm;   //!
   TBranch        *b_MC_d1d0pycm;   //!
   TBranch        *b_MC_d1d0pzcm;   //!
   TBranch        *b_MC_d1d0ecm;   //!
   TBranch        *b_MC_d1d0pcm;   //!
   TBranch        *b_MC_d1d0thtcm;   //!
   TBranch        *b_MC_d1d0phicm;   //!
   TBranch        *b_MC_d1d1px;   //!
   TBranch        *b_MC_d1d1py;   //!
   TBranch        *b_MC_d1d1pz;   //!
   TBranch        *b_MC_d1d1e;   //!
   TBranch        *b_MC_d1d1p;   //!
   TBranch        *b_MC_d1d1tht;   //!
   TBranch        *b_MC_d1d1phi;   //!
   TBranch        *b_MC_d1d1pt;   //!
   TBranch        *b_MC_d1d1m;   //!
   TBranch        *b_MC_d1d1x;   //!
   TBranch        *b_MC_d1d1y;   //!
   TBranch        *b_MC_d1d1z;   //!
   TBranch        *b_MC_d1d1l;   //!
   TBranch        *b_MC_d1d1chg;   //!
   TBranch        *b_MC_d1d1pdg;   //!
   TBranch        *b_MC_d1d1pxcm;   //!
   TBranch        *b_MC_d1d1pycm;   //!
   TBranch        *b_MC_d1d1pzcm;   //!
   TBranch        *b_MC_d1d1ecm;   //!
   TBranch        *b_MC_d1d1pcm;   //!
   TBranch        *b_MC_d1d1thtcm;   //!
   TBranch        *b_MC_d1d1phicm;   //!

   ntpBestPbarP(TTree *tree=0);
   virtual ~ntpBestPbarP();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   Double_t fMass0_lam = TDatabasePDG::Instance()->GetParticle(-3122)->Mass();
   
   // *** 
   // ----- TCanvas
   // ***
   
   TCanvas *c1;
   TCanvas *c2;
    
   //*** 
   //----- Histograms: Kinematics
   //***
   
   //Momentum v.s. Theta Dist.
   TH2F *hpbar_MomTht;
   TH2F *hpiplus_MomTht;
   TH2F *hp_MomTht;
   TH2F *hpiminus_MomTht;
   
   TH2F *hpbar_McMomTht;
   TH2F *hpiplus_McMomTht;
   TH2F *hp_McMomTht;
   TH2F *hpiminus_McMomTht;
   
   // *** 
   // ----- Histograms: Pre Selection
   // ***
      
   //Mass, Chi2, Prob, ndf
   TH1D *hlam0_m;
   TH1D *hlam0bar_m;
   TH1D *hpbarp_m;
   TH1D *hpbarp_chi2;
   TH1D *hpbarp_prob;
   TH1D *hpbarp_ndf;
   
   //Momentum
   TH2D *hlam0_p4; 
   TH2D *hlam0bar_p4;

   //2D Scatter Plots
   TH2D *h_m_llbar;
   
   //Displaced Vertex
   TH1D *h_dvzsum_llbar;
   
   // *** 
   // ----- Histograms: Final Selection
   // ***
   
   //Mass, Chi2, Prob, ndf
   TH1D *hlam0_m_cut1;
   TH1D *hlam0_m_cut2;
   TH1D *hlam0_m_cut3;
   TH1D *hlam0bar_m_cut1;
   TH1D *hlam0bar_m_cut2;
   TH1D *hlam0bar_m_cut3;
   TH1D *hpbarp_m_cut1;
   TH1D *hpbarp_m_cut2;
   TH1D *hpbarp_m_cut3;
   
   //2D Scatter Plots
   TH2D *h_m_llbar_cut3;
   
   //Displaced Vertex
   TH1D *h_dvzsum_llbar_cut3;
   
   //Theta, Cosine of Theta etc.
   TH1D *hlam0_thtcm;
   TH1D *hlam0bar_thtcm;

  
   // *** 
   // ----- Histograms: Single Canvas
   // ***
   
   /*
   //pbarp, lam0, pi-, p
   TH1D *hlam0_m;
   TH1D *hlam0_vtx_pullx;
   TH1D *hlam0_vtx_pully;
   TH1D *hlam0_vtx_pullz;
   TH1D *hpiminus_tht;
   TH1D *hp_tht;
   
   TH1D *hpbarp1_chi2;
   TH1D *hpbarp1_prob;
   
   
   //pbarp, lam0bar, pi+, pbar
   TH1D *hlam0bar_m;
   TH1D *hlam0bar_vtx_pullx;
   TH1D *hlam0bar_vtx_pully;
   TH1D *hlam0bar_vtx_pullz;
   TH1D *hpiplus_tht;
   TH1D *hpbar_tht;
   
   TH1D *hpbarp2_chi2;
   TH1D *hpbarp2_prob;
   */
   
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
   
};

#endif

#ifdef ntpBestPbarP_cxx
ntpBestPbarP::ntpBestPbarP(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
    
    
    //*** ---------------------
    //*** Added by ADEEL (START)
    //***
    
    TString path = "";
    TFile* hfile = TFile::Open(path+"ntpBestPbarP.root", "RECREATE");
    
    //*** 
    //----- Histograms: Kinematics (Shift from presel to finalsel)
    //***
    
    //pz v.s. pt
	hlam0_p4 = new TH2D("hlam0_p4","Four Momentum Distribution (#Lambda) @1.642 GeV;p_{z} /GeV/c;p_{t} /GeV/c",1000,-0.1,1.5,1000,0,0.5);
    hlam0bar_p4 = new TH2D("hlam0bar_p4","Four Momentum Distribution (#bar{#Lambda}) @1.642 GeV;p_{z} /GeV/c;p_{t} /GeV/c",1000,-0.1,1.5,1000,0,0.5);
    
    //momentum v.s. theta
    hpbar_MomTht = new TH2F("hpbar_MomTht","p/#theta dist. (anti-proton);|p| /GeV/c;#theta [Deg.]",500,0,1.65,500,0,180);
    hpiplus_MomTht = new TH2F("hpiplus_MomTht","p/#theta dist. (#pi^{+});|p| /GeV/c;#theta [Deg.]",500,0,1.65,500,0,180);
    hp_MomTht = new TH2F("hp_MomTht","p/#theta dist. (proton);|p| /GeV/c;#theta [Deg.]",500,0,1.65,500,0,180);
    hpiminus_MomTht = new TH2F("hpiminus_MomTht","p/#theta dist. (#pi^{-});|p| /GeV/c;#theta [Deg.]",500,0,1.65,500,0,180);
    
    hpbar_McMomTht = new TH2F("hpbar_McMomTht","p/#theta dist. (anti-proton);|p| /GeV/c;#theta [Deg.]",500,0,1.65,500,0,180);
    hpiplus_McMomTht = new TH2F("hpiplus_McMomTht","p/#theta dist. (#pi^{+});|p| /GeV/c;#theta [Deg.]",500,0,1.65,500,0,180);
    hp_McMomTht = new TH2F("hp_McMomTht","p/#theta dist. (proton);|p| /GeV/c;#theta [Deg.]",500,0,1.65,500,0,180);
    hpiminus_McMomTht = new TH2F("hpiminus_McMomTht","p/#theta dist. (#pi^{-});|p| /GeV/c;#theta [Deg.]",500,0,1.65,500,0,180);
    
    //*** 
    //----- Histograms: Pre-selection
    //***
    
    //Mass, Chi2, Prob, ndf
    hlam0_m = new TH1D("hlam0_m","Invariant Mass;m(#Lambda) /GeV/c^{2};Entries /GeV/c^{2}",500,1.0,1.5);
    hlam0bar_m = new TH1D("hlam0bar_m","Invariant Mass;m(#bar{#Lambda}) /GeV/c^{2};Entries /GeV/c^{2}",500,1.0,1.5);
    hpbarp_m = new TH1D("hpbarp_m","Invariant Mass;m(#bar{p}p) /GeV/c^{2};Entries /GeV/c^{2}",500,2.0,3.0);
	hpbarp_chi2 = new TH1D("hpbarp_chi2","Chi-square (4c);#chi^{2};Entries",500,0,300);
	hpbarp_prob = new TH1D("hpbarp_prob","Probability (4c);probability;Entries",500,0,1);
	hpbarp_ndf = new TH1D("hpbarp_ndf","NDF (4c);ndf;Entries",100,0,10);
        
    //Sum of z-coordinate of decay vertex
    h_dvzsum_llbar = new TH1D("h_dvzsum_llbar","Displaced Decay Vertex;z_{#Lambda} + z_{#bar{#Lambda}} /cm;Entries /cm",200,0,100);
    
    //2D Scatter Plots
    h_m_llbar = new TH2D("h_m_llbar", "Invariant Mass;m(p#pi^{-}) /GeV/c^{2};m(#bar{p}#pi^{+}) /GeV/c^{2}",1000,0.8,1.8,1000,0.8,1.8); //x: lam0, y: lam0bar


    //*** 
    //----- Histograms: Final Selection
    //***
    
    //lam0
    hlam0_m_cut1 = new TH1D("hlam0_m_cut1","m(#Lambda) after #chi^{2} < 100;m(#Lambda) /GeV/c^{2};Entries /GeV/c^{2}",500,1.0,1.5);
    hlam0_m_cut2 = new TH1D("hlam0_m_cut2","m(#Lambda) after mass cut;m(#Lambda) /GeV/c^{2};Entries /GeV/c^{2}",500,1.0,1.5);
    hlam0_m_cut3 = new TH1D("hlam0_m_cut3","m(#Lambda) after vertex cut;m(#Lambda) /GeV/c^{2};Entries /GeV/c^{2}",500,1.0,1.5);
    
    //lam0bar
    hlam0bar_m_cut1 = new TH1D("hlam0bar_m_cut1","m(#bar{#Lambda}) after #chi^{2} < 100;m(#bar{#Lambda}) /GeV/c^{2};Entries /GeV/c^{2}",500,1.0,1.5);
    hlam0bar_m_cut2 = new TH1D("hlam0bar_m_cut2","m(#bar{#Lambda}) after mass cut;m(#bar{#Lambda}) /GeV/c^{2};Entries /GeV/c^{2}",500,1.0,1.5);
    hlam0bar_m_cut3 = new TH1D("hlam0bar_m_cut3","m(#bar{#Lambda}) after vertex cut;m(#bar{#Lambda}) /GeV/c^{2};Entries /GeV/c^{2}",500,1.0,1.5);
    
    //pbarp
    hpbarp_m_cut1 = new TH1D("hpbarp_m_cut1","m(#bar{p}p) after #chi^{2} < 100;m(#bar{p}p) /GeV/c^{2};Entries /GeV/c^{2}",500,2.0,3.0);
    hpbarp_m_cut2 = new TH1D("hpbarp_m_cut2","m(#bar{p}p) after mass cut;m(#bar{p}p) /GeV/c^{2};Entries /GeV/c^{2}",500,2.0,3.0);
    hpbarp_m_cut3 = new TH1D("hpbarp_m_cut3","m(#bar{p}p) after vertex cut;m(#bar{p}p) /GeV/c^{2};Entries /GeV/c^{2}",500,2.0,3.0);
    
    //Theta, Cosine of Theta etc.
    hlam0_thtcm = new TH1D("hlam0_thtcm","Cos #theta_{#Lambda};Cos #theta_{#Lambda};Entries",200,-1.,1.);
    hlam0bar_thtcm = new TH1D("hlam0bar_thtcm","Cos #theta_{#bar{#Lambda}};Cos #theta_{#bar{#Lambda}};Entries",200,-1.,1.);
    
    //Sum of z-coordinate of decay vertex
    //h_dvzsum_llbar_cut3 = new TH1D("h_dvzsum_llbar_cut3","Displaced Decay Vertex;z_{#Lambda} + z_{#bar{#Lambda}} /cm;Entries /cm",200,0,100);
    
    //2D Scatter Plots
    //h_m_llbar_cut3 = new TH2D("h_m_llbar_cut3", "Invariant Mass;m(p#pi^{-}) /GeV/c^{2};m(#bar{p}#pi^{+}) /GeV/c^{2}",1000,0.8,1.8,1000,0.8,1.8); //x: lam0, y: lam0bar
    
    
    
    // *** 
    // ----- Histograms: Single Canvas
    // ***
    
    //pbarp, lam0, pi-, p
    
    /*
    hpbarp1_chi2 = new TH1D("hpbarp1_chi2","#bar{p}p: #chi^{2} (4C Fit)",100,0,500);
    hpbarp1_prob = new TH1D("hpbarp1_prob","#bar{p}p: Prob (4C Fit)",100,0,1);
    hlam0_m = new TH1D("hlam0_m","#Lambda_{0} - Mass",200,1.0,1.2);
    hlam0_vtx_pullx = new TH1D("hlam0_vtx_pullx","#Lambda_{0} - pull x",100,-100.,100.);
    hlam0_vtx_pully = new TH1D("hlam0_vtx_pully","#Lambda_{0} - pull y",100,-100.,100.);
    hlam0_vtx_pullz = new TH1D("hlam0_vtx_pullz","#Lambda_{0} - pull z",100,-100.,100.);
    hp_tht = new TH1D("hp_tht","#theta (p)",100,0.,180.);
    hpiminus_tht = new TH1D("hpiminus_tht","#theta (#pi^{-})",1000,0.,180.);
    */
        
    //pbarp, lam0bar, pi+, pbar
    
    /*
    hpbarp2_chi2 = new TH1D("hpbarp2_chi2","#bar{p}p: #chi^{2} (4C Fit)",100,0,500);
    hpbarp2_prob = new TH1D("hpbarp2_prob","#bar{p}p: Prob (4C Fit)",100,0,1);
    hlam0bar_m = new TH1D("hlam0bar_m","#bar{#Lambda_{0}} - Mass",200,1.0,1.2);
    hlam0bar_vtx_pullx = new TH1D("hlam0bar_vtx_pullx","#bar{#Lambda_{0}} - pull x",100,-100.,100.);
    hlam0bar_vtx_pully = new TH1D("hlam0bar_vtx_pully","#bar{#Lambda_{0}} - pull y",100,-100.,100.);
    hlam0bar_vtx_pullz = new TH1D("hlam0bar_vtx_pullz","#bar{#Lambda_{0}} - pull z",100,-100.,100.);
    hpiplus_tht = new TH1D("hpiplus_tht","#theta (#pi^{+})",100,0.,180.);
    hpbar_tht   = new TH1D("hpbar_tht","#theta (#bar{p})",100,0.,180.);
    */
    

    //*** Canvases
    
    /*
    TCanvas *c1 = new TCanvas("c1","Lambda0",900,700);
    c1->Divide(3,3);

    c1->cd(1); hpbarp1_chi2->Draw();
    c1->cd(2); hpbarp1_prob->Draw();
    c1->cd(3); hlam0_m->Draw();
    c1->cd(4); hlam0_vtx_pullx->Draw();
    c1->cd(5); hlam0_vtx_pully->Draw();
    c1->cd(6); hlam0_vtx_pullz->Draw();
    c1->cd(7); hp_tht->Draw();
    c1->cd(8); hpiminus_tht->Draw();


    TCanvas *c2 = new TCanvas("c2","Lambda0Bar",900,700);
    c2->Divide(3,3);

    c2->cd(1); hpbarp2_chi2->Draw();
    c2->cd(2); hpbarp2_prob->Draw();
    c2->cd(3); hlam0bar_m->Draw();
    c2->cd(4); hlam0bar_vtx_pullx->Draw();
    c2->cd(5); hlam0bar_vtx_pully->Draw();
    c2->cd(6); hlam0bar_vtx_pullz->Draw();
    c2->cd(7); hpbar_tht->Draw();
    c2->cd(8); hpiplus_tht->Draw();
    */


    //*** 
    //*** Added by ADEEL (END)
    //*** ---------------------    
    
    
   // ---------------------------------------------------------------
   //                    This Block Shouldn't be Modified.
   // ---------------------------------------------------------------
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("efwp2-BestPbarP.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("efwp2-BestPbarP.root");
      }
      f->GetObject("ntpBestPbarP",tree);

   }
   
   // Call Init()
   Init(tree);

   // Call Loop() 
   Loop();

   // ---------------------------------------------------------------
   //                    This Block Shouldn't be Modified.
   // ---------------------------------------------------------------
   
  
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***

   //Save to ROOT File
   hfile->Write();
   hfile->Close();

   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
   
}

ntpBestPbarP::~ntpBestPbarP()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ntpBestPbarP::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ntpBestPbarP::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ntpBestPbarP::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("McTruthMatch", &McTruthMatch, &b_McTruthMatch);
   fChain->SetBranchAddress("FourMomFit_Convergence", &FourMomFit_Convergence, &b_FourMomFit_Convergence);
   fChain->SetBranchAddress("FourMomFit_chisq", &FourMomFit_chisq, &b_FourMomFit_chisq);
   fChain->SetBranchAddress("FourMomFit_ndf", &FourMomFit_ndf, &b_FourMomFit_ndf);
   fChain->SetBranchAddress("FourMomFit_prob", &FourMomFit_prob, &b_FourMomFit_prob);
   fChain->SetBranchAddress("d0d0_ky", &d0d0_ky, &b_d0d0_ky);
   fChain->SetBranchAddress("d1d0_ky", &d1d0_ky, &b_d1d0_ky);
   fChain->SetBranchAddress("MC_d0d0_ky", &MC_d0d0_ky, &b_MC_d0d0_ky);
   fChain->SetBranchAddress("MC_d1d0_ky", &MC_d1d0_ky, &b_MC_d1d0_ky);
   fChain->SetBranchAddress("pbarp_px", &pbarp_px, &b_pbarp_px);
   fChain->SetBranchAddress("pbarp_py", &pbarp_py, &b_pbarp_py);
   fChain->SetBranchAddress("pbarp_pz", &pbarp_pz, &b_pbarp_pz);
   fChain->SetBranchAddress("pbarp_e", &pbarp_e, &b_pbarp_e);
   fChain->SetBranchAddress("pbarp_p", &pbarp_p, &b_pbarp_p);
   fChain->SetBranchAddress("pbarp_tht", &pbarp_tht, &b_pbarp_tht);
   fChain->SetBranchAddress("pbarp_phi", &pbarp_phi, &b_pbarp_phi);
   fChain->SetBranchAddress("pbarp_pt", &pbarp_pt, &b_pbarp_pt);
   fChain->SetBranchAddress("pbarp_m", &pbarp_m, &b_pbarp_m);
   fChain->SetBranchAddress("pbarp_x", &pbarp_x, &b_pbarp_x);
   fChain->SetBranchAddress("pbarp_y", &pbarp_y, &b_pbarp_y);
   fChain->SetBranchAddress("pbarp_z", &pbarp_z, &b_pbarp_z);
   fChain->SetBranchAddress("pbarp_l", &pbarp_l, &b_pbarp_l);
   fChain->SetBranchAddress("pbarp_chg", &pbarp_chg, &b_pbarp_chg);
   fChain->SetBranchAddress("pbarp_pdg", &pbarp_pdg, &b_pbarp_pdg);
   fChain->SetBranchAddress("pbarp_pxcm", &pbarp_pxcm, &b_pbarp_pxcm);
   fChain->SetBranchAddress("pbarp_pycm", &pbarp_pycm, &b_pbarp_pycm);
   fChain->SetBranchAddress("pbarp_pzcm", &pbarp_pzcm, &b_pbarp_pzcm);
   fChain->SetBranchAddress("pbarp_ecm", &pbarp_ecm, &b_pbarp_ecm);
   fChain->SetBranchAddress("pbarp_pcm", &pbarp_pcm, &b_pbarp_pcm);
   fChain->SetBranchAddress("pbarp_thtcm", &pbarp_thtcm, &b_pbarp_thtcm);
   fChain->SetBranchAddress("pbarp_phicm", &pbarp_phicm, &b_pbarp_phicm);
   fChain->SetBranchAddress("pbarp_covpxpx", &pbarp_covpxpx, &b_pbarp_covpxpx);
   fChain->SetBranchAddress("pbarp_covpxpy", &pbarp_covpxpy, &b_pbarp_covpxpy);
   fChain->SetBranchAddress("pbarp_covpxpz", &pbarp_covpxpz, &b_pbarp_covpxpz);
   fChain->SetBranchAddress("pbarp_covpxe", &pbarp_covpxe, &b_pbarp_covpxe);
   fChain->SetBranchAddress("pbarp_covpypy", &pbarp_covpypy, &b_pbarp_covpypy);
   fChain->SetBranchAddress("pbarp_covpypz", &pbarp_covpypz, &b_pbarp_covpypz);
   fChain->SetBranchAddress("pbarp_covpye", &pbarp_covpye, &b_pbarp_covpye);
   fChain->SetBranchAddress("pbarp_covpzpz", &pbarp_covpzpz, &b_pbarp_covpzpz);
   fChain->SetBranchAddress("pbarp_covpze", &pbarp_covpze, &b_pbarp_covpze);
   fChain->SetBranchAddress("pbarp_covee", &pbarp_covee, &b_pbarp_covee);
   fChain->SetBranchAddress("pbarp_pullpx", &pbarp_pullpx, &b_pbarp_pullpx);
   fChain->SetBranchAddress("pbarp_pullpy", &pbarp_pullpy, &b_pbarp_pullpy);
   fChain->SetBranchAddress("pbarp_pullpz", &pbarp_pullpz, &b_pbarp_pullpz);
   fChain->SetBranchAddress("pbarp_pulle", &pbarp_pulle, &b_pbarp_pulle);
   fChain->SetBranchAddress("pbarp_pullx", &pbarp_pullx, &b_pbarp_pullx);
   fChain->SetBranchAddress("pbarp_pully", &pbarp_pully, &b_pbarp_pully);
   fChain->SetBranchAddress("pbarp_pullz", &pbarp_pullz, &b_pbarp_pullz);
   fChain->SetBranchAddress("pbarp_pullpxpy", &pbarp_pullpxpy, &b_pbarp_pullpxpy);
   fChain->SetBranchAddress("pbarp_pullpxpz", &pbarp_pullpxpz, &b_pbarp_pullpxpz);
   fChain->SetBranchAddress("pbarp_pullpypz", &pbarp_pullpypz, &b_pbarp_pullpypz);
   fChain->SetBranchAddress("pbarp_pullpxe", &pbarp_pullpxe, &b_pbarp_pullpxe);
   fChain->SetBranchAddress("pbarp_pullpye", &pbarp_pullpye, &b_pbarp_pullpye);
   fChain->SetBranchAddress("pbarp_pullpze", &pbarp_pullpze, &b_pbarp_pullpze);
   fChain->SetBranchAddress("pbarp_pullxy", &pbarp_pullxy, &b_pbarp_pullxy);
   fChain->SetBranchAddress("pbarp_pullxz", &pbarp_pullxz, &b_pbarp_pullxz);
   fChain->SetBranchAddress("pbarp_pullyz", &pbarp_pullyz, &b_pbarp_pullyz);
   fChain->SetBranchAddress("pbarp_mct", &pbarp_mct, &b_pbarp_mct);
   fChain->SetBranchAddress("pbarp_mdif", &pbarp_mdif, &b_pbarp_mdif);
   fChain->SetBranchAddress("pbarp_oang", &pbarp_oang, &b_pbarp_oang);
   fChain->SetBranchAddress("pbarp_decang", &pbarp_decang, &b_pbarp_decang);
   fChain->SetBranchAddress("pbarp_cdecang", &pbarp_cdecang, &b_pbarp_cdecang);
   fChain->SetBranchAddress("pbarp_d0px", &pbarp_d0px, &b_pbarp_d0px);
   fChain->SetBranchAddress("pbarp_d0py", &pbarp_d0py, &b_pbarp_d0py);
   fChain->SetBranchAddress("pbarp_d0pz", &pbarp_d0pz, &b_pbarp_d0pz);
   fChain->SetBranchAddress("pbarp_d0e", &pbarp_d0e, &b_pbarp_d0e);
   fChain->SetBranchAddress("pbarp_d0p", &pbarp_d0p, &b_pbarp_d0p);
   fChain->SetBranchAddress("pbarp_d0tht", &pbarp_d0tht, &b_pbarp_d0tht);
   fChain->SetBranchAddress("pbarp_d0phi", &pbarp_d0phi, &b_pbarp_d0phi);
   fChain->SetBranchAddress("pbarp_d0pt", &pbarp_d0pt, &b_pbarp_d0pt);
   fChain->SetBranchAddress("pbarp_d0m", &pbarp_d0m, &b_pbarp_d0m);
   fChain->SetBranchAddress("pbarp_d0x", &pbarp_d0x, &b_pbarp_d0x);
   fChain->SetBranchAddress("pbarp_d0y", &pbarp_d0y, &b_pbarp_d0y);
   fChain->SetBranchAddress("pbarp_d0z", &pbarp_d0z, &b_pbarp_d0z);
   fChain->SetBranchAddress("pbarp_d0l", &pbarp_d0l, &b_pbarp_d0l);
   fChain->SetBranchAddress("pbarp_d0chg", &pbarp_d0chg, &b_pbarp_d0chg);
   fChain->SetBranchAddress("pbarp_d0pdg", &pbarp_d0pdg, &b_pbarp_d0pdg);
   fChain->SetBranchAddress("pbarp_d0pxcm", &pbarp_d0pxcm, &b_pbarp_d0pxcm);
   fChain->SetBranchAddress("pbarp_d0pycm", &pbarp_d0pycm, &b_pbarp_d0pycm);
   fChain->SetBranchAddress("pbarp_d0pzcm", &pbarp_d0pzcm, &b_pbarp_d0pzcm);
   fChain->SetBranchAddress("pbarp_d0ecm", &pbarp_d0ecm, &b_pbarp_d0ecm);
   fChain->SetBranchAddress("pbarp_d0pcm", &pbarp_d0pcm, &b_pbarp_d0pcm);
   fChain->SetBranchAddress("pbarp_d0thtcm", &pbarp_d0thtcm, &b_pbarp_d0thtcm);
   fChain->SetBranchAddress("pbarp_d0phicm", &pbarp_d0phicm, &b_pbarp_d0phicm);
   fChain->SetBranchAddress("pbarp_d0covpxpx", &pbarp_d0covpxpx, &b_pbarp_d0covpxpx);
   fChain->SetBranchAddress("pbarp_d0covpxpy", &pbarp_d0covpxpy, &b_pbarp_d0covpxpy);
   fChain->SetBranchAddress("pbarp_d0covpxpz", &pbarp_d0covpxpz, &b_pbarp_d0covpxpz);
   fChain->SetBranchAddress("pbarp_d0covpxe", &pbarp_d0covpxe, &b_pbarp_d0covpxe);
   fChain->SetBranchAddress("pbarp_d0covpypy", &pbarp_d0covpypy, &b_pbarp_d0covpypy);
   fChain->SetBranchAddress("pbarp_d0covpypz", &pbarp_d0covpypz, &b_pbarp_d0covpypz);
   fChain->SetBranchAddress("pbarp_d0covpye", &pbarp_d0covpye, &b_pbarp_d0covpye);
   fChain->SetBranchAddress("pbarp_d0covpzpz", &pbarp_d0covpzpz, &b_pbarp_d0covpzpz);
   fChain->SetBranchAddress("pbarp_d0covpze", &pbarp_d0covpze, &b_pbarp_d0covpze);
   fChain->SetBranchAddress("pbarp_d0covee", &pbarp_d0covee, &b_pbarp_d0covee);
   fChain->SetBranchAddress("pbarp_d0pullpx", &pbarp_d0pullpx, &b_pbarp_d0pullpx);
   fChain->SetBranchAddress("pbarp_d0pullpy", &pbarp_d0pullpy, &b_pbarp_d0pullpy);
   fChain->SetBranchAddress("pbarp_d0pullpz", &pbarp_d0pullpz, &b_pbarp_d0pullpz);
   fChain->SetBranchAddress("pbarp_d0pulle", &pbarp_d0pulle, &b_pbarp_d0pulle);
   fChain->SetBranchAddress("pbarp_d0pullx", &pbarp_d0pullx, &b_pbarp_d0pullx);
   fChain->SetBranchAddress("pbarp_d0pully", &pbarp_d0pully, &b_pbarp_d0pully);
   fChain->SetBranchAddress("pbarp_d0pullz", &pbarp_d0pullz, &b_pbarp_d0pullz);
   fChain->SetBranchAddress("pbarp_d0pullpxpy", &pbarp_d0pullpxpy, &b_pbarp_d0pullpxpy);
   fChain->SetBranchAddress("pbarp_d0pullpxpz", &pbarp_d0pullpxpz, &b_pbarp_d0pullpxpz);
   fChain->SetBranchAddress("pbarp_d0pullpypz", &pbarp_d0pullpypz, &b_pbarp_d0pullpypz);
   fChain->SetBranchAddress("pbarp_d0pullpxe", &pbarp_d0pullpxe, &b_pbarp_d0pullpxe);
   fChain->SetBranchAddress("pbarp_d0pullpye", &pbarp_d0pullpye, &b_pbarp_d0pullpye);
   fChain->SetBranchAddress("pbarp_d0pullpze", &pbarp_d0pullpze, &b_pbarp_d0pullpze);
   fChain->SetBranchAddress("pbarp_d0pullxy", &pbarp_d0pullxy, &b_pbarp_d0pullxy);
   fChain->SetBranchAddress("pbarp_d0pullxz", &pbarp_d0pullxz, &b_pbarp_d0pullxz);
   fChain->SetBranchAddress("pbarp_d0pullyz", &pbarp_d0pullyz, &b_pbarp_d0pullyz);
   fChain->SetBranchAddress("pbarp_d0mct", &pbarp_d0mct, &b_pbarp_d0mct);
   fChain->SetBranchAddress("pbarp_d0mdif", &pbarp_d0mdif, &b_pbarp_d0mdif);
   fChain->SetBranchAddress("pbarp_d0oang", &pbarp_d0oang, &b_pbarp_d0oang);
   fChain->SetBranchAddress("pbarp_d0decang", &pbarp_d0decang, &b_pbarp_d0decang);
   fChain->SetBranchAddress("pbarp_d0cdecang", &pbarp_d0cdecang, &b_pbarp_d0cdecang);
   fChain->SetBranchAddress("pbarp_d0d0px", &pbarp_d0d0px, &b_pbarp_d0d0px);
   fChain->SetBranchAddress("pbarp_d0d0py", &pbarp_d0d0py, &b_pbarp_d0d0py);
   fChain->SetBranchAddress("pbarp_d0d0pz", &pbarp_d0d0pz, &b_pbarp_d0d0pz);
   fChain->SetBranchAddress("pbarp_d0d0e", &pbarp_d0d0e, &b_pbarp_d0d0e);
   fChain->SetBranchAddress("pbarp_d0d0p", &pbarp_d0d0p, &b_pbarp_d0d0p);
   fChain->SetBranchAddress("pbarp_d0d0tht", &pbarp_d0d0tht, &b_pbarp_d0d0tht);
   fChain->SetBranchAddress("pbarp_d0d0phi", &pbarp_d0d0phi, &b_pbarp_d0d0phi);
   fChain->SetBranchAddress("pbarp_d0d0pt", &pbarp_d0d0pt, &b_pbarp_d0d0pt);
   fChain->SetBranchAddress("pbarp_d0d0m", &pbarp_d0d0m, &b_pbarp_d0d0m);
   fChain->SetBranchAddress("pbarp_d0d0x", &pbarp_d0d0x, &b_pbarp_d0d0x);
   fChain->SetBranchAddress("pbarp_d0d0y", &pbarp_d0d0y, &b_pbarp_d0d0y);
   fChain->SetBranchAddress("pbarp_d0d0z", &pbarp_d0d0z, &b_pbarp_d0d0z);
   fChain->SetBranchAddress("pbarp_d0d0l", &pbarp_d0d0l, &b_pbarp_d0d0l);
   fChain->SetBranchAddress("pbarp_d0d0chg", &pbarp_d0d0chg, &b_pbarp_d0d0chg);
   fChain->SetBranchAddress("pbarp_d0d0pdg", &pbarp_d0d0pdg, &b_pbarp_d0d0pdg);
   fChain->SetBranchAddress("pbarp_d0d0pxcm", &pbarp_d0d0pxcm, &b_pbarp_d0d0pxcm);
   fChain->SetBranchAddress("pbarp_d0d0pycm", &pbarp_d0d0pycm, &b_pbarp_d0d0pycm);
   fChain->SetBranchAddress("pbarp_d0d0pzcm", &pbarp_d0d0pzcm, &b_pbarp_d0d0pzcm);
   fChain->SetBranchAddress("pbarp_d0d0ecm", &pbarp_d0d0ecm, &b_pbarp_d0d0ecm);
   fChain->SetBranchAddress("pbarp_d0d0pcm", &pbarp_d0d0pcm, &b_pbarp_d0d0pcm);
   fChain->SetBranchAddress("pbarp_d0d0thtcm", &pbarp_d0d0thtcm, &b_pbarp_d0d0thtcm);
   fChain->SetBranchAddress("pbarp_d0d0phicm", &pbarp_d0d0phicm, &b_pbarp_d0d0phicm);
   fChain->SetBranchAddress("pbarp_d0d0covpxpx", &pbarp_d0d0covpxpx, &b_pbarp_d0d0covpxpx);
   fChain->SetBranchAddress("pbarp_d0d0covpxpy", &pbarp_d0d0covpxpy, &b_pbarp_d0d0covpxpy);
   fChain->SetBranchAddress("pbarp_d0d0covpxpz", &pbarp_d0d0covpxpz, &b_pbarp_d0d0covpxpz);
   fChain->SetBranchAddress("pbarp_d0d0covpxe", &pbarp_d0d0covpxe, &b_pbarp_d0d0covpxe);
   fChain->SetBranchAddress("pbarp_d0d0covpypy", &pbarp_d0d0covpypy, &b_pbarp_d0d0covpypy);
   fChain->SetBranchAddress("pbarp_d0d0covpypz", &pbarp_d0d0covpypz, &b_pbarp_d0d0covpypz);
   fChain->SetBranchAddress("pbarp_d0d0covpye", &pbarp_d0d0covpye, &b_pbarp_d0d0covpye);
   fChain->SetBranchAddress("pbarp_d0d0covpzpz", &pbarp_d0d0covpzpz, &b_pbarp_d0d0covpzpz);
   fChain->SetBranchAddress("pbarp_d0d0covpze", &pbarp_d0d0covpze, &b_pbarp_d0d0covpze);
   fChain->SetBranchAddress("pbarp_d0d0covee", &pbarp_d0d0covee, &b_pbarp_d0d0covee);
   fChain->SetBranchAddress("pbarp_d0d0pullpx", &pbarp_d0d0pullpx, &b_pbarp_d0d0pullpx);
   fChain->SetBranchAddress("pbarp_d0d0pullpy", &pbarp_d0d0pullpy, &b_pbarp_d0d0pullpy);
   fChain->SetBranchAddress("pbarp_d0d0pullpz", &pbarp_d0d0pullpz, &b_pbarp_d0d0pullpz);
   fChain->SetBranchAddress("pbarp_d0d0pulle", &pbarp_d0d0pulle, &b_pbarp_d0d0pulle);
   fChain->SetBranchAddress("pbarp_d0d0pullx", &pbarp_d0d0pullx, &b_pbarp_d0d0pullx);
   fChain->SetBranchAddress("pbarp_d0d0pully", &pbarp_d0d0pully, &b_pbarp_d0d0pully);
   fChain->SetBranchAddress("pbarp_d0d0pullz", &pbarp_d0d0pullz, &b_pbarp_d0d0pullz);
   fChain->SetBranchAddress("pbarp_d0d0pullpxpy", &pbarp_d0d0pullpxpy, &b_pbarp_d0d0pullpxpy);
   fChain->SetBranchAddress("pbarp_d0d0pullpxpz", &pbarp_d0d0pullpxpz, &b_pbarp_d0d0pullpxpz);
   fChain->SetBranchAddress("pbarp_d0d0pullpypz", &pbarp_d0d0pullpypz, &b_pbarp_d0d0pullpypz);
   fChain->SetBranchAddress("pbarp_d0d0pullpxe", &pbarp_d0d0pullpxe, &b_pbarp_d0d0pullpxe);
   fChain->SetBranchAddress("pbarp_d0d0pullpye", &pbarp_d0d0pullpye, &b_pbarp_d0d0pullpye);
   fChain->SetBranchAddress("pbarp_d0d0pullpze", &pbarp_d0d0pullpze, &b_pbarp_d0d0pullpze);
   fChain->SetBranchAddress("pbarp_d0d0pullxy", &pbarp_d0d0pullxy, &b_pbarp_d0d0pullxy);
   fChain->SetBranchAddress("pbarp_d0d0pullxz", &pbarp_d0d0pullxz, &b_pbarp_d0d0pullxz);
   fChain->SetBranchAddress("pbarp_d0d0pullyz", &pbarp_d0d0pullyz, &b_pbarp_d0d0pullyz);
   fChain->SetBranchAddress("pbarp_d0d0mct", &pbarp_d0d0mct, &b_pbarp_d0d0mct);
   fChain->SetBranchAddress("pbarp_d0d0pide", &pbarp_d0d0pide, &b_pbarp_d0d0pide);
   fChain->SetBranchAddress("pbarp_d0d0pidmu", &pbarp_d0d0pidmu, &b_pbarp_d0d0pidmu);
   fChain->SetBranchAddress("pbarp_d0d0pidpi", &pbarp_d0d0pidpi, &b_pbarp_d0d0pidpi);
   fChain->SetBranchAddress("pbarp_d0d0pidk", &pbarp_d0d0pidk, &b_pbarp_d0d0pidk);
   fChain->SetBranchAddress("pbarp_d0d0pidp", &pbarp_d0d0pidp, &b_pbarp_d0d0pidp);
   fChain->SetBranchAddress("pbarp_d0d0pidmax", &pbarp_d0d0pidmax, &b_pbarp_d0d0pidmax);
   fChain->SetBranchAddress("pbarp_d0d0pidbest", &pbarp_d0d0pidbest, &b_pbarp_d0d0pidbest);
   fChain->SetBranchAddress("pbarp_d0d0trpdg", &pbarp_d0d0trpdg, &b_pbarp_d0d0trpdg);
   fChain->SetBranchAddress("pbarp_d0d1px", &pbarp_d0d1px, &b_pbarp_d0d1px);
   fChain->SetBranchAddress("pbarp_d0d1py", &pbarp_d0d1py, &b_pbarp_d0d1py);
   fChain->SetBranchAddress("pbarp_d0d1pz", &pbarp_d0d1pz, &b_pbarp_d0d1pz);
   fChain->SetBranchAddress("pbarp_d0d1e", &pbarp_d0d1e, &b_pbarp_d0d1e);
   fChain->SetBranchAddress("pbarp_d0d1p", &pbarp_d0d1p, &b_pbarp_d0d1p);
   fChain->SetBranchAddress("pbarp_d0d1tht", &pbarp_d0d1tht, &b_pbarp_d0d1tht);
   fChain->SetBranchAddress("pbarp_d0d1phi", &pbarp_d0d1phi, &b_pbarp_d0d1phi);
   fChain->SetBranchAddress("pbarp_d0d1pt", &pbarp_d0d1pt, &b_pbarp_d0d1pt);
   fChain->SetBranchAddress("pbarp_d0d1m", &pbarp_d0d1m, &b_pbarp_d0d1m);
   fChain->SetBranchAddress("pbarp_d0d1x", &pbarp_d0d1x, &b_pbarp_d0d1x);
   fChain->SetBranchAddress("pbarp_d0d1y", &pbarp_d0d1y, &b_pbarp_d0d1y);
   fChain->SetBranchAddress("pbarp_d0d1z", &pbarp_d0d1z, &b_pbarp_d0d1z);
   fChain->SetBranchAddress("pbarp_d0d1l", &pbarp_d0d1l, &b_pbarp_d0d1l);
   fChain->SetBranchAddress("pbarp_d0d1chg", &pbarp_d0d1chg, &b_pbarp_d0d1chg);
   fChain->SetBranchAddress("pbarp_d0d1pdg", &pbarp_d0d1pdg, &b_pbarp_d0d1pdg);
   fChain->SetBranchAddress("pbarp_d0d1pxcm", &pbarp_d0d1pxcm, &b_pbarp_d0d1pxcm);
   fChain->SetBranchAddress("pbarp_d0d1pycm", &pbarp_d0d1pycm, &b_pbarp_d0d1pycm);
   fChain->SetBranchAddress("pbarp_d0d1pzcm", &pbarp_d0d1pzcm, &b_pbarp_d0d1pzcm);
   fChain->SetBranchAddress("pbarp_d0d1ecm", &pbarp_d0d1ecm, &b_pbarp_d0d1ecm);
   fChain->SetBranchAddress("pbarp_d0d1pcm", &pbarp_d0d1pcm, &b_pbarp_d0d1pcm);
   fChain->SetBranchAddress("pbarp_d0d1thtcm", &pbarp_d0d1thtcm, &b_pbarp_d0d1thtcm);
   fChain->SetBranchAddress("pbarp_d0d1phicm", &pbarp_d0d1phicm, &b_pbarp_d0d1phicm);
   fChain->SetBranchAddress("pbarp_d0d1covpxpx", &pbarp_d0d1covpxpx, &b_pbarp_d0d1covpxpx);
   fChain->SetBranchAddress("pbarp_d0d1covpxpy", &pbarp_d0d1covpxpy, &b_pbarp_d0d1covpxpy);
   fChain->SetBranchAddress("pbarp_d0d1covpxpz", &pbarp_d0d1covpxpz, &b_pbarp_d0d1covpxpz);
   fChain->SetBranchAddress("pbarp_d0d1covpxe", &pbarp_d0d1covpxe, &b_pbarp_d0d1covpxe);
   fChain->SetBranchAddress("pbarp_d0d1covpypy", &pbarp_d0d1covpypy, &b_pbarp_d0d1covpypy);
   fChain->SetBranchAddress("pbarp_d0d1covpypz", &pbarp_d0d1covpypz, &b_pbarp_d0d1covpypz);
   fChain->SetBranchAddress("pbarp_d0d1covpye", &pbarp_d0d1covpye, &b_pbarp_d0d1covpye);
   fChain->SetBranchAddress("pbarp_d0d1covpzpz", &pbarp_d0d1covpzpz, &b_pbarp_d0d1covpzpz);
   fChain->SetBranchAddress("pbarp_d0d1covpze", &pbarp_d0d1covpze, &b_pbarp_d0d1covpze);
   fChain->SetBranchAddress("pbarp_d0d1covee", &pbarp_d0d1covee, &b_pbarp_d0d1covee);
   fChain->SetBranchAddress("pbarp_d0d1pullpx", &pbarp_d0d1pullpx, &b_pbarp_d0d1pullpx);
   fChain->SetBranchAddress("pbarp_d0d1pullpy", &pbarp_d0d1pullpy, &b_pbarp_d0d1pullpy);
   fChain->SetBranchAddress("pbarp_d0d1pullpz", &pbarp_d0d1pullpz, &b_pbarp_d0d1pullpz);
   fChain->SetBranchAddress("pbarp_d0d1pulle", &pbarp_d0d1pulle, &b_pbarp_d0d1pulle);
   fChain->SetBranchAddress("pbarp_d0d1pullx", &pbarp_d0d1pullx, &b_pbarp_d0d1pullx);
   fChain->SetBranchAddress("pbarp_d0d1pully", &pbarp_d0d1pully, &b_pbarp_d0d1pully);
   fChain->SetBranchAddress("pbarp_d0d1pullz", &pbarp_d0d1pullz, &b_pbarp_d0d1pullz);
   fChain->SetBranchAddress("pbarp_d0d1pullpxpy", &pbarp_d0d1pullpxpy, &b_pbarp_d0d1pullpxpy);
   fChain->SetBranchAddress("pbarp_d0d1pullpxpz", &pbarp_d0d1pullpxpz, &b_pbarp_d0d1pullpxpz);
   fChain->SetBranchAddress("pbarp_d0d1pullpypz", &pbarp_d0d1pullpypz, &b_pbarp_d0d1pullpypz);
   fChain->SetBranchAddress("pbarp_d0d1pullpxe", &pbarp_d0d1pullpxe, &b_pbarp_d0d1pullpxe);
   fChain->SetBranchAddress("pbarp_d0d1pullpye", &pbarp_d0d1pullpye, &b_pbarp_d0d1pullpye);
   fChain->SetBranchAddress("pbarp_d0d1pullpze", &pbarp_d0d1pullpze, &b_pbarp_d0d1pullpze);
   fChain->SetBranchAddress("pbarp_d0d1pullxy", &pbarp_d0d1pullxy, &b_pbarp_d0d1pullxy);
   fChain->SetBranchAddress("pbarp_d0d1pullxz", &pbarp_d0d1pullxz, &b_pbarp_d0d1pullxz);
   fChain->SetBranchAddress("pbarp_d0d1pullyz", &pbarp_d0d1pullyz, &b_pbarp_d0d1pullyz);
   fChain->SetBranchAddress("pbarp_d0d1mct", &pbarp_d0d1mct, &b_pbarp_d0d1mct);
   fChain->SetBranchAddress("pbarp_d0d1pide", &pbarp_d0d1pide, &b_pbarp_d0d1pide);
   fChain->SetBranchAddress("pbarp_d0d1pidmu", &pbarp_d0d1pidmu, &b_pbarp_d0d1pidmu);
   fChain->SetBranchAddress("pbarp_d0d1pidpi", &pbarp_d0d1pidpi, &b_pbarp_d0d1pidpi);
   fChain->SetBranchAddress("pbarp_d0d1pidk", &pbarp_d0d1pidk, &b_pbarp_d0d1pidk);
   fChain->SetBranchAddress("pbarp_d0d1pidp", &pbarp_d0d1pidp, &b_pbarp_d0d1pidp);
   fChain->SetBranchAddress("pbarp_d0d1pidmax", &pbarp_d0d1pidmax, &b_pbarp_d0d1pidmax);
   fChain->SetBranchAddress("pbarp_d0d1pidbest", &pbarp_d0d1pidbest, &b_pbarp_d0d1pidbest);
   fChain->SetBranchAddress("pbarp_d0d1trpdg", &pbarp_d0d1trpdg, &b_pbarp_d0d1trpdg);
   fChain->SetBranchAddress("pbarp_d1px", &pbarp_d1px, &b_pbarp_d1px);
   fChain->SetBranchAddress("pbarp_d1py", &pbarp_d1py, &b_pbarp_d1py);
   fChain->SetBranchAddress("pbarp_d1pz", &pbarp_d1pz, &b_pbarp_d1pz);
   fChain->SetBranchAddress("pbarp_d1e", &pbarp_d1e, &b_pbarp_d1e);
   fChain->SetBranchAddress("pbarp_d1p", &pbarp_d1p, &b_pbarp_d1p);
   fChain->SetBranchAddress("pbarp_d1tht", &pbarp_d1tht, &b_pbarp_d1tht);
   fChain->SetBranchAddress("pbarp_d1phi", &pbarp_d1phi, &b_pbarp_d1phi);
   fChain->SetBranchAddress("pbarp_d1pt", &pbarp_d1pt, &b_pbarp_d1pt);
   fChain->SetBranchAddress("pbarp_d1m", &pbarp_d1m, &b_pbarp_d1m);
   fChain->SetBranchAddress("pbarp_d1x", &pbarp_d1x, &b_pbarp_d1x);
   fChain->SetBranchAddress("pbarp_d1y", &pbarp_d1y, &b_pbarp_d1y);
   fChain->SetBranchAddress("pbarp_d1z", &pbarp_d1z, &b_pbarp_d1z);
   fChain->SetBranchAddress("pbarp_d1l", &pbarp_d1l, &b_pbarp_d1l);
   fChain->SetBranchAddress("pbarp_d1chg", &pbarp_d1chg, &b_pbarp_d1chg);
   fChain->SetBranchAddress("pbarp_d1pdg", &pbarp_d1pdg, &b_pbarp_d1pdg);
   fChain->SetBranchAddress("pbarp_d1pxcm", &pbarp_d1pxcm, &b_pbarp_d1pxcm);
   fChain->SetBranchAddress("pbarp_d1pycm", &pbarp_d1pycm, &b_pbarp_d1pycm);
   fChain->SetBranchAddress("pbarp_d1pzcm", &pbarp_d1pzcm, &b_pbarp_d1pzcm);
   fChain->SetBranchAddress("pbarp_d1ecm", &pbarp_d1ecm, &b_pbarp_d1ecm);
   fChain->SetBranchAddress("pbarp_d1pcm", &pbarp_d1pcm, &b_pbarp_d1pcm);
   fChain->SetBranchAddress("pbarp_d1thtcm", &pbarp_d1thtcm, &b_pbarp_d1thtcm);
   fChain->SetBranchAddress("pbarp_d1phicm", &pbarp_d1phicm, &b_pbarp_d1phicm);
   fChain->SetBranchAddress("pbarp_d1covpxpx", &pbarp_d1covpxpx, &b_pbarp_d1covpxpx);
   fChain->SetBranchAddress("pbarp_d1covpxpy", &pbarp_d1covpxpy, &b_pbarp_d1covpxpy);
   fChain->SetBranchAddress("pbarp_d1covpxpz", &pbarp_d1covpxpz, &b_pbarp_d1covpxpz);
   fChain->SetBranchAddress("pbarp_d1covpxe", &pbarp_d1covpxe, &b_pbarp_d1covpxe);
   fChain->SetBranchAddress("pbarp_d1covpypy", &pbarp_d1covpypy, &b_pbarp_d1covpypy);
   fChain->SetBranchAddress("pbarp_d1covpypz", &pbarp_d1covpypz, &b_pbarp_d1covpypz);
   fChain->SetBranchAddress("pbarp_d1covpye", &pbarp_d1covpye, &b_pbarp_d1covpye);
   fChain->SetBranchAddress("pbarp_d1covpzpz", &pbarp_d1covpzpz, &b_pbarp_d1covpzpz);
   fChain->SetBranchAddress("pbarp_d1covpze", &pbarp_d1covpze, &b_pbarp_d1covpze);
   fChain->SetBranchAddress("pbarp_d1covee", &pbarp_d1covee, &b_pbarp_d1covee);
   fChain->SetBranchAddress("pbarp_d1pullpx", &pbarp_d1pullpx, &b_pbarp_d1pullpx);
   fChain->SetBranchAddress("pbarp_d1pullpy", &pbarp_d1pullpy, &b_pbarp_d1pullpy);
   fChain->SetBranchAddress("pbarp_d1pullpz", &pbarp_d1pullpz, &b_pbarp_d1pullpz);
   fChain->SetBranchAddress("pbarp_d1pulle", &pbarp_d1pulle, &b_pbarp_d1pulle);
   fChain->SetBranchAddress("pbarp_d1pullx", &pbarp_d1pullx, &b_pbarp_d1pullx);
   fChain->SetBranchAddress("pbarp_d1pully", &pbarp_d1pully, &b_pbarp_d1pully);
   fChain->SetBranchAddress("pbarp_d1pullz", &pbarp_d1pullz, &b_pbarp_d1pullz);
   fChain->SetBranchAddress("pbarp_d1pullpxpy", &pbarp_d1pullpxpy, &b_pbarp_d1pullpxpy);
   fChain->SetBranchAddress("pbarp_d1pullpxpz", &pbarp_d1pullpxpz, &b_pbarp_d1pullpxpz);
   fChain->SetBranchAddress("pbarp_d1pullpypz", &pbarp_d1pullpypz, &b_pbarp_d1pullpypz);
   fChain->SetBranchAddress("pbarp_d1pullpxe", &pbarp_d1pullpxe, &b_pbarp_d1pullpxe);
   fChain->SetBranchAddress("pbarp_d1pullpye", &pbarp_d1pullpye, &b_pbarp_d1pullpye);
   fChain->SetBranchAddress("pbarp_d1pullpze", &pbarp_d1pullpze, &b_pbarp_d1pullpze);
   fChain->SetBranchAddress("pbarp_d1pullxy", &pbarp_d1pullxy, &b_pbarp_d1pullxy);
   fChain->SetBranchAddress("pbarp_d1pullxz", &pbarp_d1pullxz, &b_pbarp_d1pullxz);
   fChain->SetBranchAddress("pbarp_d1pullyz", &pbarp_d1pullyz, &b_pbarp_d1pullyz);
   fChain->SetBranchAddress("pbarp_d1mct", &pbarp_d1mct, &b_pbarp_d1mct);
   fChain->SetBranchAddress("pbarp_d1mdif", &pbarp_d1mdif, &b_pbarp_d1mdif);
   fChain->SetBranchAddress("pbarp_d1oang", &pbarp_d1oang, &b_pbarp_d1oang);
   fChain->SetBranchAddress("pbarp_d1decang", &pbarp_d1decang, &b_pbarp_d1decang);
   fChain->SetBranchAddress("pbarp_d1cdecang", &pbarp_d1cdecang, &b_pbarp_d1cdecang);
   fChain->SetBranchAddress("pbarp_d1d0px", &pbarp_d1d0px, &b_pbarp_d1d0px);
   fChain->SetBranchAddress("pbarp_d1d0py", &pbarp_d1d0py, &b_pbarp_d1d0py);
   fChain->SetBranchAddress("pbarp_d1d0pz", &pbarp_d1d0pz, &b_pbarp_d1d0pz);
   fChain->SetBranchAddress("pbarp_d1d0e", &pbarp_d1d0e, &b_pbarp_d1d0e);
   fChain->SetBranchAddress("pbarp_d1d0p", &pbarp_d1d0p, &b_pbarp_d1d0p);
   fChain->SetBranchAddress("pbarp_d1d0tht", &pbarp_d1d0tht, &b_pbarp_d1d0tht);
   fChain->SetBranchAddress("pbarp_d1d0phi", &pbarp_d1d0phi, &b_pbarp_d1d0phi);
   fChain->SetBranchAddress("pbarp_d1d0pt", &pbarp_d1d0pt, &b_pbarp_d1d0pt);
   fChain->SetBranchAddress("pbarp_d1d0m", &pbarp_d1d0m, &b_pbarp_d1d0m);
   fChain->SetBranchAddress("pbarp_d1d0x", &pbarp_d1d0x, &b_pbarp_d1d0x);
   fChain->SetBranchAddress("pbarp_d1d0y", &pbarp_d1d0y, &b_pbarp_d1d0y);
   fChain->SetBranchAddress("pbarp_d1d0z", &pbarp_d1d0z, &b_pbarp_d1d0z);
   fChain->SetBranchAddress("pbarp_d1d0l", &pbarp_d1d0l, &b_pbarp_d1d0l);
   fChain->SetBranchAddress("pbarp_d1d0chg", &pbarp_d1d0chg, &b_pbarp_d1d0chg);
   fChain->SetBranchAddress("pbarp_d1d0pdg", &pbarp_d1d0pdg, &b_pbarp_d1d0pdg);
   fChain->SetBranchAddress("pbarp_d1d0pxcm", &pbarp_d1d0pxcm, &b_pbarp_d1d0pxcm);
   fChain->SetBranchAddress("pbarp_d1d0pycm", &pbarp_d1d0pycm, &b_pbarp_d1d0pycm);
   fChain->SetBranchAddress("pbarp_d1d0pzcm", &pbarp_d1d0pzcm, &b_pbarp_d1d0pzcm);
   fChain->SetBranchAddress("pbarp_d1d0ecm", &pbarp_d1d0ecm, &b_pbarp_d1d0ecm);
   fChain->SetBranchAddress("pbarp_d1d0pcm", &pbarp_d1d0pcm, &b_pbarp_d1d0pcm);
   fChain->SetBranchAddress("pbarp_d1d0thtcm", &pbarp_d1d0thtcm, &b_pbarp_d1d0thtcm);
   fChain->SetBranchAddress("pbarp_d1d0phicm", &pbarp_d1d0phicm, &b_pbarp_d1d0phicm);
   fChain->SetBranchAddress("pbarp_d1d0covpxpx", &pbarp_d1d0covpxpx, &b_pbarp_d1d0covpxpx);
   fChain->SetBranchAddress("pbarp_d1d0covpxpy", &pbarp_d1d0covpxpy, &b_pbarp_d1d0covpxpy);
   fChain->SetBranchAddress("pbarp_d1d0covpxpz", &pbarp_d1d0covpxpz, &b_pbarp_d1d0covpxpz);
   fChain->SetBranchAddress("pbarp_d1d0covpxe", &pbarp_d1d0covpxe, &b_pbarp_d1d0covpxe);
   fChain->SetBranchAddress("pbarp_d1d0covpypy", &pbarp_d1d0covpypy, &b_pbarp_d1d0covpypy);
   fChain->SetBranchAddress("pbarp_d1d0covpypz", &pbarp_d1d0covpypz, &b_pbarp_d1d0covpypz);
   fChain->SetBranchAddress("pbarp_d1d0covpye", &pbarp_d1d0covpye, &b_pbarp_d1d0covpye);
   fChain->SetBranchAddress("pbarp_d1d0covpzpz", &pbarp_d1d0covpzpz, &b_pbarp_d1d0covpzpz);
   fChain->SetBranchAddress("pbarp_d1d0covpze", &pbarp_d1d0covpze, &b_pbarp_d1d0covpze);
   fChain->SetBranchAddress("pbarp_d1d0covee", &pbarp_d1d0covee, &b_pbarp_d1d0covee);
   fChain->SetBranchAddress("pbarp_d1d0pullpx", &pbarp_d1d0pullpx, &b_pbarp_d1d0pullpx);
   fChain->SetBranchAddress("pbarp_d1d0pullpy", &pbarp_d1d0pullpy, &b_pbarp_d1d0pullpy);
   fChain->SetBranchAddress("pbarp_d1d0pullpz", &pbarp_d1d0pullpz, &b_pbarp_d1d0pullpz);
   fChain->SetBranchAddress("pbarp_d1d0pulle", &pbarp_d1d0pulle, &b_pbarp_d1d0pulle);
   fChain->SetBranchAddress("pbarp_d1d0pullx", &pbarp_d1d0pullx, &b_pbarp_d1d0pullx);
   fChain->SetBranchAddress("pbarp_d1d0pully", &pbarp_d1d0pully, &b_pbarp_d1d0pully);
   fChain->SetBranchAddress("pbarp_d1d0pullz", &pbarp_d1d0pullz, &b_pbarp_d1d0pullz);
   fChain->SetBranchAddress("pbarp_d1d0pullpxpy", &pbarp_d1d0pullpxpy, &b_pbarp_d1d0pullpxpy);
   fChain->SetBranchAddress("pbarp_d1d0pullpxpz", &pbarp_d1d0pullpxpz, &b_pbarp_d1d0pullpxpz);
   fChain->SetBranchAddress("pbarp_d1d0pullpypz", &pbarp_d1d0pullpypz, &b_pbarp_d1d0pullpypz);
   fChain->SetBranchAddress("pbarp_d1d0pullpxe", &pbarp_d1d0pullpxe, &b_pbarp_d1d0pullpxe);
   fChain->SetBranchAddress("pbarp_d1d0pullpye", &pbarp_d1d0pullpye, &b_pbarp_d1d0pullpye);
   fChain->SetBranchAddress("pbarp_d1d0pullpze", &pbarp_d1d0pullpze, &b_pbarp_d1d0pullpze);
   fChain->SetBranchAddress("pbarp_d1d0pullxy", &pbarp_d1d0pullxy, &b_pbarp_d1d0pullxy);
   fChain->SetBranchAddress("pbarp_d1d0pullxz", &pbarp_d1d0pullxz, &b_pbarp_d1d0pullxz);
   fChain->SetBranchAddress("pbarp_d1d0pullyz", &pbarp_d1d0pullyz, &b_pbarp_d1d0pullyz);
   fChain->SetBranchAddress("pbarp_d1d0mct", &pbarp_d1d0mct, &b_pbarp_d1d0mct);
   fChain->SetBranchAddress("pbarp_d1d0pide", &pbarp_d1d0pide, &b_pbarp_d1d0pide);
   fChain->SetBranchAddress("pbarp_d1d0pidmu", &pbarp_d1d0pidmu, &b_pbarp_d1d0pidmu);
   fChain->SetBranchAddress("pbarp_d1d0pidpi", &pbarp_d1d0pidpi, &b_pbarp_d1d0pidpi);
   fChain->SetBranchAddress("pbarp_d1d0pidk", &pbarp_d1d0pidk, &b_pbarp_d1d0pidk);
   fChain->SetBranchAddress("pbarp_d1d0pidp", &pbarp_d1d0pidp, &b_pbarp_d1d0pidp);
   fChain->SetBranchAddress("pbarp_d1d0pidmax", &pbarp_d1d0pidmax, &b_pbarp_d1d0pidmax);
   fChain->SetBranchAddress("pbarp_d1d0pidbest", &pbarp_d1d0pidbest, &b_pbarp_d1d0pidbest);
   fChain->SetBranchAddress("pbarp_d1d0trpdg", &pbarp_d1d0trpdg, &b_pbarp_d1d0trpdg);
   fChain->SetBranchAddress("pbarp_d1d1px", &pbarp_d1d1px, &b_pbarp_d1d1px);
   fChain->SetBranchAddress("pbarp_d1d1py", &pbarp_d1d1py, &b_pbarp_d1d1py);
   fChain->SetBranchAddress("pbarp_d1d1pz", &pbarp_d1d1pz, &b_pbarp_d1d1pz);
   fChain->SetBranchAddress("pbarp_d1d1e", &pbarp_d1d1e, &b_pbarp_d1d1e);
   fChain->SetBranchAddress("pbarp_d1d1p", &pbarp_d1d1p, &b_pbarp_d1d1p);
   fChain->SetBranchAddress("pbarp_d1d1tht", &pbarp_d1d1tht, &b_pbarp_d1d1tht);
   fChain->SetBranchAddress("pbarp_d1d1phi", &pbarp_d1d1phi, &b_pbarp_d1d1phi);
   fChain->SetBranchAddress("pbarp_d1d1pt", &pbarp_d1d1pt, &b_pbarp_d1d1pt);
   fChain->SetBranchAddress("pbarp_d1d1m", &pbarp_d1d1m, &b_pbarp_d1d1m);
   fChain->SetBranchAddress("pbarp_d1d1x", &pbarp_d1d1x, &b_pbarp_d1d1x);
   fChain->SetBranchAddress("pbarp_d1d1y", &pbarp_d1d1y, &b_pbarp_d1d1y);
   fChain->SetBranchAddress("pbarp_d1d1z", &pbarp_d1d1z, &b_pbarp_d1d1z);
   fChain->SetBranchAddress("pbarp_d1d1l", &pbarp_d1d1l, &b_pbarp_d1d1l);
   fChain->SetBranchAddress("pbarp_d1d1chg", &pbarp_d1d1chg, &b_pbarp_d1d1chg);
   fChain->SetBranchAddress("pbarp_d1d1pdg", &pbarp_d1d1pdg, &b_pbarp_d1d1pdg);
   fChain->SetBranchAddress("pbarp_d1d1pxcm", &pbarp_d1d1pxcm, &b_pbarp_d1d1pxcm);
   fChain->SetBranchAddress("pbarp_d1d1pycm", &pbarp_d1d1pycm, &b_pbarp_d1d1pycm);
   fChain->SetBranchAddress("pbarp_d1d1pzcm", &pbarp_d1d1pzcm, &b_pbarp_d1d1pzcm);
   fChain->SetBranchAddress("pbarp_d1d1ecm", &pbarp_d1d1ecm, &b_pbarp_d1d1ecm);
   fChain->SetBranchAddress("pbarp_d1d1pcm", &pbarp_d1d1pcm, &b_pbarp_d1d1pcm);
   fChain->SetBranchAddress("pbarp_d1d1thtcm", &pbarp_d1d1thtcm, &b_pbarp_d1d1thtcm);
   fChain->SetBranchAddress("pbarp_d1d1phicm", &pbarp_d1d1phicm, &b_pbarp_d1d1phicm);
   fChain->SetBranchAddress("pbarp_d1d1covpxpx", &pbarp_d1d1covpxpx, &b_pbarp_d1d1covpxpx);
   fChain->SetBranchAddress("pbarp_d1d1covpxpy", &pbarp_d1d1covpxpy, &b_pbarp_d1d1covpxpy);
   fChain->SetBranchAddress("pbarp_d1d1covpxpz", &pbarp_d1d1covpxpz, &b_pbarp_d1d1covpxpz);
   fChain->SetBranchAddress("pbarp_d1d1covpxe", &pbarp_d1d1covpxe, &b_pbarp_d1d1covpxe);
   fChain->SetBranchAddress("pbarp_d1d1covpypy", &pbarp_d1d1covpypy, &b_pbarp_d1d1covpypy);
   fChain->SetBranchAddress("pbarp_d1d1covpypz", &pbarp_d1d1covpypz, &b_pbarp_d1d1covpypz);
   fChain->SetBranchAddress("pbarp_d1d1covpye", &pbarp_d1d1covpye, &b_pbarp_d1d1covpye);
   fChain->SetBranchAddress("pbarp_d1d1covpzpz", &pbarp_d1d1covpzpz, &b_pbarp_d1d1covpzpz);
   fChain->SetBranchAddress("pbarp_d1d1covpze", &pbarp_d1d1covpze, &b_pbarp_d1d1covpze);
   fChain->SetBranchAddress("pbarp_d1d1covee", &pbarp_d1d1covee, &b_pbarp_d1d1covee);
   fChain->SetBranchAddress("pbarp_d1d1pullpx", &pbarp_d1d1pullpx, &b_pbarp_d1d1pullpx);
   fChain->SetBranchAddress("pbarp_d1d1pullpy", &pbarp_d1d1pullpy, &b_pbarp_d1d1pullpy);
   fChain->SetBranchAddress("pbarp_d1d1pullpz", &pbarp_d1d1pullpz, &b_pbarp_d1d1pullpz);
   fChain->SetBranchAddress("pbarp_d1d1pulle", &pbarp_d1d1pulle, &b_pbarp_d1d1pulle);
   fChain->SetBranchAddress("pbarp_d1d1pullx", &pbarp_d1d1pullx, &b_pbarp_d1d1pullx);
   fChain->SetBranchAddress("pbarp_d1d1pully", &pbarp_d1d1pully, &b_pbarp_d1d1pully);
   fChain->SetBranchAddress("pbarp_d1d1pullz", &pbarp_d1d1pullz, &b_pbarp_d1d1pullz);
   fChain->SetBranchAddress("pbarp_d1d1pullpxpy", &pbarp_d1d1pullpxpy, &b_pbarp_d1d1pullpxpy);
   fChain->SetBranchAddress("pbarp_d1d1pullpxpz", &pbarp_d1d1pullpxpz, &b_pbarp_d1d1pullpxpz);
   fChain->SetBranchAddress("pbarp_d1d1pullpypz", &pbarp_d1d1pullpypz, &b_pbarp_d1d1pullpypz);
   fChain->SetBranchAddress("pbarp_d1d1pullpxe", &pbarp_d1d1pullpxe, &b_pbarp_d1d1pullpxe);
   fChain->SetBranchAddress("pbarp_d1d1pullpye", &pbarp_d1d1pullpye, &b_pbarp_d1d1pullpye);
   fChain->SetBranchAddress("pbarp_d1d1pullpze", &pbarp_d1d1pullpze, &b_pbarp_d1d1pullpze);
   fChain->SetBranchAddress("pbarp_d1d1pullxy", &pbarp_d1d1pullxy, &b_pbarp_d1d1pullxy);
   fChain->SetBranchAddress("pbarp_d1d1pullxz", &pbarp_d1d1pullxz, &b_pbarp_d1d1pullxz);
   fChain->SetBranchAddress("pbarp_d1d1pullyz", &pbarp_d1d1pullyz, &b_pbarp_d1d1pullyz);
   fChain->SetBranchAddress("pbarp_d1d1mct", &pbarp_d1d1mct, &b_pbarp_d1d1mct);
   fChain->SetBranchAddress("pbarp_d1d1pide", &pbarp_d1d1pide, &b_pbarp_d1d1pide);
   fChain->SetBranchAddress("pbarp_d1d1pidmu", &pbarp_d1d1pidmu, &b_pbarp_d1d1pidmu);
   fChain->SetBranchAddress("pbarp_d1d1pidpi", &pbarp_d1d1pidpi, &b_pbarp_d1d1pidpi);
   fChain->SetBranchAddress("pbarp_d1d1pidk", &pbarp_d1d1pidk, &b_pbarp_d1d1pidk);
   fChain->SetBranchAddress("pbarp_d1d1pidp", &pbarp_d1d1pidp, &b_pbarp_d1d1pidp);
   fChain->SetBranchAddress("pbarp_d1d1pidmax", &pbarp_d1d1pidmax, &b_pbarp_d1d1pidmax);
   fChain->SetBranchAddress("pbarp_d1d1pidbest", &pbarp_d1d1pidbest, &b_pbarp_d1d1pidbest);
   fChain->SetBranchAddress("pbarp_d1d1trpdg", &pbarp_d1d1trpdg, &b_pbarp_d1d1trpdg);
   fChain->SetBranchAddress("MC_d0px", &MC_d0px, &b_MC_d0px);
   fChain->SetBranchAddress("MC_d0py", &MC_d0py, &b_MC_d0py);
   fChain->SetBranchAddress("MC_d0pz", &MC_d0pz, &b_MC_d0pz);
   fChain->SetBranchAddress("MC_d0e", &MC_d0e, &b_MC_d0e);
   fChain->SetBranchAddress("MC_d0p", &MC_d0p, &b_MC_d0p);
   fChain->SetBranchAddress("MC_d0tht", &MC_d0tht, &b_MC_d0tht);
   fChain->SetBranchAddress("MC_d0phi", &MC_d0phi, &b_MC_d0phi);
   fChain->SetBranchAddress("MC_d0pt", &MC_d0pt, &b_MC_d0pt);
   fChain->SetBranchAddress("MC_d0m", &MC_d0m, &b_MC_d0m);
   fChain->SetBranchAddress("MC_d0x", &MC_d0x, &b_MC_d0x);
   fChain->SetBranchAddress("MC_d0y", &MC_d0y, &b_MC_d0y);
   fChain->SetBranchAddress("MC_d0z", &MC_d0z, &b_MC_d0z);
   fChain->SetBranchAddress("MC_d0l", &MC_d0l, &b_MC_d0l);
   fChain->SetBranchAddress("MC_d0chg", &MC_d0chg, &b_MC_d0chg);
   fChain->SetBranchAddress("MC_d0pdg", &MC_d0pdg, &b_MC_d0pdg);
   fChain->SetBranchAddress("MC_d0pxcm", &MC_d0pxcm, &b_MC_d0pxcm);
   fChain->SetBranchAddress("MC_d0pycm", &MC_d0pycm, &b_MC_d0pycm);
   fChain->SetBranchAddress("MC_d0pzcm", &MC_d0pzcm, &b_MC_d0pzcm);
   fChain->SetBranchAddress("MC_d0ecm", &MC_d0ecm, &b_MC_d0ecm);
   fChain->SetBranchAddress("MC_d0pcm", &MC_d0pcm, &b_MC_d0pcm);
   fChain->SetBranchAddress("MC_d0thtcm", &MC_d0thtcm, &b_MC_d0thtcm);
   fChain->SetBranchAddress("MC_d0phicm", &MC_d0phicm, &b_MC_d0phicm);
   fChain->SetBranchAddress("MC_d1px", &MC_d1px, &b_MC_d1px);
   fChain->SetBranchAddress("MC_d1py", &MC_d1py, &b_MC_d1py);
   fChain->SetBranchAddress("MC_d1pz", &MC_d1pz, &b_MC_d1pz);
   fChain->SetBranchAddress("MC_d1e", &MC_d1e, &b_MC_d1e);
   fChain->SetBranchAddress("MC_d1p", &MC_d1p, &b_MC_d1p);
   fChain->SetBranchAddress("MC_d1tht", &MC_d1tht, &b_MC_d1tht);
   fChain->SetBranchAddress("MC_d1phi", &MC_d1phi, &b_MC_d1phi);
   fChain->SetBranchAddress("MC_d1pt", &MC_d1pt, &b_MC_d1pt);
   fChain->SetBranchAddress("MC_d1m", &MC_d1m, &b_MC_d1m);
   fChain->SetBranchAddress("MC_d1x", &MC_d1x, &b_MC_d1x);
   fChain->SetBranchAddress("MC_d1y", &MC_d1y, &b_MC_d1y);
   fChain->SetBranchAddress("MC_d1z", &MC_d1z, &b_MC_d1z);
   fChain->SetBranchAddress("MC_d1l", &MC_d1l, &b_MC_d1l);
   fChain->SetBranchAddress("MC_d1chg", &MC_d1chg, &b_MC_d1chg);
   fChain->SetBranchAddress("MC_d1pdg", &MC_d1pdg, &b_MC_d1pdg);
   fChain->SetBranchAddress("MC_d1pxcm", &MC_d1pxcm, &b_MC_d1pxcm);
   fChain->SetBranchAddress("MC_d1pycm", &MC_d1pycm, &b_MC_d1pycm);
   fChain->SetBranchAddress("MC_d1pzcm", &MC_d1pzcm, &b_MC_d1pzcm);
   fChain->SetBranchAddress("MC_d1ecm", &MC_d1ecm, &b_MC_d1ecm);
   fChain->SetBranchAddress("MC_d1pcm", &MC_d1pcm, &b_MC_d1pcm);
   fChain->SetBranchAddress("MC_d1thtcm", &MC_d1thtcm, &b_MC_d1thtcm);
   fChain->SetBranchAddress("MC_d1phicm", &MC_d1phicm, &b_MC_d1phicm);
   fChain->SetBranchAddress("MC_d0d0px", &MC_d0d0px, &b_MC_d0d0px);
   fChain->SetBranchAddress("MC_d0d0py", &MC_d0d0py, &b_MC_d0d0py);
   fChain->SetBranchAddress("MC_d0d0pz", &MC_d0d0pz, &b_MC_d0d0pz);
   fChain->SetBranchAddress("MC_d0d0e", &MC_d0d0e, &b_MC_d0d0e);
   fChain->SetBranchAddress("MC_d0d0p", &MC_d0d0p, &b_MC_d0d0p);
   fChain->SetBranchAddress("MC_d0d0tht", &MC_d0d0tht, &b_MC_d0d0tht);
   fChain->SetBranchAddress("MC_d0d0phi", &MC_d0d0phi, &b_MC_d0d0phi);
   fChain->SetBranchAddress("MC_d0d0pt", &MC_d0d0pt, &b_MC_d0d0pt);
   fChain->SetBranchAddress("MC_d0d0m", &MC_d0d0m, &b_MC_d0d0m);
   fChain->SetBranchAddress("MC_d0d0x", &MC_d0d0x, &b_MC_d0d0x);
   fChain->SetBranchAddress("MC_d0d0y", &MC_d0d0y, &b_MC_d0d0y);
   fChain->SetBranchAddress("MC_d0d0z", &MC_d0d0z, &b_MC_d0d0z);
   fChain->SetBranchAddress("MC_d0d0l", &MC_d0d0l, &b_MC_d0d0l);
   fChain->SetBranchAddress("MC_d0d0chg", &MC_d0d0chg, &b_MC_d0d0chg);
   fChain->SetBranchAddress("MC_d0d0pdg", &MC_d0d0pdg, &b_MC_d0d0pdg);
   fChain->SetBranchAddress("MC_d0d0pxcm", &MC_d0d0pxcm, &b_MC_d0d0pxcm);
   fChain->SetBranchAddress("MC_d0d0pycm", &MC_d0d0pycm, &b_MC_d0d0pycm);
   fChain->SetBranchAddress("MC_d0d0pzcm", &MC_d0d0pzcm, &b_MC_d0d0pzcm);
   fChain->SetBranchAddress("MC_d0d0ecm", &MC_d0d0ecm, &b_MC_d0d0ecm);
   fChain->SetBranchAddress("MC_d0d0pcm", &MC_d0d0pcm, &b_MC_d0d0pcm);
   fChain->SetBranchAddress("MC_d0d0thtcm", &MC_d0d0thtcm, &b_MC_d0d0thtcm);
   fChain->SetBranchAddress("MC_d0d0phicm", &MC_d0d0phicm, &b_MC_d0d0phicm);
   fChain->SetBranchAddress("MC_d0d1px", &MC_d0d1px, &b_MC_d0d1px);
   fChain->SetBranchAddress("MC_d0d1py", &MC_d0d1py, &b_MC_d0d1py);
   fChain->SetBranchAddress("MC_d0d1pz", &MC_d0d1pz, &b_MC_d0d1pz);
   fChain->SetBranchAddress("MC_d0d1e", &MC_d0d1e, &b_MC_d0d1e);
   fChain->SetBranchAddress("MC_d0d1p", &MC_d0d1p, &b_MC_d0d1p);
   fChain->SetBranchAddress("MC_d0d1tht", &MC_d0d1tht, &b_MC_d0d1tht);
   fChain->SetBranchAddress("MC_d0d1phi", &MC_d0d1phi, &b_MC_d0d1phi);
   fChain->SetBranchAddress("MC_d0d1pt", &MC_d0d1pt, &b_MC_d0d1pt);
   fChain->SetBranchAddress("MC_d0d1m", &MC_d0d1m, &b_MC_d0d1m);
   fChain->SetBranchAddress("MC_d0d1x", &MC_d0d1x, &b_MC_d0d1x);
   fChain->SetBranchAddress("MC_d0d1y", &MC_d0d1y, &b_MC_d0d1y);
   fChain->SetBranchAddress("MC_d0d1z", &MC_d0d1z, &b_MC_d0d1z);
   fChain->SetBranchAddress("MC_d0d1l", &MC_d0d1l, &b_MC_d0d1l);
   fChain->SetBranchAddress("MC_d0d1chg", &MC_d0d1chg, &b_MC_d0d1chg);
   fChain->SetBranchAddress("MC_d0d1pdg", &MC_d0d1pdg, &b_MC_d0d1pdg);
   fChain->SetBranchAddress("MC_d0d1pxcm", &MC_d0d1pxcm, &b_MC_d0d1pxcm);
   fChain->SetBranchAddress("MC_d0d1pycm", &MC_d0d1pycm, &b_MC_d0d1pycm);
   fChain->SetBranchAddress("MC_d0d1pzcm", &MC_d0d1pzcm, &b_MC_d0d1pzcm);
   fChain->SetBranchAddress("MC_d0d1ecm", &MC_d0d1ecm, &b_MC_d0d1ecm);
   fChain->SetBranchAddress("MC_d0d1pcm", &MC_d0d1pcm, &b_MC_d0d1pcm);
   fChain->SetBranchAddress("MC_d0d1thtcm", &MC_d0d1thtcm, &b_MC_d0d1thtcm);
   fChain->SetBranchAddress("MC_d0d1phicm", &MC_d0d1phicm, &b_MC_d0d1phicm);
   fChain->SetBranchAddress("MC_d1d0px", &MC_d1d0px, &b_MC_d1d0px);
   fChain->SetBranchAddress("MC_d1d0py", &MC_d1d0py, &b_MC_d1d0py);
   fChain->SetBranchAddress("MC_d1d0pz", &MC_d1d0pz, &b_MC_d1d0pz);
   fChain->SetBranchAddress("MC_d1d0e", &MC_d1d0e, &b_MC_d1d0e);
   fChain->SetBranchAddress("MC_d1d0p", &MC_d1d0p, &b_MC_d1d0p);
   fChain->SetBranchAddress("MC_d1d0tht", &MC_d1d0tht, &b_MC_d1d0tht);
   fChain->SetBranchAddress("MC_d1d0phi", &MC_d1d0phi, &b_MC_d1d0phi);
   fChain->SetBranchAddress("MC_d1d0pt", &MC_d1d0pt, &b_MC_d1d0pt);
   fChain->SetBranchAddress("MC_d1d0m", &MC_d1d0m, &b_MC_d1d0m);
   fChain->SetBranchAddress("MC_d1d0x", &MC_d1d0x, &b_MC_d1d0x);
   fChain->SetBranchAddress("MC_d1d0y", &MC_d1d0y, &b_MC_d1d0y);
   fChain->SetBranchAddress("MC_d1d0z", &MC_d1d0z, &b_MC_d1d0z);
   fChain->SetBranchAddress("MC_d1d0l", &MC_d1d0l, &b_MC_d1d0l);
   fChain->SetBranchAddress("MC_d1d0chg", &MC_d1d0chg, &b_MC_d1d0chg);
   fChain->SetBranchAddress("MC_d1d0pdg", &MC_d1d0pdg, &b_MC_d1d0pdg);
   fChain->SetBranchAddress("MC_d1d0pxcm", &MC_d1d0pxcm, &b_MC_d1d0pxcm);
   fChain->SetBranchAddress("MC_d1d0pycm", &MC_d1d0pycm, &b_MC_d1d0pycm);
   fChain->SetBranchAddress("MC_d1d0pzcm", &MC_d1d0pzcm, &b_MC_d1d0pzcm);
   fChain->SetBranchAddress("MC_d1d0ecm", &MC_d1d0ecm, &b_MC_d1d0ecm);
   fChain->SetBranchAddress("MC_d1d0pcm", &MC_d1d0pcm, &b_MC_d1d0pcm);
   fChain->SetBranchAddress("MC_d1d0thtcm", &MC_d1d0thtcm, &b_MC_d1d0thtcm);
   fChain->SetBranchAddress("MC_d1d0phicm", &MC_d1d0phicm, &b_MC_d1d0phicm);
   fChain->SetBranchAddress("MC_d1d1px", &MC_d1d1px, &b_MC_d1d1px);
   fChain->SetBranchAddress("MC_d1d1py", &MC_d1d1py, &b_MC_d1d1py);
   fChain->SetBranchAddress("MC_d1d1pz", &MC_d1d1pz, &b_MC_d1d1pz);
   fChain->SetBranchAddress("MC_d1d1e", &MC_d1d1e, &b_MC_d1d1e);
   fChain->SetBranchAddress("MC_d1d1p", &MC_d1d1p, &b_MC_d1d1p);
   fChain->SetBranchAddress("MC_d1d1tht", &MC_d1d1tht, &b_MC_d1d1tht);
   fChain->SetBranchAddress("MC_d1d1phi", &MC_d1d1phi, &b_MC_d1d1phi);
   fChain->SetBranchAddress("MC_d1d1pt", &MC_d1d1pt, &b_MC_d1d1pt);
   fChain->SetBranchAddress("MC_d1d1m", &MC_d1d1m, &b_MC_d1d1m);
   fChain->SetBranchAddress("MC_d1d1x", &MC_d1d1x, &b_MC_d1d1x);
   fChain->SetBranchAddress("MC_d1d1y", &MC_d1d1y, &b_MC_d1d1y);
   fChain->SetBranchAddress("MC_d1d1z", &MC_d1d1z, &b_MC_d1d1z);
   fChain->SetBranchAddress("MC_d1d1l", &MC_d1d1l, &b_MC_d1d1l);
   fChain->SetBranchAddress("MC_d1d1chg", &MC_d1d1chg, &b_MC_d1d1chg);
   fChain->SetBranchAddress("MC_d1d1pdg", &MC_d1d1pdg, &b_MC_d1d1pdg);
   fChain->SetBranchAddress("MC_d1d1pxcm", &MC_d1d1pxcm, &b_MC_d1d1pxcm);
   fChain->SetBranchAddress("MC_d1d1pycm", &MC_d1d1pycm, &b_MC_d1d1pycm);
   fChain->SetBranchAddress("MC_d1d1pzcm", &MC_d1d1pzcm, &b_MC_d1d1pzcm);
   fChain->SetBranchAddress("MC_d1d1ecm", &MC_d1d1ecm, &b_MC_d1d1ecm);
   fChain->SetBranchAddress("MC_d1d1pcm", &MC_d1d1pcm, &b_MC_d1d1pcm);
   fChain->SetBranchAddress("MC_d1d1thtcm", &MC_d1d1thtcm, &b_MC_d1d1thtcm);
   fChain->SetBranchAddress("MC_d1d1phicm", &MC_d1d1phicm, &b_MC_d1d1phicm);
   Notify();
}

Bool_t ntpBestPbarP::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ntpBestPbarP::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ntpBestPbarP::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ntpBestPbarP_cxx
