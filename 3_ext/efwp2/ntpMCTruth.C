#define ntpMCTruth_cxx
#include "ntpMCTruth.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void ntpMCTruth::Loop()
{
//   In a ROOT session, you can do:
//      root> .L ntpMCTruth.C
//      root> ntpMCTruth t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
      
        //*** ---------------------
        //*** Added by ADEEL (START)
        //***


        /* ****************************************************
        *     ...	      pbarp
        *     d0          lambdabar
        *     d0d0        pbar (anti-proton)
        *     d0d1        piplus (pi+)
        *     d1          lambda
        *     d1d0        p (proton)
        *     d1d1        piminus (pi-)
        * *****************************************************/
        
        
        //Momentum v.s. Theta Dist. (Rad. to Deg.)
        hpbar_MomTht->Fill(MC_d0d0p,MC_d0d0tht*TMath::RadToDeg());
        hpiplus_MomTht->Fill(MC_d0d1p,MC_d0d1tht*TMath::RadToDeg());
        hp_MomTht->Fill(MC_d1d0p,MC_d1d0tht*TMath::RadToDeg());
        hpiminus_MomTht->Fill(MC_d1d1p,MC_d1d1tht*TMath::RadToDeg());
        
        //z-profile
        hMC_d0z->Fill(MC_d0z);                        // lam0bar
        hMC_d1z->Fill(MC_d1z);                        // lam0
        
        //2D Scatter Plots
        hMC_d0z_d1z->Fill(MC_d1z, MC_d0z);            // lam0bar vs lam0 (PVZ)
        hMC_d0d0z_d0d1z->Fill(MC_d0d0z, MC_d0d1z);    // pbar vs pi+ (PVZ)

        
        
        
        //*** 
        //*** Added by ADEEL (END)
        //*** ---------------------
        
   }
}
