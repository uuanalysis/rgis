#define ntpBestPbarP_cxx
#include "ntpBestPbarP.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void ntpBestPbarP::Loop()
{
//   In a ROOT session, you can do:
//      root> .L ntpBestPbarP.C
//      root> ntpBestPbarP t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
    if (fChain == 0) return;

    Long64_t nentries = fChain->GetEntriesFast();

    Long64_t nbytes = 0, nb = 0;
    for (Long64_t jentry=0; jentry<nentries;jentry++) {
        Long64_t ientry = LoadTree(jentry);
        if (ientry < 0) break;
        nb = fChain->GetEntry(jentry);   nbytes += nb;
        // if (Cut(ientry) < 0) continue;
        
        
        //*** ---------------------
        //*** Added by ADEEL (START)
        //***

        /* ****************************************************
        *     ...         pbarp
        *     d0          lambdabar
        *     d0d0        pbar (anti-proton)
        *     d0d1        piplus (pi+)
        *     d1          lambda
        *     d1d0        p (proton)
        *     d1d1        piminus (pi-)
        * *****************************************************/
        
        
        // ----- 2D Scatter Plots
        h_m_llbar->Fill(pbarp_d1m, pbarp_d0m);                  // lam0, lam0bar (Mass)
        h_dvxy_lam0->Fill(pbarp_d1d0x, pbarp_d1d0y);            // lam0: DVx
        h_dvxy_lam0bar->Fill(pbarp_d0d0x, pbarp_d0d0y);         // lam0bar: DVy
        
        
        //*** 
        //----- Kinematics (Switch b/w Pre-selection & Final Selection)
        //***
        
        /*
        // ----- |p| vs theta
        // ----- RECO
        hpbar_MomTht->Fill(pbarp_d0d0p,pbarp_d0d0tht*TMath::RadToDeg());
        hpiplus_MomTht->Fill(pbarp_d0d1p,pbarp_d0d1tht*TMath::RadToDeg());
        hp_MomTht->Fill(pbarp_d1d0p,pbarp_d1d0tht*TMath::RadToDeg());
        hpiminus_MomTht->Fill(pbarp_d1d1p,pbarp_d1d1tht*TMath::RadToDeg());
        
        // ----- MC
        hpbar_McMomTht->Fill(MC_d0d0p,MC_d0d0tht*TMath::RadToDeg());
        hpiplus_McMomTht->Fill(MC_d0d1p,MC_d0d1tht*TMath::RadToDeg());
        hp_McMomTht->Fill(MC_d1d0p,MC_d1d0tht*TMath::RadToDeg());
        hpiminus_McMomTht->Fill(MC_d1d1p,MC_d1d1tht*TMath::RadToDeg());
        
        // ----- pz vs pt
        // ----- RECO
        hlam0_PzPt->Fill(pbarp_d1pz, pbarp_d1pt);  // lam0
        hlam0bar_PzPt->Fill(pbarp_d0pz, pbarp_d0pt);  // lam0bar
        
        // ----- MC
        hlam0_McPzPt->Fill(MC_d1pz, MC_d1pt);  // lam0
        hlam0bar_McPzPt->Fill(MC_d0pz, MC_d0pt);  // lam0bar
        */
        
        //***-----------------------------------------------------
        //---                      Pre-selection
        //***-----------------------------------------------------
        
        // ----- Mass, Chi2, Prob, ndf
        hlam0_m->Fill(pbarp_d1m);                               // lam0
        hlam0bar_m->Fill(pbarp_d0m);                            // lam0bar
        hpbarp_m->Fill(pbarp_m);                                // pbarp
        hpbarp_chi2->Fill(FourMomFit_chisq);                    // pbarp
        hpbarp_prob->Fill(FourMomFit_prob);                     // pbarp
        hpbarp_ndf->Fill(FourMomFit_ndf);                       // pbarp
        h_dvzsum_llbar->Fill((pbarp_d1d0z+pbarp_d0d0z));        // lam0, lam0bar (DVZ)
        
        
        //***-----------------------------------------------------
        //---                      Final Selection
        //***-----------------------------------------------------
        
        // ----- Cut#1: Chi2 on 4C Fit(Momentum)
        if (FourMomFit_chisq > 100) continue;
        hlam0_m_cut1->Fill(pbarp_d1m);
        hlam0bar_m_cut1->Fill(pbarp_d0m);
        hpbarp_m_cut1->Fill(pbarp_m);

        // ----- Cut#2: Mass Constraint (w/ 500 bins, cut1)
        if (TMath::Abs((pbarp_d1m - fMass0_lam)) > 5*4.761*TMath::Power(10,-3)) continue; //lamb0
        if (TMath::Abs((pbarp_d0m - fMass0_lam)) > 5*4.725*TMath::Power(10,-3)) continue; //lam0bar
        hlam0_m_cut2->Fill(pbarp_d1m);
        hlam0bar_m_cut2->Fill(pbarp_d0m);
        hpbarp_m_cut2->Fill(pbarp_m);

        // ----- Cut#3: z-axis Constraint
        if ((pbarp_d0d0z + pbarp_d1d0z) < 2.0) continue;
        hlam0_m_cut3->Fill(pbarp_d1m);
        hlam0bar_m_cut3->Fill(pbarp_d0m);
        hpbarp_m_cut3->Fill(pbarp_m);
        hlam0_thtcm->Fill(TMath::Cos(pbarp_d1thtcm));  // lam0
        hlam0bar_thtcm->Fill(TMath::Cos(pbarp_d0thtcm));  // lam0bar
        
        // -------------------------------------------------------
        //                    More Plots for Final Selection
        // -------------------------------------------------------
        
        
        
        //*** 
        //----- Kinematics


        // |p| vs theta
        hpbar_McMomTht->Fill(MC_d0d0p,MC_d0d0tht*TMath::RadToDeg());
        hpiplus_McMomTht->Fill(MC_d0d1p,MC_d0d1tht*TMath::RadToDeg());
        hp_McMomTht->Fill(MC_d1d0p,MC_d1d0tht*TMath::RadToDeg());
        hpiminus_McMomTht->Fill(MC_d1d1p,MC_d1d1tht*TMath::RadToDeg());
        
        hpbar_MomTht->Fill(pbarp_d0d0p,pbarp_d0d0tht*TMath::RadToDeg());
        hpiplus_MomTht->Fill(pbarp_d0d1p,pbarp_d0d1tht*TMath::RadToDeg());
        hp_MomTht->Fill(pbarp_d1d0p,pbarp_d1d0tht*TMath::RadToDeg());
        hpiminus_MomTht->Fill(pbarp_d1d1p,pbarp_d1d1tht*TMath::RadToDeg());
        
        // pz vs pt
        hlam0_McPzPt->Fill(MC_d1pz, MC_d1pt);
        hlam0bar_McPzPt->Fill(MC_d0pz, MC_d0pt);

        hlam0_PzPt->Fill(pbarp_d1pz, pbarp_d1pt);
        hlam0bar_PzPt->Fill(pbarp_d0pz, pbarp_d0pt);
        
        
        // ptheta, pzpt for all
        McMomThtAll->Fill(MC_d0p,MC_d0tht*TMath::RadToDeg());
        McMomThtAll->Fill(MC_d1p,MC_d1tht*TMath::RadToDeg());
        McMomThtAll->Fill(MC_d0d0p,MC_d0d0tht*TMath::RadToDeg());
        McMomThtAll->Fill(MC_d0d1p,MC_d0d1tht*TMath::RadToDeg());
        McMomThtAll->Fill(MC_d1d0p,MC_d1d0tht*TMath::RadToDeg());
        McMomThtAll->Fill(MC_d1d1p,MC_d1d1tht*TMath::RadToDeg());
        
        MomThtAll->Fill(pbarp_d0p,pbarp_d0tht*TMath::RadToDeg());
        MomThtAll->Fill(pbarp_d1p,pbarp_d1tht*TMath::RadToDeg());
        MomThtAll->Fill(pbarp_d0d0p,pbarp_d0d0tht*TMath::RadToDeg());
        MomThtAll->Fill(pbarp_d0d1p,pbarp_d0d1tht*TMath::RadToDeg());
        MomThtAll->Fill(pbarp_d1d0p,pbarp_d1d0tht*TMath::RadToDeg());
        MomThtAll->Fill(pbarp_d1d1p,pbarp_d1d1tht*TMath::RadToDeg());
        
        McPzPtAll->Fill(MC_d0pz,MC_d0pt);
        McPzPtAll->Fill(MC_d1pz,MC_d1pt);
        McPzPtAll->Fill(MC_d0d0pz,MC_d0d0pt);
        McPzPtAll->Fill(MC_d0d1pz,MC_d0d1pt);
        McPzPtAll->Fill(MC_d1d0pz,MC_d1d0pt);
        McPzPtAll->Fill(MC_d1d1p,MC_d1d1pt);
        
        PzPtAll->Fill(pbarp_d0pz,pbarp_d0pt);
        PzPtAll->Fill(pbarp_d1pz,pbarp_d1pt);
        PzPtAll->Fill(pbarp_d0d0pz,pbarp_d0d0pt);
        PzPtAll->Fill(pbarp_d0d1pz,pbarp_d0d1pt);
        PzPtAll->Fill(pbarp_d1d0pz,pbarp_d1d0pt);
        PzPtAll->Fill(pbarp_d1d1p,pbarp_d1d1pt);
        
        MczRAll->Fill(MC_d0d0z,TMath::Sqrt(MC_d0d0x*MC_d0d0x+MC_d0d0y*MC_d0d0y));
        zRAll->Fill(pbarp_d1d0z,TMath::Sqrt(pbarp_d1d0x*pbarp_d1d0x+pbarp_d1d0y*pbarp_d1d0y));
                
        //----- Kinematics
        //***
        
        
        
        
        //*** 
        //----- Histograms: Single Canvas
        //***
        
        //pbarp, lam0, pi-, p
        /*
        hpbarp1_prob->Fill(FourMomFit_prob);
        hpbarp1_chi2->Fill(FourMomFit_chisq);
        hlam0_m->Fill(pbarp_d0m);
        hlam0_vtx_pullx->Fill(pbarp_d0d0pullx);
        hlam0_vtx_pully->Fill(pbarp_d0d0pully);
        hlam0_vtx_pullz->Fill(pbarp_d0d0pullz);
        hp_tht->Fill(pbarp_d0d0tht*180./TMath::Pi());
        hpiminus_tht->Fill(pbarp_d0d1tht*180./TMath::Pi());
        */
        
        //pbarp, lam0bar, pi+, pbar
        /*
        hpbarp2_prob->Fill(FourMomFit_prob);
        hpbarp2_chi2->Fill(FourMomFit_chisq);
        hlam0bar_m->Fill(pbarp_d1m);
        hlam0bar_vtx_pullx->Fill(pbarp_d1d0pullx);
        hlam0bar_vtx_pully->Fill(pbarp_d1d0pully);
        hlam0bar_vtx_pullz->Fill(pbarp_d1d0pullz);
        hpbar_tht->Fill(pbarp_d1d0tht*180./TMath::Pi());
        hpiplus_tht->Fill(pbarp_d1d1tht*180./TMath::Pi());
        */
        
        //*** 
        //*** Added by ADEEL (END)
        //*** ---------------------
   }
}
