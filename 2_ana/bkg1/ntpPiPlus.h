//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Aug 10 16:11:18 2021 by ROOT version 6.20/08
// from TTree ntpPiPlus/PiPlus Info.
// found on file: bkg1-PiPlus.root
//////////////////////////////////////////////////////////

#ifndef ntpPiPlus_h
#define ntpPiPlus_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class ntpPiPlus {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         piplus_px;
   Float_t         piplus_py;
   Float_t         piplus_pz;
   Float_t         piplus_e;
   Float_t         piplus_p;
   Float_t         piplus_tht;
   Float_t         piplus_phi;
   Float_t         piplus_pt;
   Float_t         piplus_m;
   Float_t         piplus_x;
   Float_t         piplus_y;
   Float_t         piplus_z;
   Float_t         piplus_l;
   Float_t         piplus_chg;
   Float_t         piplus_pdg;

   // List of branches
   TBranch        *b_piplus_px;   //!
   TBranch        *b_piplus_py;   //!
   TBranch        *b_piplus_pz;   //!
   TBranch        *b_piplus_e;   //!
   TBranch        *b_piplus_p;   //!
   TBranch        *b_piplus_tht;   //!
   TBranch        *b_piplus_phi;   //!
   TBranch        *b_piplus_pt;   //!
   TBranch        *b_piplus_m;   //!
   TBranch        *b_piplus_x;   //!
   TBranch        *b_piplus_y;   //!
   TBranch        *b_piplus_z;   //!
   TBranch        *b_piplus_l;   //!
   TBranch        *b_piplus_chg;   //!
   TBranch        *b_piplus_pdg;   //!

   ntpPiPlus(TTree *tree=0);
   virtual ~ntpPiPlus();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   TH2F *hpiplus_MomTht;
   TH2F *hpiplus_PzPt;
   TH2F *hpiplus_xy;
   TH1F *hpiplus_mass;
   
   TH2F *hpiplus_zR;         // l = R = Perp()
   TH1F *hpiplus_r;          // r = sqrt(x^2 + y^2)
   
   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
};

#endif

#ifdef ntpPiPlus_cxx
ntpPiPlus::ntpPiPlus(TTree *tree) : fChain(0) 
{

    //*** ---------------------
    //*** Added by ADEEL (START)
    //***
    
    TString path = "";
    TFile* hfile = TFile::Open(path+"ntpPiPlus.root", "RECREATE");
    hpiplus_MomTht = new TH2F("hpiplus_MomTht","|p| vs #theta dist. (piplus);|p| [GeV/c];#theta [Deg.]",500,0,1.65,500,0,180);
    hpiplus_PzPt = new TH2F("hpiplus_PzPt","p_{z} vs p_{t} dist. (piplus);p_{z} [GeV/c];p_{t} [GeV/c]",500,-1.65,1.65,500,0,1.65);
    hpiplus_xy = new TH2F("hpiplus_xy","x vs y dist. (piplus);x [cm];y [cm]",500,-100,100,500,-100,100);
    hpiplus_mass = new TH1F("hpiplus_mass","mass dist. (piplus);m(p) /GeV/c^{2};Entries /GeV/c^{2}",500,0,0.5);
    
    hpiplus_zR = new TH2F("hpiplus_zR","z vs r dist. (piplus);z [cm];R [cm]",500,-100,100,500,0,50);
    hpiplus_r = new TH1F("hpiplus_r","r dist. (piplus);r [cm]",500,0,50);
    
    //*** 
    //*** Added by ADEEL (END)
    //*** ---------------------
    
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("bkg1-PiPlus.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("bkg1-PiPlus.root");
      }
      f->GetObject("ntpPiPlus",tree);

   }
   Init(tree);
   
   
   //*** ---------------------
   //*** Added by ADEEL (START)
   //***
   
   // Call Loop() 
   Loop();

   //Save to ROOT File
   hfile->Write();
   hfile->Close();

   //*** 
   //*** Added by ADEEL (END)
   //*** ---------------------
}

ntpPiPlus::~ntpPiPlus()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ntpPiPlus::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ntpPiPlus::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ntpPiPlus::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("piplus_px", &piplus_px, &b_piplus_px);
   fChain->SetBranchAddress("piplus_py", &piplus_py, &b_piplus_py);
   fChain->SetBranchAddress("piplus_pz", &piplus_pz, &b_piplus_pz);
   fChain->SetBranchAddress("piplus_e", &piplus_e, &b_piplus_e);
   fChain->SetBranchAddress("piplus_p", &piplus_p, &b_piplus_p);
   fChain->SetBranchAddress("piplus_tht", &piplus_tht, &b_piplus_tht);
   fChain->SetBranchAddress("piplus_phi", &piplus_phi, &b_piplus_phi);
   fChain->SetBranchAddress("piplus_pt", &piplus_pt, &b_piplus_pt);
   fChain->SetBranchAddress("piplus_m", &piplus_m, &b_piplus_m);
   fChain->SetBranchAddress("piplus_x", &piplus_x, &b_piplus_x);
   fChain->SetBranchAddress("piplus_y", &piplus_y, &b_piplus_y);
   fChain->SetBranchAddress("piplus_z", &piplus_z, &b_piplus_z);
   fChain->SetBranchAddress("piplus_l", &piplus_l, &b_piplus_l);
   fChain->SetBranchAddress("piplus_chg", &piplus_chg, &b_piplus_chg);
   fChain->SetBranchAddress("piplus_pdg", &piplus_pdg, &b_piplus_pdg);
   Notify();
}

Bool_t ntpPiPlus::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ntpPiPlus::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ntpPiPlus::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ntpPiPlus_cxx
