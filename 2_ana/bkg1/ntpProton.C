#define ntpProton_cxx
#include "ntpProton.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void ntpProton::Loop()
{
//   In a ROOT session, you can do:
//      root> .L ntpProton.C
//      root> ntpProton t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;
      
      //*** ---------------------
      //*** Added by ADEEL (START)
      //***
      
      hp_MomTht->Fill(proton_p,proton_tht*TMath::RadToDeg());
      hp_PzPt->Fill(proton_pz,proton_pt);
      hp_xy->Fill(proton_x,proton_y);
      hp_mass->Fill(proton_m);
      
      hp_zR->Fill(proton_z,TMath::Sqrt(proton_x*proton_x+proton_y*proton_y));
      hp_r->Fill(TMath::Sqrt(proton_x*proton_x+proton_y*proton_y));
      
      //*** 
      //*** Added by ADEEL (END)
      //*** ---------------------
      
   }
}
