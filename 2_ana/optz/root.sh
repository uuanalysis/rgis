#!/bin/sh

if [ $# -lt 1 ]; then
  echo -e "\nMacro File is Necessary.\n"
  echo -e "USAGE: ./root.sh <macro.C>"
  exit 1
fi

FILE=$1
CONTAINER=$HOME/fair/pandaroot.sif
# singularity exec $CONTAINER root -l -b -q $FILE && exit
# singularity run $CONTAINER -c "root -l -b -q $FILE" && exit


for chi2 in $(seq 0 5 200)
do
  singularity run $CONTAINER -c "root -l -q -b $FILE\($chi2\)"
done
