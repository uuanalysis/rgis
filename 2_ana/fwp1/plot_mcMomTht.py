import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import uproot


# Plot 2D Histogram
def plot_2d_histogram(x, y, bins=100, range=None, cmap='viridis', norm=None, colorbar=True,
                      banner=None, xlabel=None, ylabel=None, title=None, figsize=(10,8), 
                      savefig=False, filename="2d_histogram.png", dpi=300, **kwargs):
    """
    Plot a 2D histogram with a color bar.

    Parameters:
        x (array-like): Data for the x-axis.
        y (array-like): Data for the y-axis.
        bins (int or [int, int]): Number of bins or [bins_x, bins_y]. Default is 100.
        range ([[xmin, xmax], [ymin, ymax]]): Range for the bins. Default is None.
        cmap (str or Colormap): Colormap to use. Default is 'viridis'.
        norm (Normalize): Normalization for the colormap (e.g., LogNorm). Default is None.
        colorbar (bool): Whether to add a color bar. Default is True.
        xlabel (str): Label for the x-axis. Default is None.
        ylabel (str): Label for the y-axis. Default is None.
        title (str): Title of the plot. Default is None.
        figsize (tuple): Figure size (width, height). Default is (10, 8).
        savefig (bool): Whether to save the figure. Default is False.
        filename (str): Name of the file to save. Default is "2d_histogram.png".
        dpi (int): DPI for saving the figure. Default is 300.
        **kwargs: Additional keyword arguments passed to `plt.hist2d`.
    """
    plt.close('all')
    
    # Create the figure and axis
    fig, ax = plt.subplots(figsize=figsize)

    # Plot the 2D histogram
    H, xedges, yedges, image = ax.hist2d(x, y, bins=bins, range=range, cmap=cmap, norm=norm, **kwargs)
        
    # Handle Colorbar
    if colorbar:
        cbar = plt.colorbar(image, ax=ax, pad=0., shrink=1.0)
        cbar.ax.tick_params(labelsize=16)

    # Handle Labels
    if xlabel:
        ax.set_xlabel(xlabel, fontsize=18)
    if ylabel:
        ax.set_ylabel(ylabel, fontsize=18)
    if title:
        ax.set_title(title, fontsize=18)

    # Handle Ticks
    ax.tick_params(axis='both', which='major', labelsize=16)
    ax.tick_params(axis='both', which='minor', labelsize=16)
    
    # Handle Annotations
    annotations = [
        (1.1, 12, r"$\Lambda, \bar{\Lambda}$", 25, "grey"),
        (0.15, 80, r"$\pi^{-}, \pi^{+}$", 25, "grey"),
        (0.6, 40, r"$p, \bar{p}$", 25, "grey"),
    ]
    
    for x, y, text, size, *color in annotations:
        ax.text(x, y, text, fontsize=size, fontweight='bold', color=color[0] if color else 'black')
        
    # Handle Lines
    lines = [
        (-5, 20, 1.1, 20), 
        (-5, 180, 1.1, 180),
    ]
    for x1, y1, x2, y2 in lines:
        # ax.plot([x1, x2], [y1, y2], 'k--', linewidth=1)
        pass
        
    # Handle Banner
    if banner is not None:
        try:
            import atlasify as atl
            atl.atlasify(
                "Simulation",           # Main Text
                banner,                 # Sub Text
                font_size=18,           # Main Badge
                label_font_size=18,     # Text After Badge
                sub_font_size=14,       # Subtext Font
                subtext_distance=0.0,   # Distance before subtext
                brand="PANDA",          # Change defaults to PANDA
                outside=False,          # Inside or Outside
                # enlarge=1.2,
            )
        except ImportError:
            print("Warning: 'atlasify' module not found. Skipping banner text.")
    
    # Save the figure if requested
    if savefig:
        fig.savefig(filename, dpi=dpi, bbox_inches='tight')
        
    return ax, fig

def main():

    # Choose Events
    n_events = 1000000
    
    # Open the ROOT file and get data as before
    file = uproot.open("fwp1-MCTruth.root")
    tree = file["ntpMCTruth;1"]

    # Extract Data (Momentum and Theta)
    mom_keys = ["MC_d0p", "MC_d1p", "MC_d0d0p", "MC_d0d1p", "MC_d1d0p", "MC_d1d1p"]  # List of keys
    mom_data = [tree[key].array(library="np")[:n_events] for key in mom_keys if key in tree.keys()]  # Ensure key exists
    mom = np.concatenate(mom_data)  # Concatenate arrays

    tht_keys = ["MC_d0tht", "MC_d1tht", "MC_d0d0tht", "MC_d0d1tht", "MC_d1d0tht", "MC_d1d1tht"]  # List of keys
    tht_data = [tree[key].array(library="np")[:n_events] * (180 / np.pi) for key in tht_keys if key in tree.keys()]    # Ensure key exists
    tht = np.concatenate(tht_data)  # Concatenate arrays
    
    # Plot 2D Histogram
    xlabel = "|P| / GeV/c"
    ylabel = r"$\theta$ / Deg."
    banner = r"Simulated: $\bar{p}p \rightarrow \bar{\Lambda} \: (\bar{p}\pi^{+}) \: \Lambda \:  (p\pi^{-})$"+"\nBeam Momentum = 1.642 GeV/c"
    
    ax, fig = plot_2d_histogram(
        x=mom, y=tht,                   # Input of x and y
        bins=[500, 500],                # Bins for x and y
        range=[[-0.03, 1.3], [0, 180]], # Range for x and y
        cmap='plasma',                  # Colormap
        norm=mcolors.LogNorm(),         # Logarithmic scaling
        banner=banner,                  # Banner Text
        xlabel=xlabel,                  # X Label
        ylabel=ylabel,                  # Y Label
        figsize=(8,6),                  # Figure size
        savefig=True,                   # Save figure
        filename="mcMomTht_1642.png"    # Figure name with type
    );

__name__ == "__main__"
main()
