#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <cmath>

TF1* dGausFit(TH1D *hist, double inner=0.1, double outer=0.01, bool autorange=kFALSE)
{
    /**
    * @brief Performes a Double Gaussian Fit
    */
    
    /*
    * Gaussian Fitting:
    *
    * The "gaus" function with three parameter (p0=const, p1=mean, p2=sigma)
    * is defined as the following:
    *
    *                 f(x) = p0*exp(-0.5*((x - p1)/p2)^2)
    *
    * In a fit, these prameters are labeled as p0, p1 and p2 in the statbox.
    */

    double parameters[6] = {0,0,0,0,0,0};
    double mean = hist->GetMean();
    
    //*** Debugging
    std::cout << "mean : " << mean << std::endl;
    std::cout << "inner: " << inner << std::endl;
    std::cout << "outer: " << outer << std::endl;
    
    if (autorange) 
    {   
        inner = fabs(mean - hist->GetXaxis()->GetXmax())/10;    // ???
        outer = fabs(mean - hist->GetXaxis()->GetXmax())/100;  // ???
    }
    
    //*** Debugging
    std::cout << "mean : " << mean << std::endl;
    std::cout << "inner: " << inner << std::endl;
    std::cout << "outer: " << outer << std::endl;
    
    std::cout << "Inner Range: (" << mean-inner << ", " << mean+inner << ")" << std::endl;
    std::cout << "Outer Range: (" << mean-outer << ", " << mean+outer << ")" << std::endl;
    std::cout << "Final Range: (" << mean-outer << ", " << mean+outer << ")" << std::endl;
    
    //*** 1st/Inner Gaussian
    TF1 *G1 = new TF1 ("G1","gaus",mean-inner,mean+inner);
    G1->SetLineColor(kMagenta);
    G1->SetLineStyle(1);
    hist->Fit(G1, "Q0R");                        // apply G1
    G1->GetParameters(&parameters[0]);                  // Get Params from G1
    
    //*** 2nd/Outer Gaussian
    TF1 *G2 = new TF1 ("G2","gaus",mean-outer,mean+outer);
    G2->SetLineColor(kCyan);
    G2->SetLineStyle(1);
    hist->Fit(G2, "Q0R");                        // apply G1
    G2->GetParameters(&parameters[3]);                  // Get Params from G2
    
    //*** 3rd/Combined Gaussian
    TF1 *G3 = new TF1 ("G3","gaus(0)+gaus(3)",mean-outer,mean+outer);
    G3->SetParameters(parameters);
    
    //*** Params (Default)
    // p0=const1, p1=mean1, p2=sigma1, p3=const2, p4=mean2, p5=sigma2
    
    //*** Params (User-defined)
    G3->SetParName(0, "Const (inner)");
    G3->SetParName(1, "Mean (inner)");
    G3->SetParName(2, "Sigma (inner)");
    G3->SetParName(3, "Const (outer)");
    G3->SetParName(4, "Mean (outer)");
    G3->SetParName(5, "Sigma (outer)");
    
    //G3->SetLineColor(hist->GetLineColor());
    G3->SetLineColor(kRed);
    //G3->SetLineWidth(hist->GetLineWidth());
    G3->SetLineStyle(1);
    hist->Fit(G3,"Q0R");
    
    return G3;
}
