#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;

void plot_recMomTht() {

    //*** Open Histogram Root File
    TFile *f = TFile::Open("ntpBestPbarP.root");
    if (!f) return;
    f->ls();

	//****************************** First histogram **************************
	TString hist_name="";
    
    //----- Kinematics (MC/REC of Final States)
    hist_name = "MomThtAll";
    //hist_name = "McMomThtAll";
    
	TH1F* h1 = (TH1F*)f->Get(hist_name);

	// Fixing title
	h1->SetTitle(" ");

	// Fixing the X axis
	h1->GetXaxis()->SetRangeUser(-0.1,1.6);
	h1->GetXaxis()->SetLabelSize(0.055);
	h1->GetXaxis()->SetTitleSize(0.06);
	h1->GetXaxis()->SetTitleOffset(0.99);
	h1->GetXaxis()->CenterTitle();
	h1->GetXaxis()->SetTitle("|P| /GeV/c");

	// Fixing the Y axis
	h1->GetYaxis()->SetRangeUser(0,180);
	h1->GetYaxis()->SetLabelSize(0.055);
	h1->GetYaxis()->SetTitleSize(0.06);
	h1->GetYaxis()->SetTitleOffset(0.9);
	h1->GetYaxis()->CenterTitle();
	h1->GetYaxis()->SetTitle("#theta [Deg.]");

	h1->SetStats(kFALSE);

	//*************************************************************************
    //                               TCanvas
    //*************************************************************************

	TCanvas * can = new TCanvas("c1","|p| vs. theta");
	can->SetCanvasSize(1200, 900);
   	can->SetWindowSize(1200, 900);
	can->SetTopMargin(0.025);
	can->SetBottomMargin(0.15);
	can->SetLeftMargin(0.15);
	can->SetLogz();

	h1->Draw("colz");

	// Drawing text
	TLatex latexText1(0.4,168,"PANDA MC Simulation");
	latexText1.SetTextFont(42);
	latexText1.SetTextSize(0.06);
	latexText1.SetTextColor(kGray+1);
	latexText1.DrawClone();
	
	TLatex latexText2(0.4,158,"p_{beam} = 1.642 GeV/c");
	latexText2.SetTextFont(62);
	latexText2.SetTextSize(0.05);
	latexText2.SetTextColor(kGray+1);
	latexText2.DrawClone();
      
	 TLatex latexText3(1.15,10,"#Lambda, #bar{#Lambda}");
	 latexText3.SetTextFont(62);
	 latexText3.SetTextSize(0.07);
	 //latexText3.SetTextColor(kGray+1);
	 latexText3.DrawClone();
	 
	 TLatex latexText4(0.15,80,"#pi^{-}, #pi^{+}");
	 latexText4.SetTextFont(62);
	 latexText4.SetTextSize(0.07);
	 //latexText4.SetTextColor(kGray+1);
	 latexText4.DrawClone();
	 
	 TLatex latexText5(0.6,40,"p, #bar{p}");
	 latexText5.SetTextFont(62);
	 latexText5.SetTextSize(0.07);
	 //latexText5.SetTextColor(kGray+1);
	 latexText5.DrawClone();
	
	can->SaveAs("ptht_rec_1642.png");
	
	can->Draw();
}
