#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

using std::cout;
using std::endl;

void plot_mcTheta() {

    //*** Open Histogram Root File
    TFile *f = TFile::Open("ntpMCTruth.root");
    if (!f) return;
    f->ls();

	//****************************** First histogram **************************
	TString hist_name="";
    
    //----- Kinematics (MC/REC of Final States)
    hist_name = "hlam0bar_thtcm";
    
	TH1F* h1 = (TH1F*)f->Get(hist_name);

	// Fixing title
	h1->SetTitle(" ");

	// Fixing the X axis
	//h1->GetXaxis()->SetRangeUser(-0.1,1.6);
	h1->GetXaxis()->SetLabelSize(0.03);
	h1->GetXaxis()->SetTitleSize(0.05);
	h1->GetXaxis()->SetTitleOffset(0.99);
	h1->GetXaxis()->CenterTitle();
	h1->GetXaxis()->SetTitle("Cos #theta_{#bar{#Lambda}}");

	// Fixing the Y axis
	//h1->GetYaxis()->SetRangeUser(0,180);
	h1->GetYaxis()->SetLabelSize(0.03);
	h1->GetYaxis()->SetTitleSize(0.05);
	h1->GetYaxis()->SetTitleOffset(0.9);
	h1->GetYaxis()->CenterTitle();
	h1->GetYaxis()->SetTitle("Entries");

	h1->SetStats(kFALSE);

	//*************************************************************************
    //                               TCanvas
    //*************************************************************************

	TCanvas * can = new TCanvas("c1","");
	can->SetCanvasSize(1200, 800);
   	can->SetWindowSize(1200, 800);
	can->SetTopMargin(0.025);
	can->SetBottomMargin(0.15);
	can->SetLeftMargin(0.15);
	can->SetLogz();

    h1->Draw("colz");

    // Drawing text
    TLatex latexText1(-0.8,20000,"PANDA MC Simulation");
    latexText1.SetTextFont(42);
    latexText1.SetTextSize(0.06);
    latexText1.SetTextColor(kGray+1);
    latexText1.DrawClone();

    TLatex latexText2(-0.8,18500,"p_{beam} = 1.642 GeV/c");
    latexText2.SetTextFont(62);
    latexText2.SetTextSize(0.05);
    latexText2.SetTextColor(kGray+1);
    latexText2.DrawClone();

    can->SaveAs("hlam0bar_thtcm.pdf");

    can->Draw();
}
