import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import uproot


# Plot 2D Histogram
# Plot 2D Histogram
def plot_2d_histogram(x, y, bins=100, range=None, cmap='viridis', norm=None, colorbar=True,
                      banner=None, xlabel=None, ylabel=None, title=None, figsize=(10,8), 
                      savefig=False, filename="2d_histogram.png", dpi=300, **kwargs):
    """
    Plot a 2D histogram with a color bar.

    Parameters:
        x (array-like): Data for the x-axis.
        y (array-like): Data for the y-axis.
        bins (int or [int, int]): Number of bins or [bins_x, bins_y]. Default is 100.
        range ([[xmin, xmax], [ymin, ymax]]): Range for the bins. Default is None.
        cmap (str or Colormap): Colormap to use. Default is 'viridis'.
        norm (Normalize): Normalization for the colormap (e.g., LogNorm). Default is None.
        colorbar (bool): Whether to add a color bar. Default is True.
        xlabel (str): Label for the x-axis. Default is None.
        ylabel (str): Label for the y-axis. Default is None.
        title (str): Title of the plot. Default is None.
        figsize (tuple): Figure size (width, height). Default is (10, 8).
        savefig (bool): Whether to save the figure. Default is False.
        filename (str): Name of the file to save. Default is "2d_histogram.png".
        dpi (int): DPI for saving the figure. Default is 300.
        **kwargs: Additional keyword arguments passed to `plt.hist2d`.
    """
    plt.close('all')
    
    # Create the figure and axis
    fig, ax = plt.subplots(figsize=figsize)

    # Plot the 2D histogram
    H, xedges, yedges, image = ax.hist2d(x, y, bins=bins, range=range, cmap=cmap, norm=norm, **kwargs)
        
    # Handle Colorbar
    if colorbar:
        cbar = plt.colorbar(image, ax=ax, pad=0., shrink=1.0)
        cbar.ax.tick_params(labelsize=16)

    # Handle Labels
    if xlabel:
        ax.set_xlabel(xlabel, fontsize=18)
    if ylabel:
        ax.set_ylabel(ylabel, fontsize=18)
    if title:
        ax.set_title(title, fontsize=18)

    # Handle Ticks
    ax.tick_params(axis='both', which='major', labelsize=16)
    ax.tick_params(axis='both', which='minor', labelsize=16)
            
    # Handle Banner
    if banner is not None:
        try:
            import atlasify as atl
            atl.atlasify(
                "Simulation",           # Main Text
                banner,                 # Sub Text
                font_size=18,           # Main Badge
                label_font_size=18,     # Text After Badge
                sub_font_size=14,       # Subtext Font
                subtext_distance=0.0,   # Distance before subtext
                brand="PANDA",          # Change defaults to PANDA
                outside=False,          # Inside or Outside
                # enlarge=1.2,
            )
        except ImportError:
            print("Warning: 'atlasify' module not found. Skipping banner text.")
    
    # Save the figure if requested
    if savefig:
        fig.savefig(filename, dpi=dpi, bbox_inches='tight')
        
    return ax, fig

def main():
    
   # Choose Events
    n_events = 1000000
    
    # Open the ROOT file and get data as before
    file = uproot.open("fwp1-MCTruth.root")
    tree = file["ntpMCTruth;1"]

    # Extract Data
    zvtx_keys = ["MC_d0d0z", "MC_d1d0z"]  # List of keys
    zvtx_data = [tree[key].array(library="np")[:n_events] for key in zvtx_keys if key in tree.keys()]  # Ensure key exists
    z_vtx = np.concatenate(zvtx_data)  # Concatenate arrays

    rvtx_keys_x = ["MC_d0d0x", "MC_d1d0x"]
    rvtx_keys_y = ["MC_d0d0y", "MC_d1d0y"]

    # Concatenate Data for radius (r_vtx)
    r_vtx_data = [
        np.sqrt(tree[key_x].array(library="np")[:n_events]**2 + tree[key_y].array(library="np")[:n_events]**2)
        for key_x, key_y in zip(rvtx_keys_x, rvtx_keys_y) if key_x in tree.keys() and key_y in tree.keys()
    ]
    r_vtx = np.concatenate(r_vtx_data)  # Concatenate arrays
    
    # Plot 2D Histogram
    xlabel = "Z-vertex / cm"
    ylabel = "R-vertex / cm"
    banner = r"Simulated: $\bar{p}p \rightarrow \bar{\Lambda} \: (\bar{p}\pi^{+}) \: \Lambda \:  (p\pi^{-})$"+"\nBeam Momentum = 1.642 GeV/c"
    
    ax, fig = plot_2d_histogram(
        x=z_vtx, y=r_vtx,             # Input of x and y
        bins=[500, 500],              # Bins for x and y
        range=[[-5,220], [0,50]],     # Range for x and y
        cmap='plasma',                # Colormap
        norm=mcolors.LogNorm(),       # Logarithmic scaling
        banner=banner,                # Banner Text
        xlabel=xlabel,                # X Label
        ylabel=ylabel,                # Y Label
        figsize=(8,6),                # Figure size
        savefig=False,                # Save figure
        filename="mcVtx.pdf"          # Figure name with type
    );

    # Add annotations
    annotations = [
        (50, 35, "STT", 15, 'black'),
        (-1, 10, "MVD", 15, 'black'),
        (160, 35, "GEM", 15, 'black')
    ]
    for x, y, text, size, *color in annotations:
        ax.text(x, y, text, fontsize=size, fontweight='bold', color=color[0] if color else 'black')

    # Add lines
    lines = [
        (-5, 13.5, 19, 13.5),  # MVD Boundary
        (19, 0, 19, 13.5),     # MVD Edge
        (-5, 15, 100, 15),     # STT Lower Boundary
        (-5, 42, 100, 42),     # STT Upper Boundary
        (100, 15, 100, 42),    # STT Side
        (117, 0, 117, 45),     # GEM Left
        (153, 0, 153, 49),     # GEM Middle
        (189, 0, 189, 49),     # GEM Right
    ]
    for x1, y1, x2, y2 in lines:
        ax.plot([x1, x2], [y1, y2], 'k-', linewidth=1)
        
    fig.savefig("mcVtx_1642.png", dpi=300, bbox_inches='tight')

__name__ == "__main__"
main()
