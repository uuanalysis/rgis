## Standard Target

```bash
# reaction
ppbar --> lambda + lambdabar --> p + piminus + c.c
```

```bash
# notation
pbarp	          pbarp
pbarp_d0          lambdabar
pbarp_d0d0        pbar (antiproton)
pbarp_d0d1        pi+
pbarp_d1          lambda
pbarp_d1d0        p (proton)
pbarp_d1d1        pi-
```

## _Important Information_

- 10000
- `ana_ntp.C` for `fwp, bkg, dpm` for consistencey. For `fwp/signal` the _**FairTask**_ `ana_ideal.C or anaideal.C` can be used. However, this might not work for _**Non-resonant background**_.
- copy `ntpBestPbarP.C, ntpBestPbarP.h` accross folders


    - 1_fwp for signal
    - 2_bkg for non-resonant bkg
    - 3_dmp for generic bkg


## _Plotting Macros_

Important macros for get plots

- `pl_dGausFit.C`: Double Gaussian Fit Plots
    - Lambda Mass with dGauss Fit with Cut 1
    - Anti-Lambda Mass with dGauss Fit with Cut 1

&nbsp;

- `pl_comb.C`: Combine Plots of Signal (`fwp1/ntpBestPbarP.root`) and Background (`bkg1/ntpBestPbarP.root`)
    - Lambda Mass
    - Anti-Lambda Mass
    - pbarp_chi2
    - pbarp_prob
    - Sum of Z-axis of Decay Vertex of LLBar

&nbsp;

- `pl_vtxf.C`: Get Vetex Fit Probability (LLBar)


&nbsp;

```bash
# Plot Signal+Bkg
root -l pl_comb.C
```